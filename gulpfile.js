var gulp = require("gulp"),
    concat = require('gulp-concat'),
    template = require('gulp-template-compile'),
    minifyCSS = require('gulp-minify-css'),
    uglify = require("gulp-uglify"),
    fs = require('fs-extra'),
    styls = require("gulp-stylus"),
    ts = require('gulp-typescript'),
    sourcemaps = require('gulp-sourcemaps'),
    gulpsync = require('gulp-sync')(gulp);


var projectpaths = {
    style: "src/**/*.styl",
    css: "src/**/*.css",
    map: "src/**/*.map",
    js: "src/**/*.js",
    ts: "src/**/*.ts",
    html: "src/canvas.html",
    fonts: [
        "src/**/*.otf",
        "src/**/*.eot",
        "src/**/*.ttf",
        "src/**/*.woff",
        "src/**/*.woff2",
        "src/**/*.svg"
    ],
    images: [
        "src/**/*.jpg",
        "src/**/*.png",
        "src/**/*.gif"
    ],
    priority: [
        "style", "css", "ts", "html", "fonts", "images", "js", "map"
    ]
};

gulp.task("min-tween", function () {

    gulp.src("src/utils/utils/vendor/")

});

var rootFolder = "src";

gulp.task("default", ["NarrEpub", "comics", "utils", "build"]);

gulp.task("NarrEpub", function () {

    gulp.src("src/plugins/NarrEpub/tpl/*.html")
        .pipe(template())
        .pipe(concat('templates.js'))
        .pipe(gulp.dest('src/plugins/NarrEpub/'));

});

gulp.task("comics", function () {

    gulp.src("src/plugins/NarrComics/tpl/*.html")
        .pipe(template())
        .pipe(concat('comicsTpl.js'))
        .pipe(gulp.dest('src/plugins/NarrComics/'));

});

gulp.task("utils", function () {

    gulp.src("src/utils/utils/*.html")
        .pipe(template())
        .pipe(concat('template.js'))
        .pipe(gulp.dest('src/utils/utils/'));

});

gulp.task("copy", function () {

    var folders = ["debug-build", "build"];

    folders.forEach(function (folderPath, index) {

        var names = fs.readdirSync("src");

        names.forEach(function (name) {

            fs.copySync("src/" + name, folderPath + "/" + name);

        });

    });

});

gulp.task("compile-ts", function () {

    var tsResult = gulp.src(projectpaths.ts)
        .pipe(sourcemaps.init()) // This means sourcemaps will be generated
        .pipe(ts({
            module: "AMD",
            target: "ES5",
            noExternalResolve: true
        }));

    return tsResult.js
        .pipe(sourcemaps.write("./")) // Now the sourcemaps are added to the .js file
        .pipe(gulp.dest(rootFolder));


});

gulp.task("compile-style", function () {

    gulp.src(projectpaths.style)
        .pipe(styls())
        .pipe(gulp.dest(rootFolder));

});

gulp.task("after-copy", function () {

    var folders = ["debug-build", "build"];

    folders.forEach(function (folderPath) {

        fs.deleteSync(folderPath + "/interface");

        var version = fs.readFileSync("src/utils/ver.js", {encoding: "utf-8"});
        var config = fs.readFileSync("./" + folderPath + "/utils/app/config.js", {encoding: "utf-8"});
        var verArr = version.split(/\n/);

        fs.writeFileSync(folderPath + "/utils/app/config.js", config.replace("tsEngineType", verArr[0]).replace("verMajor", verArr[1]).replace("verMinor", verArr[2]));
        fs.writeFileSync(folderPath + "/utils/ver.js", version);

    });

});

gulp.task("minify", function () {


});

gulp.task("build", gulpsync.sync(["compile-ts", /*"compile-style", */ "copy", "after-copy", "minify"]));