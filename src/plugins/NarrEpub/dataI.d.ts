/// <reference path="../../interface/defaultDataI.d.ts" />

interface EpubDataI extends defaultDataI {
    pdfMode: boolean;
    singlePageMode: boolean;
    path: string;
    styles: Array<string>;
    content: Array<string>;
    subjectList: Array<Object>;
}

declare var data:EpubDataI;