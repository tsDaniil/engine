(function() {(window["JST"] = window["JST"] || {})["bookMarkList.tpl.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 for (var i = 0; i < list.length; i++) { ;
__p += '\n<a href="#" data-progress="' +
((__t = ( list[i].page )) == null ? '' : __t) +
'" class="list-group-item ' +
((__t = ( list[i].type )) == null ? '' : __t) +
'">' +
((__t = ( list[i].text )) == null ? '' : __t) +
'</a>\n';
 } ;


}
return __p
}})();
(function() {(window["JST"] = window["JST"] || {})["bookmark.tpl.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="bookmark" class="subject view">\n    <div class="epub-toolBar-top">\n        <div class="btn-group-sm btn-group bookmark-group">\n            <button type="button" data-mode="subject" class="btn btn-default subject-matter active">Содержание\n            </button>\n            <button type="button" data-mode="bookmark" class="btn btn-default bookmark">Закладки</button>\n        </div>\n        <div class="btn-group-sm btn-group menu">\n            <button type="button" data-show="read" class="btn showView btn-default">Закрыть</button>\n        </div>\n    </div>\n    <div class="list-group list-dom">\n    </div>\n</div>';

}
return __p
}})();
(function() {(window["JST"] = window["JST"] || {})["epub.tpl.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="read" class="view active">\n    <div class="epub-toolBar-top">\n        <div class="back btn-group-sm btn-group left">\n            <button type="button" class="btn btn-default to-platform"><span\n                    class="glyphicon glyphicon-chevron-left"></span>\n                Назад\n            </button>\n        </div>\n        <div class="btn-group-sm btn-group menu">\n            <button type="button" data-show="settings" class="showView btn btn-default"><i class="fa fa-cog"></i></i>\n            </button>\n            <button type="button" class="btn btn-default bookmark"><i class="fa fa-bookmark-o"></i></button>\n            <button type="button" data-show="bookmark" class="showView btn btn-default"><i class="fa fa-list"></i>\n            </button>\n        </div>\n    </div>\n    <div id="epub">\n        <div class="epub-body">\n            <div class="epub-content">\n            </div>\n        </div>\n    </div>\n    <div class="toolBar-bottom">\n        <div>\n            <hr>\n            <div class="slider"></div>\n            <span class="progress">0%</span>\n        </div>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {(window["JST"] = window["JST"] || {})["general.tpl.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="screen">\n    ' +
((__t = ( read )) == null ? '' : __t) +
'\n    ' +
((__t = ( bookmark )) == null ? '' : __t) +
'\n    ' +
((__t = ( settings )) == null ? '' : __t) +
'\n</div>';

}
return __p
}})();
(function() {(window["JST"] = window["JST"] || {})["settings.tpl.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="settings" class="view">\n    <div class="epub-toolBar-top">\n        <div class="btn-group-sm btn-group menu">\n            <button type="button" data-show="read" class="btn showView btn-default">Закрыть</button>\n        </div>\n    </div>\n\n    <div class="row">\n\n        <div class="col-md-2 col-sm-1 col-xs-0"></div>\n        <div class="col-md-8 col-sm-10 col-xs-12">\n\n            <ul class="list-group">\n                <li class="list-group-item" style="overflow: hidden">\n                    <div class="col-md-6 col-sm-6 col-xs-4">Анимации</div>\n                    <div class="col-md-6 col-sm-6 col-xs-8" style="position: relative">\n\n                        <div class="onoffswitch" style="position: absolute; left: 0; top: -7px;">\n                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" ';
 if (typeof(checked) != "undefined") { ;
__p += ' checked ';
 } ;
__p += '>\n                            <label class="onoffswitch-label" for="myonoffswitch">\n                                <span class="onoffswitch-inner"></span>\n                                <span class="onoffswitch-switch"></span>\n                            </label>\n                        </div>\n\n                    </div>\n                </li>\n                <li class="list-group-item speed-line" style="overflow: hidden">\n                    <div class="col-md-6 col-sm-6 col-xs-4">Скорость анимации</div>\n                    <div class="col-md-6 col-sm-6 col-xs-8" style="padding-left: 0">\n\n                        <div class="btn-group btn-group-xs">\n                            <button type="button" data-speed="1000" class="set-speed btn btn-default">Медленно</button>\n                            <button type="button" data-speed="500" class="set-speed btn btn-default">Нормально</button>\n                            <button type="button" data-speed="200" class="set-speed btn btn-default">Быстро</button>\n                        </div>\n\n                    </div>\n                </li>\n            </ul>\n\n        </div>\n        <div class="col-md-2 col-sm-1 col-xs-0"></div>\n\n    </div>\n\n</div>';

}
return __p
}})();