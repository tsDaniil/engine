/// <reference path="./ToolBarI.d.ts" />
/// <reference path="../dataI.d.ts" />
/// <reference path="../epub/interfaces/EpubI.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="../../../interface/Events.d.ts" />

interface bookmarkI {
    type: string;
    text: string;
    page: number;
}

import $ = require("$");
import Templater = require("../../../utils/app/Templater");
import LocalStorageManager = require("../../../utils/app/LocalStorageManager");
import UserData = require("../../../utils/app/user/UserData");

class BookmarkBar {

    private elements:ElementsI;
    private epub:EpubI;
    private parent;

    public bookmarkList:Array<StateI> = [];

    constructor(parent, epub:EpubI, Elements:ElementsI, data:EpubDataI) {
        this.parent = parent;
        this.elements = Elements;
        this.epub = epub;

        this.init(data);
    }

    private init(data:EpubDataI):void {

        this.addSubjectList(data);
        this.startEpub();

        var that = this;

        this.elements.bookmark.bookmarkModeButtons.tap(function () {
            that.elements.bookmark.bookmarkModeButtons.toggleClass("active");
            that.elements.bookmark.view.removeClass()
                .addClass("view")
                .addClass("active")
                .addClass(this.attr("data-mode"));
        });

        this.elements.bookmark.listDom.addEventListener($.events.start, function (event:MouseEvent) {

            that.elements.bookmark.close.tap();
            that.epub.jumpByPage(parseFloat($(event.target).attr("data-progress")));
            that.epub.change();

        });

        this.elements.bookmark.listDom.on($.events.start, function () {

            var moveHandler = function (event:MoveI) {
                that.elements.bookmark.listDom.node.scrollTop += -event.lastDistance.y;
            };

            that.elements.bookmark.view.on("User:move", moveHandler);

            var endHandler = function () {
                that.elements.bookmark.view.off("User:move", moveHandler);
                that.elements.bookmark.listDom.off($.events.end, endHandler);
            };

            that.elements.bookmark.listDom.on($.events.end, endHandler);
        });

    }

    private addBookmarkList():void {
        this.elements.bookmark.listDom.find(".bookmark").remove();
        this.addList({
            list: this.bookmarkList.map(function (state:StateI) {
                return {
                    "type": "bookmark",
                    "page": state.page,
                    "text": "Закладка. Прогресс: " + state.page
                };
            })
        });
    }

    private addList(bookMarkData):void {
        this.elements.bookmark.listDom.append(Templater.getTemplate("bookMarkList.tpl.html", bookMarkData));
    }

    private addSubjectList(data:EpubDataI):void {
        this.addList({list: data.subjectList});
    }

    public checkBookmark(state:StateI):void {
        if (this.bookmarkList.some(function (bookmark:StateI) {
                return bookmark.page == state.page;
            })) {
            this.elements.epub.addBookmark.addClass("active");
        } else {
            this.elements.epub.addBookmark.removeClass("active");
        }
    }

    public toggleBookmark():void {
        var state = this.epub.getState();
        if (this.elements.epub.addBookmark.hasClass("active")) {
            this.bookmarkList = this.bookmarkList.filter(function (bookmark:StateI) {
                return (state.page != bookmark.page);
            });
        } else {
            this.bookmarkList.push(state);
        }
        this.addBookmarkList();
        LocalStorageManager.setObject("epub:bookmarks", this.bookmarkList);
        this.elements.epub.addBookmark.toggleClass("active");
    }

    private startEpub():void {
        var toolBar = this;

        LocalStorageManager.onLoad(function () {
            LocalStorageManager.getObject("epub:bookmarks", function (bookmarkList:Array<StateI>) {
                toolBar.bookmarkList = bookmarkList || [];
                toolBar.addBookmarkList();
            });
            LocalStorageManager.getObject("epub", function (state:StateI) {
                console.log("state:", state);
                if (state) {
                    toolBar.epub.loadByState(state);
                }
                if (UserData.getSearch().page_index) {
                    toolBar.epub.jumpByPage(Number(UserData.getSearch().page_index));
                    toolBar.epub.change();
                }
                toolBar.epub.start();
                toolBar.parent.fireEvent("Epub:Started");
            });
        });
    }
}
export = BookmarkBar;