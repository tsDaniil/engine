/// <reference path="../../../interface/ModuleI.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />

interface ToolBarI extends ModuleI {
    setSizeEpubContainer():void;
}

interface ElementsEpubI {

    view: $i;
    exit: $i;
    showSettings:$i;
    addBookmark: $i;
    showBookmark: $i;
    progress: $i;
    slider: $i;
    epubDom: $i;
    hr: $i;

}

interface ElementsSettingsI {

    view: $i;
    onoffswitch: $i;
    myonoffswitch: $i;
    speedLine: $i;
    setSpeed: $$;
    close: $i;

}

interface ElementsBookmarkI {

    view: $i;
    close: $i;
    bookmarkModeButtons: $$;
    listDom: $i;

}

interface ElementsI {

    epub:ElementsEpubI;
    settings:ElementsSettingsI;
    bookmark:ElementsBookmarkI;

}