/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="./ToolBarI.d.ts" />

import $ = require("$");

class Elements {

    private epub:ElementsEpubI;
    private settings:ElementsSettingsI;
    private bookmark:ElementsBookmarkI;

    constructor() {

        this.epub = {

            view: $("#read"),
            exit: $(".to-platform"),
            showSettings: $("#read .menu").find(".showView").first(),
            addBookmark: $("#read .menu").find(".bookmark").first(),
            showBookmark: $("#read .menu").find(".showView").last(),

            progress: $("#read .progress"),
            slider: $("#read .slider"),
            epubDom: $("#epub"),
            hr: $("#read hr")
        };

        this.settings = {

            view: $("#settings"),
            onoffswitch: $(".onoffswitch"),
            myonoffswitch: $("#myonoffswitch"),
            speedLine: $(".speed-line"),
            setSpeed: $("#settings").find(".set-speed"),
            close: $("#settings .showView")


        };

        this.bookmark = {

            view: $("#bookmark"),
            close: $("#bookmark .showView"),
            bookmarkModeButtons: $("#bookmark .btn-group").find("button"),
            listDom: $("#bookmark .list-dom")

        };
    }

}

export = Elements;