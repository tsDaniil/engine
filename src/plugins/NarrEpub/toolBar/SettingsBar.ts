/// <reference path="../epub/interfaces/EpubI.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="./ToolBarI.d.ts" />

import $ = require("$");
import UserData = require("../../../utils/app/user/UserData");

class Settings {

    private epub:EpubI;
    private elements:ElementsI;

    constructor(epub:EpubI, elements:ElementsI) {
        this.epub = epub;
        this.elements = elements;
        this.setHandlers();
    }

    private setHandlers():void {

        var that = this;

        if (UserData.isDesktop()) {
            this.elements.settings.setSpeed.last().addClass("active");
        } else {
            this.elements.settings.setSpeed.first().addClass("active");
        }

        this.elements.settings.myonoffswitch.change(function () {

            that.epub.animanions = !that.epub.animanions;
            that.elements.settings.speedLine.displayToggle();

        });

        this.elements.settings.setSpeed.tap(function () {
                this.parent().find(".active").removeClass("active");
                that.epub.speed = parseInt(this.addClass("active").attr("data-speed"));
            });

        if (!that.epub.animanions) {
            that.elements.settings.speedLine.hide();
        }

        that.epub.speed = parseInt($(".set-speed.active").attr("data-speed"));

    }

}
export = Settings;