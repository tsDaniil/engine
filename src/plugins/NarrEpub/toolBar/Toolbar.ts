/// <reference path="../epub/interfaces/EpubI.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />

import Elements = require("./Elements");
import SettingsBar = require("./SettingsBar");
import EpubBar = require("./EpubBar");
import BookmarkBar = require("./BookmarkBar");
import $ = require("$");

class Toolbar {

    private epubBar;
    public settingsBar;
    public epub:EpubI;
    public bookmarkBar;
    private elements;

    constructor(parent, epub:EpubI, data:EpubDataI) {

        this.epub = epub;
        this.elements = new Elements();
        this.epubBar = new EpubBar(this, this.epub, this.elements, data);
        this.settingsBar = new SettingsBar(this.epub, this.elements);
        this.bookmarkBar = new BookmarkBar(parent, this.epub, this.elements, data);

        this.setGeneralHandlers();

    }

    private setGeneralHandlers():void {

        $.createCollection([
            this.elements.bookmark.close,
            this.elements.settings.close,
            this.elements.epub.showSettings,
            this.elements.epub.showBookmark
        ]).tap(function () {
            $(".view.active").toggleClass("active");
            $("#" + this.attr("data-show")).toggleClass("active");
        });

    }

}
export = Toolbar;