/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="../../../interface/Events.d.ts" />
/// <reference path="./ToolBarI.d.ts" />
/// <reference path="../epub/interfaces/EpubI.d.ts" />
/// <reference path="../NarrEpubI.d.ts" />
/// <reference path="../dataI.d.ts" />


import Module = require("../../../utils/base/Module");
import $ = require("$");
import LocalStorageManager = require("../../../utils/app/LocalStorageManager");
import PlatformManager = require("../../../utils/app/PlatformManager");

class EpubBar extends Module {

    public parent;
    public epub:EpubI;

    private elements:ElementsI;

    constructor(parent, epub:EpubI, elements:ElementsI, data:EpubDataI) {
        super();
        this.elements = elements;
        this.parent = parent;
        this.epub = epub;

        this.init(data);
    }

    private init(data:EpubDataI):void {

        this.setHandlers();

    }

    private setHandlers():void {

        this.setToolBarTopHandlers();
        this.setScrollerHandlers();
        this.setEpubHandlers();

    }

    private setEpubHandlers():void {

        var that = this;

        this.epub.onChange(function (state:StateI, progress:number) {
            that.elements.epub.progress.html(Math.round(progress) + "%");
            that.elements.epub.slider.css("left", progress + "%");
            LocalStorageManager.setObject("epub", state);
            that.parent.bookmarkBar.checkBookmark(state);
        });

        this.elements.epub.epubDom.tap(function (event:TapI) {
            if (event.coords.x >= that.elements.epub.epubDom.width() / 2) {
                that.epub.next();
            } else {
                that.epub.prev();
            }
        });

        this.elements.epub.epubDom.on("User:swipe:horizontal", function (direction) {
            if (direction == "left") {
                that.epub.next();
            } else {
                that.epub.prev();
            }
        });

    }

    private setToolBarTopHandlers():void {

        this.elements.epub.exit.tap(function () {
            PlatformManager.exit();
            setTimeout(function () {
                PlatformManager.showSettings();
            }, 250);
        });

        this.elements.epub.addBookmark.tap(function () {
            this.parent.bookmarkBar.toggleBookmark();
        }.bind(this));

    }

    private setScrollerHandlers():void {

        var that = this;

        this.elements.epub.slider.on("User:touch:start", function () {

            var percent;

            that.view.on("User:move", function (event:MoveI) {

                var offset = that.elements.epub.hr.offset(true).left;
                percent = ((event.coords.x - offset) / that.elements.epub.hr.width()) * 100;

                if (percent >= 100) {
                    percent = 100;
                }

                if (percent <= 0) {
                    percent = 0;
                }

                that.elements.epub.progress.html(Math.floor(percent) + "%");
                that.elements.epub.slider.css("left", percent + "%");

            });

            that.view.on("User:touch:end", function () {
                that.view.off("User:move");
                that.view.off("User:touch:end");
                that.setPage(percent);
            });

        });

    }

    public setPage(percent:number):void {
        this.epub.jumpByPercent(Math.floor(percent));
    }

}
export = EpubBar;