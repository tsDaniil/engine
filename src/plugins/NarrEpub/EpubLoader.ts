/// <reference path="./dataI.d.ts" />
/// <reference path="./NarrEpubI.d.ts" />

import Templater = require("../../utils/app/Templater");
import Downloader = require("../../utils/utils/Downloader");

interface ToLoad {
    DomLiteCss:boolean;
    templates:boolean;
    stylesVendor:boolean;
    stylesData:boolean;
    stylesEngine:boolean;
}

class EpubLoader {

    private parent:NarrEpubI;
    private toLoad:ToLoad = {
        DomLiteCss: false,
        templates: false,
        stylesVendor: false,
        stylesData: false,
        stylesEngine: false
    };

    constructor(parent:NarrEpubI, data:EpubDataI) {
        this.parent = parent;

        this.init(data);
    }

    private init(data:EpubDataI):void {

        var that = this;

        Downloader.addLitePlugin("DomLite-css", function () {
            that.toLoad.DomLiteCss = true;
            that.checkLoad();
        });
        Templater.loadTemplate("plugins/NarrEpub/templates", function () {
            that.toLoad.templates = true;
            that.checkLoad();
        });

        this.loadStyles(data);

    }

    private checkLoad():void {
        var allLoaded = true;
        for (var module in this.toLoad) {
            if (this.toLoad.hasOwnProperty(module)) {
                if (!this.toLoad[module]) {
                    allLoaded = false;
                    break;
                }
            }
        }
        if (allLoaded) {
            this.parent.loaded();
        }
    }

    private loadStyles(data:EpubDataI):void {
        var that = this;
        Downloader.loadEngineStyles([
            "utils/utils/vendor/bootstrap/bootstrap.min.css",
            "utils/utils/vendor/bootstrap/bootstrap-theme.min.css",
            "utils/utils/vendor/font-awesome/font-awesome.min.css"
        ], function () {
            that.toLoad.stylesVendor = true;
            that.checkLoad();
        });
        Downloader.loadStyles(data.styles, function () {
            that.toLoad.stylesData = true;
            that.checkLoad();
        });
        Downloader.loadEngineStyle("plugins/NarrEpub/css/style.css", function () {
            that.toLoad.stylesEngine = true;
            that.checkLoad();
        });
    }

}
export = EpubLoader;