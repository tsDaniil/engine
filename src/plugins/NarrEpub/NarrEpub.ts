/// <reference path="./NarrEpubI.d.ts" />
/// <reference path="../../interface/require.d.ts" />
/// <reference path="./epub/interfaces/EpubI.d.ts" />
/// <reference path="./dataI.d.ts" />8
/// <reference path="../../interface/DomLite.d.ts" />
/// <reference path="./toolBar/ToolBarI.d.ts" />

import Module = require("../../utils/base/Module");
import Epub = require("./epub/classes/Epub");
import Templater = require("../../utils/app/Templater");
import Toolbar = require("./toolBar/Toolbar");
import UserData = require("../../utils/app/user/UserData");
import PlatformManager = require("../../utils/app/PlatformManager");
import EpubLoader = require("./EpubLoader");
import OrientationManager = require("../../utils/utils/OrientationManager");
import $ = require("$");

class NarrEpub extends Module implements NarrEpubI {

    private epub:EpubI;
    private toolBar;

    public run(data:EpubDataI):void {

        var eManager:NarrEpub = this;

        new EpubLoader(this, data);

        this.onLoad(function () {

            NarrEpub.addTemplate();
            eManager.createEpub();
            eManager.createToolBar(data);
            data.content = null;
            eManager.fireEvent("ready");
        });

        this.listenTo(OrientationManager, OrientationManager.eventName, this.onChangeOrientation, this);

    }

    private onChangeOrientation():void {
        this.toolBar.epubBar.setSizeEpubContainer();
        this.epub.dropScale();
        this.epub.setSingePageMode(NarrEpub.getMode());
    }

    public onResize(width:number, height:number):void {
        if (this.toolBar) {
            this.toolBar.epubBar.setSizeEpubContainer();
            this.epub.dropScale();
            this.epub.redrow();
        }
    }

    public getPageCount():number {
        return 1;
    }

    public setPage():void {

    }

    private createEpub():void {
        var epubNode = $("#epub");
        if (!data.pdfMode) {
            epubNode.addClass("epub-mode");
        }
        this.epub = new Epub(NarrEpub.fixUrls(data.content, data.path), epubNode.node, data.pdfMode, NarrEpub.getMode(), {});
        this.epub.animanions = UserData.isDesktop();
    }

    private createToolBar(data:EpubDataI):void {
        this.toolBar = new Toolbar(this, this.epub, data);
    }

    static addTemplate():void {
        $("#bigrect").append(Templater.getTemplate("general.tpl.html", {
            settings: Templater.getTemplate("settings.tpl.html", {
                checked: UserData.isDesktop() ? true : undefined
            }),
            bookmark: Templater.getTemplate("bookmark.tpl.html", {}),
            read: Templater.getTemplate("epub.tpl.html", {})
        }));
    }

    static fixUrls(content:Array<string>, path:string):Array<string> {
        return content.map(function (slide:string) {
            return slide.replace(/<img.+?>/g, function (imageString:string) {
                return imageString.replace(/\.\.\//, "/").replace(/src=\"/, "src=\"" + path + "/").replace(/\/\//, "/");
            });
        });
    }

    static getMode():boolean {
        if (UserData.getSearch().singlePageMode && UserData.getSearch().singlePageMode == "true") {
            return true;
        }
        return OrientationManager.getOrientation() != "horizontal";
    }

}
export = NarrEpub;