/// <reference path="../../../../interface/DomLite.d.ts" />
/// <reference path="../interfaces/HeadI.d.ts" />
/// <reference path="../interfaces/SlideI.d.ts" />

import Module = require("../../../../utils/base/Module");
import UserData = require("../../../../utils/app/user/UserData");
import $ = require("$");

class Slide extends Module implements SlideI {

    public hasNode:boolean = false;
    public node:$$;
    public parent:HeadI;
    public targetNode:$i;
    public slideContent:string;

    constructor(parent:HeadI, targetNode:$i, slideContent:string) {
        super();
        this.parent = parent;
        this.targetNode = targetNode;
        this.slideContent = slideContent;
    }

    public hide(direction?:number):void {

        var param;

        if (typeof direction != "undefined" && direction != 0) {
            param = direction > 0;
        }

        if (this.hasNode) {

            this.getAnimationManager().hide(this, param);

            this.trigger("Epub:Slide:hide", []);
        }
    }

    public show(direction?:number):void {

        var param;

        if (typeof direction != "undefined" && direction != 0) {
            param = direction > 0;
        }

        this.node.removeClass("preload").addClass("toShow");
        this.getAnimationManager().show(this, param);

        this.trigger("Epub:Slide:show", []);
    }

    public preLoad():void {
        if (!this.hasNode) {
            this.createNode();
        }
        if (!UserData.isWindows()) {
            this.node.addClass("preload").removeClass("toShow");
            this.getAnimationManager().addToDom(this);
        }
    }

    public remove():void {
        if (this.hasNode) {
            this.node.remove();
        }
    }

    private createNode():void {
        this.node = $.parseHTML(this.slideContent, true).children();
        this.hasNode = true;
    }

    public getNodes():$$ {
        return this.node;
    }

    public getEpub():EpubI {
        return this.parent.getEpub();
    }

    private getAnimationManager():AnimationManagerI {
        return this.getEpub().getAnimationManager();
    }

    public destroy():void {
        this.remove();
        this.node = null;
        this.hasNode = false;
    }
}
export = Slide;