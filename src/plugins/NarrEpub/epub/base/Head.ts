/// <reference path="../interfaces/SlideI.d.ts" />
/// <reference path="../interfaces/EpubI.d.ts" />
/// <reference path="../interfaces/HeadI.d.ts" />

import Collection = require("../../../../utils/base/Collection");
import $ = require("$");

class Head extends Collection implements HeadI {

    public parent:EpubI;
    public collection:Array<SlideI> = [];
    public singleMode:boolean;
    public content:Array<string> = [];
    public node:$i;
    public loadedPages:Array<number> = [];
    public rejectedPages:Array<number> = [];

    constructor(parent:EpubI, content:Array<string>, node:$i, singlePage:boolean) {
        super();
        this.node = node;
        this.parent = parent;
        this.content = content;
        this.singleMode = singlePage;
    }

    public getActive():SlideI {
        return this.getByIndex(this.active);
    }

    public getByIndex(index):SlideI {
        return this.collection[index];
    }

    public next():void {
        this.jumpTo(this.active + 1);
    }

    public prev():void {
        this.jumpTo(this.active - 1);
    }

    public getEpub():EpubI {
        return this.parent;
    }

    public getProgress(step:number) {
        return this.getStep(step) * (this.active + 1);
    }

    public getStep(step:number) {
        return (step / (this.collection.length));
    }

    public jumpTo(page:number):void {
        if (this.has(page)) {
            var direction:number = page - this.active;
            this.hide(direction);
            this.active = page;
            this.setLoadedPages();
            this.show(direction);
            this.getEpub().change();
        }
    }

    public preLoad():void {
        if (this.getActive()) {
            this.getActive().preLoad();
        }
    }

    public setLoadedPages():void {

        if (!$.hasInArray(this.loadedPages, this.active)) {
            this.addToLoadedPages(this.active);
        }
        var that = this;

        this.loadedPages = this.loadedPages.filter(function (pageIndex:number) {
            if (Math.abs(this.active - pageIndex) > 2) {
                that.getByIndex(pageIndex).remove();
                return false;
            } else {
                return true;
            }
        }, this);

        if (this.hasNext() && !$.hasInArray(this.loadedPages, this.active + 1)) {
            this.addToLoadedPages(this.active + 1);
        }

        if (this.hasPrev() && !$.hasInArray(this.loadedPages, this.active - 1)) {
            this.addToLoadedPages(this.active - 1);
        }
    }

    public hide(direction?:number) {
        if (this.getActive()) {
            this.getActive().hide(direction);
        }
    }

    public show(direction?:number) {
        this.getActive().show(direction);
    }

    public setSingePageMode(mode:boolean):void {
        if (mode != this.singleMode) {

            this.singleMode = mode;
            if (mode) {
                this.active = this.active * 2;
            } else {
                this.active = Math.round(this.active / 2);
            }

            this.removeCollection();
            this.initCollection();
            this.setLoadedPages();

            if (!this.has(this.active)) {
                this.active = this.collection.length - 1;
            }
        }
    }

    public jumpByPercent(percent:number):void {
        var step:number = this.getStep(100);
        this.active = Math.floor(percent / step);
    }

    private addToLoadedPages(index:number) {
        this.getByIndex(index).preLoad();
        this.loadedPages.push(index);
    }

    private removeCollection():void {
        this.collection.forEach(function (slide:SlideI) {
            slide.destroy();
        });
        this.collection = [];
        this.rejectedPages = [];
        this.loadedPages = [];
    }

    public getPage():number {
        console.error("Метожд не переопределен!");
        return 0;
    }

    public initCollection():void {
        if (this.singleMode) {
            this.init(this.content.filter(function (slideContent:string, index:number) {
                if (slideContent.indexOf("--rejected content--") != -1) {
                    this.rejectedPages.push(index);
                    return false;
                } else {
                    return true
                }
            }, this).map(function (slideContent:string) {
                return Head.getWrapper(slideContent, "center");
            }, this));
        } else {
            var result:Array<string> = [], str:string;
            for (var i = 0, len = this.content.length; i < len; i += 2) {
                str = Head.getWrapper(this.content[i], "left");
                if (this.content[i + 1]) {
                    str += Head.getWrapper(this.content[i + 1], "right");
                }
                result.push(str);
            }
            this.init(result);
        }
    }

    public jumpByPage(page:number):void {
        alert("Метод не переопределен!");
    }

    public init(content:Array<string>):void {
        alert("Метод не переопределен!");
    }


    static getWrapper(slideContent:string, side:string):string {
        return "<div class='slide-wrapper " + side + "'>" + slideContent + "</div>";
    }
}
export = Head;