/// <reference path="../../../../interface/DomLite.d.ts" />

interface sizeI {
    width: number;
    height: number;
}

interface ColuminizerI {

    create(target:$i):ColuminizerI;
    setContent(content:$i):ColuminizerI;
    pause():ColuminizerI;
    play():ColuminizerI;

    initKibble():ColuminizerI;

    setSize(size:sizeI):ColuminizerI;

    getSize():sizeI;

    kibbleContent(needfast:boolean):void;

    getState():string;
    close():ColuminizerI;

}