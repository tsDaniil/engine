/// <reference path="./EpubI.d.ts" />
/// <reference path="./HeadI.d.ts" />
/// <reference path="../../../../interface/DomLite.d.ts" />

interface SlideI {

    parent:HeadI;

    hide(direction?:number):void;
    show(direction?:number):void;

    preLoad():void;

    remove():void;

    getEpub():EpubI;

    destroy():void;

    getNodes():$$;

}