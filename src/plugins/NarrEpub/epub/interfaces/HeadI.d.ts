/// <reference path="./EpubI.d.ts" />
/// <reference path="../../../../interface/CollectionI.d.ts" />

interface HeadI extends CollectionI {

    singleMode:boolean;
    rejectedPages:Array<number>;
    next():void;
    prev():void;
    jumpTo(page:number):void
    getActiveIndex():number;
    hasNext():boolean;
    hasPrev():boolean;
    has(slide:number):boolean;
    hide(direction?:number):void;
    show(direction?:number):void;
    getEpub():EpubI;
    setSingePageMode(mode:boolean):void;
    getProgress(step:number):number;
    jumpByPercent(percent:number):void;

    init(content:Array<string>):void;
    getPage():number;

    preLoad():void;

    /**
     * Только для епаба из пдф
     * @param page
     */
    jumpByPage(page:number):void;

}