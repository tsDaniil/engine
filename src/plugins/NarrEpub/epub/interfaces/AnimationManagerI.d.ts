/// <reference path="../../../../interface/DomLite.d.ts" />
/// <reference path="../interfaces/SlideI.d.ts" />

interface AnimationManagerI {

    show(slide:SlideI, direction?:boolean):void;
    hide(slide:SlideI, direction?:boolean):void;
    addToDom(slide:SlideI, direction?:boolean):void;

}