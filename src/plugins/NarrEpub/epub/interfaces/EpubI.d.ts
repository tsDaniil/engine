/// <reference path="../../../../interface/Utils.d.ts" />
/// <reference path="../../../../interface/DomLite.d.ts" />
/// <reference path="./AnimationManagerI.d.ts" />

interface StateI {

    activeHead:number;
    activeSlide:number;
    fontSize:number;
    progress:number;
    page:number;

}

interface EpubI {

    animanions:boolean;
    speed:number;
    node:$i;

    next():void;
    prev():void;
    jumpTo(page:number):void;
    onChange(callback:(StateInt, progress:number) => void):void;
    getState():StateI;
    getProgress():number;
    change():void;
    loadByState(state:StateI):void;
    setSingePageMode(mode:boolean):void;
    jumpByPercent(prercent:number):void
    start():void;
    redrow():void;
    showPreloader():void;
    hidePreloader():void;
    setNextFontSize():void;
    setPrevFontSize():void;
    getStep():number;
    scale(coff:number, left:number, top:number, size:SizeI):void;
    getCoff():number;
    dropScale():void;
    jumpByPage(page:number):void;
    getAnimationManager():AnimationManagerI
    getPageMode():boolean;



}