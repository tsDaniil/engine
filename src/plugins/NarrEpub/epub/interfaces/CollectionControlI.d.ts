/// <reference path="../../../../interface/CollectionI.d.ts" />

interface CollectionControlI extends CollectionI {

    next():void;
    prev():void;
    jumpTo(index:number):void;

}