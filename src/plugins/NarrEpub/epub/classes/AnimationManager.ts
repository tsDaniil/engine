/// <reference path="../interfaces/AnimationManagerI.d.ts" />
/// <reference path="../interfaces/EpubI.d.ts" />
/// <reference path="../interfaces/SlideI.d.ts" />
/// <reference path="../../../../interface/DomLite.d.ts" />

import Module = require("../../../../utils/base/Module");
import $ = require("$");
import UserData = require("../../../../utils/app/user/UserData");

class AnimationManager extends Module implements AnimationManagerI {

    private epub:EpubI;
    private animator = new Animator(this);

    private node:$i;

    public get animationSpeed():number {
        return this.epub.speed;
    }

    private isNeedAnimation():boolean {
        return this.epub.animanions;
    }

    constructor(epub:EpubI) {
        super();
        this.epub = epub;
        this.node = $.createElement("div", "slide", true);
        this.epub.node.append(this.node);
    }

    public addToDom(slide, direction) {
        var nodes = slide.getNodes();
        if (nodes.length > 1) {
            if (direction) {
                this.node.append(nodes[0]);
                this.node.prepend(nodes[1]);
            } else {
                this.node.prepend(nodes[0]);
                this.node.append(nodes[1]);
            }
        } else {
            if (direction) {
                this.node.prepend(nodes);
            } else {
                this.node.append(nodes);
            }
        }
    }

    public show(slide:SlideI, direction:boolean):void {
        if (!this.isNeedAnimation() || typeof(direction) == "undefined" || !this.node.children().length) {
            this.node.empty();
            this.node.append(slide.getNodes());
            this.childrenToTop();
        } else {
            this.animator.setDirection(direction);
            if (UserData.isWindows()) {
                this.addToDom(slide, direction);
            }
            this.animator.addShowSlide(slide);
        }
    }

    public hide(slide:SlideI, direction:boolean):void {
        if (!this.isNeedAnimation() || typeof(direction) == "undefined") {
            this.node.empty();
        } else {
            this.animator.addHideSlide(slide);
        }
    }

    private childrenToTop():void {
        this.node.children().forEach(function (slide) {
            slide.addClass("visible");
        });
    }

}

class Animator {

    private hideSlide:SlideI = null;
    private showSlide:SlideI = null;
    private animation:AnimationProgressI;
    private direction:boolean;
    private manager;

    constructor(AnimationManager) {
        this.manager = AnimationManager;
        window["animator"] = this;
    }

    public addHideSlide(slide:SlideI):void {
        this.dropAnimation();
        this.hideSlide = slide;
    }

    public addShowSlide(slide:SlideI):void {
        this.showSlide = slide;

        var start = function () {
            if (this.hasSlides()) {
                if (this.manager.epub.singlePageMode) {
                    this.singleAnimate();
                } else {
                    this.animate();
                }
            } else {
                this.showSlide.getNodes().removeClass("toShow").addClass("visible");
            }
        }.bind(this);

        if (UserData.isWindows()) {
            start();
        } else {
            setTimeout(start, 100);
        }
    }

    public
    setDirection(direction:boolean):void {
        this.direction = direction
    }

    private hasSlides():boolean {
        return !!(this.showSlide && this.hideSlide);
    }

    private dropAnimation():void {
        if (this.animation) {
            this.animation.end();
            delete this.animation.callback;
            this.showSlide.getNodes().forEach(function (node) {
                node.node.removeAttribute("style")
            });
            this.animation = null;
            this.hideSlide = null;
            this.showSlide = null;
        }
    }

    private singleAnimate():void {

        var animatedSlide, start, end, toHide;

        toHide = this.hideSlide.getNodes().first();

        if (this.animation) {
            this.animation.end();
        }

        if (this.direction) {
            animatedSlide = toHide;
            start = 360;
            end = 270;
        } else {
            animatedSlide = this.showSlide.getNodes().first();
            start = 270;
            end = 360;
        }

        this.animation = $.animate({
            duration: this.manager.animationSpeed,
            step: function (progress) {
                animatedSlide.css("transform", "translate3d(0px, 0px, 1px) rotate3d(0, 1, 0, " + (start + ((end - start) * progress)) + "deg)");
            },
            callback: function () {
                animatedSlide.node.style.cssText = "";
                this.hideSlide.getNodes().removeClass("visible");
                this.showSlide.getNodes().addClass("visible");
                if (UserData.isWindows()) {
                    toHide.remove();
                }
                this.animation = null;
                this.direction = null;
                this.hideSlide = null;
                this.showSlide = null;
            }.bind(this)
        });
    }

    private animate():void {

        var that = this;

        var toHide, toShow, hideEndDeg, showStartDeg, endShow, hideStartDeg;

        if (that.direction) {
            hideStartDeg = 360;
            toHide = that.hideSlide.getNodes().last();
            toShow = that.showSlide.getNodes().first();
            hideEndDeg = 270;
            showStartDeg = 90;
            endShow = 0;
        } else {
            toHide = that.hideSlide.getNodes().first();
            toShow = that.showSlide.getNodes().last();
            hideStartDeg = 0;
            hideEndDeg = 90;
            showStartDeg = 270;
            endShow = 360;
        }

        toShow.css("transform", "translate3d(0px, 0px, 1px) rotate3d(0, 1, 0, " + showStartDeg + "deg)");

        that.animation = $.animate({
            duration: that.manager.animationSpeed,
            step: function (progress:number) {
                toHide.css("transform", "translate3d(0px, 0px, 1px) rotate3d(0, 1, 0, " + (hideStartDeg + ((hideEndDeg - hideStartDeg) * progress)) + "deg)");
            },
            callback: function () {

                that.hideSlide.getNodes().removeClass("visible").addClass("toShow");
                that.showSlide.getNodes().removeClass("toShow").addClass("visible");

                that.animation = $.animate({
                    duration: that.manager.animationSpeed,
                    step: function (progress:number) {
                        toShow.css("transform", "translate3d(0,0,1px) rotate3d(0, 1, 0, " + (showStartDeg + ((endShow - showStartDeg) * progress)) + "deg)");
                    },
                    callback: function () {

                        that.hideSlide.getNodes().removeClass("toShow");
                        if (UserData.isWindows()) {
                            that.hideSlide.getNodes().remove();
                        }
                        toHide.node.removeAttribute("style");
                        toShow.node.removeAttribute("style");
                        that.animation = null;
                        that.showSlide.getNodes().addClass("visible");
                        that.hideSlide = null;
                        that.showSlide = null;

                    }
                });

            }
        })

    }

}

export = AnimationManager;