/// <reference path="../../interfaces/HeadI.d.ts" />
/// <reference path="../../interfaces/SlideI.d.ts" />
/// <reference path="../../../../../interface/DomLite.d.ts" />

import Slide = require("../../base/Slide");

interface sizeI {
    width: number;
    height: number;
}

class PDFSlide extends Slide implements SlideI {

    private coff:number = 0;
    private targetSize:sizeI;

    constructor(parent:HeadI, targetNode:$i, slideContent:string) {
        super(parent, targetNode, slideContent);
        this.setHandlers();
    }

    private setHandlers():void {
        this.on("Epub:Slide:show", this.afterShow)
    }

    private afterShow():void {

        var targetSize:sizeI, realSize:sizeI;

        if (!this.getEpub().getCoff()) {
            targetSize = this.getTargetSize();
            realSize = this.getNodeSize();

            var coff = Math.min(targetSize.width / realSize.width, targetSize.height / realSize.height);
            this.getEpub().scale(coff, ((targetSize.width - realSize.width) / 2), ((targetSize.height - realSize.height) / 2), realSize);
        }
    }

    private getNodeSize():sizeI {
        var width = this.node.last().width();
        if (!this.parent.singleMode) {
            width = width * 2
        }
        return {
            width: width - 1,
            height: this.node.last().height()
        }
    }

    private getTargetSize():sizeI {
        var targetParent = this.targetNode.parent();
        return {
            width: targetParent.width(),
            height: targetParent.height()
        }
    }

    public destroy():void {
        this.remove();
        this.off();
    }
}
export = PDFSlide;