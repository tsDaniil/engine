/// <reference path="../../interfaces/HeadI.d.ts" />
/// <reference path="../../interfaces/EpubI.d.ts" />
/// <reference path="../../interfaces/SlideI.d.ts" />
/// <reference path="./PDFSlide" />
/// <reference path="../../../../../interface/DomLite.d.ts" />

import PDFSlide = require("./PDFSlide");
import Head = require("../../base/Head");
import $ = require("$");

class PDFEpub extends Head implements HeadI {

    constructor(parent:EpubI, content:Array<string>, node:$i, slinglePage:boolean) {
        super(parent, content, node, slinglePage);
        this.initCollection();
    }


    public init(content:Array<string>):void {

        content.forEach(function (slide:string) {

            this.collection.push(new PDFSlide(this, this.node, slide));

        }, this);

    }

    public jumpByPercent(percent:number):void {
        var step:number = this.getStep(100);
        this.active = Math.max(Math.floor(percent / step) - 1, 0);
        if (!$.hasInArray(this.loadedPages, this.active)) {
            this.getActive().preLoad();
        }
    }

    public jumpByPage(page:number):void {
        if (this.singleMode) {
            this.active = page - this.getRejectedCount(page);
        } else {
            this.active = Math.floor(page/2);
        }
        if (!$.hasInArray(this.loadedPages, this.active)) {
            this.getActive().preLoad();
        }
    }

    private getRejectedCount(page:number):number {
        var count:number = 0;
        this.rejectedPages.some(function (index) {
            if (index < page) {
                count++;
            } else {
                return true;
            }
        });
        return count;
    }

    public getPage():number {
        if (this.singleMode) {
            return this.active;
        } else {
            return this.active * 2
        }
    }

}

export = PDFEpub;