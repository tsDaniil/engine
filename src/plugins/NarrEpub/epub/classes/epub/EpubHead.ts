/// <reference path="../../interfaces/HeadI.d.ts" />
/// <reference path="../../interfaces/SlideI.d.ts" />
/// <reference path="../../interfaces/ColuminizerI.d.ts" />
/// <reference path="../../../../../interface/require.d.ts" />

import Collection = require("../../../../../utils/base/Collection");
import Head = require('../../base/Head');
import EpubSlide = require("../../base/Slide");
import Downloader = require("../../../../../utils/utils/Downloader");
import $ = require("$");

class EpubHead extends Head implements HeadI {

    private columinizer:ColuminizerI;
    private isLoading:boolean = false;
    private contentNode:$i;
    private contentText:string;
    private isImagesLoaded:boolean = false;
    private slideContainer:$i = $.createElement("div", "slide-container", true);
    private fontSize:number;

    constructor(parent:EpubI, content:string, node:$i, singlePage:boolean) {
        super(parent, [content], node, singlePage);
        this.view.append(this.slideContainer);
        this.contentText = content;
        this.loadColuminizer();
    }

    public init(content:any):void {
        var that = this;
        content.forEach(function (slide:$i) {
            that.collection.push(new EpubSlide(that, that.node, slide.html()));
            that.collection[that.collection.length - 1].preLoad();
        });
    }

    public jumpTo(page:number):void {
        if (this.has(page)) {
            var direction:number = page - this.active;
            this.hide(direction);
            this.active = page;
            this.setLoadedPages();
            this.show(direction);
            this.getEpub().change();
        } else {
            this.show();
        }
    }

    public hide(direction?:number) {
        if (this.hasCollection()) {
            this.getActive().hide(direction);
        }
    }

    public show(direction?:number) {
        if (this.hasCollection()) {
            this.getActive().show(direction);
        } else {
            if (!this.isLoading) {
                this.createCollection(direction);
            }
        }
    }

    private createCollection(direction?:number):void {
        this.getEpub().showPreloader();
        var that = this;
        this.contentNode = $.parseHTML(this.contentText, true).addClass("slide");
        if (!this.isImagesLoaded) {
            this.loadImages(function () {
                that.creaeSlides(direction);
            });
        } else {
            this.creaeSlides(direction);
        }
    }

    private creaeSlides(direction?:number):void {
        this.setParamsToColuminizer();
        this.kibble();
        this.initCollection();
        this.show(direction);
        this.getEpub().hidePreloader();
    }

    public initCollection():void {
        if (this.singleMode) {
            this.init(this.slideContainer.children());
        } else {
            var content:$$ = this.slideContainer.children();
            var slide:$i;
            var result:$$ = $.createCollection([]);
            for (var i = 0, len = content.length; i < len; i += 2) {
                slide = $.createElement("div", "", true);
                slide.append(content[i].clone().addClass("slide-wrapper left"));
                if (this.content[i + 1]) {
                    slide.append(content[i + 1].clone().addClass("slide-wrapper right"));
                }
                result.push(slide);
            }
            this.init(result);
        }
    }

    private setParamsToColuminizer():void {
        var that = this;
        this.onLoad(function () {
            that.columinizer.setContent(that.contentNode);
            that.columinizer.setSize(that.getColuminizerSize());
        });
    }

    private hasCollection():boolean {
        return !!this.collection.length;
    }

    private loadColuminizer():void {
        var that = this;
        require(["./columinizer"], function (Columinizer:ColuminizerI) {
            that.columinizer = Columinizer.create(that.slideContainer);
            that.loaded();
        });
    }

    private loadImages(callback:()=>void):void {
        var images = $.toArray(this.contentNode.find("img"));
        Downloader.loadImages(images.map(function (image:$i) {
            return image.attr("src");
        }), function (result) {
            images.forEach(function (image:$i) {
                var src = image.attr("src");
                if (src in result && result[src].state) {
                    image.width(result[src].image.width);
                    image.height(result[src].image.height);
                } else {
                    result.each(function (value:ImageStateI, resultSrc) {
                        if (resultSrc.indexOf(src) != -1) {
                            image.width(value.image.width);
                            image.height(value.image.height);
                        }
                    });
                }
            });
            this.isImagesLoaded = true;
            callback();
        });
    }

    private kibble():void {
        var that = this;
        this.onLoad(function () {
            $.createCollection([that.slideContainer, that.node]).css("font-size", (100 + that.fontSize) + "%");
            that.slideContainer.empty();
            that.columinizer.initKibble().kibbleContent(true);
        });
    }

    private getColuminizerSize() {
        return {
            height: this.node.height() - 20,
            width: this.singleMode ? this.node.width() - 20 : this.node.width() / 2 - 40
        }
    }

}
export = EpubHead;