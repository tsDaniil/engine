define(["../../../../../utils/base/Base", "$"], function (Base, $) {

    var extend = function (object) {
        var constructor = object.constructor;
        constructor.prototype = new Base();
        constructor.prototype.constructor = constructor;
        for (var method in object) {
            if (object.hasOwnProperty(method) && method != "constructor") {
                if (method != "static") {
                    constructor.prototype[method] = object[method];
                } else {
                    for (var staticMethod in object[method]) {
                        if (object[method].hasOwnProperty(staticMethod)) {
                            constructor[staticMethod] = object[method][staticMethod];
                        }
                    }
                }
            }
        }
        return constructor;
    };

    /**
     * @class Columinizer
     * @extends Base
     */
    var Columinizer = extend({

        /**
         * @constructor
         * @param {{width: Number, height: Number}} size
         * @param {$|String} content
         * @param {$} _target
         * @returns {boolean}
         */
        constructor: function Columinizer(size, content, _target) {

            /**
             * Состояние колуминайзера
             * @type {string}
             */
            this.state = "ready";

            /**
             * Время ожидания при разбивании контента
             * @type {number}
             * @private
             */
            this._waitTime = 100;

            if (!_target) {
                console.log("Не указан целевой элемент");
                return false;
            }

            this._initParams(size, content, _target);

        },

        /**
         * Инициализация ичходных данных
         * @param {Object} size
         * @param {String|HTMLElement} content
         * @param {$} target
         * @returns {Columinizer}
         * @private
         */
        _initParams: function (size, content, target) {

            var defaltData = {
                size: {
                    width: 360,
                    height: 700
                }
            };

            /**
             * Тип NodeType елемента при котором он не TextNode
             * @type {number}
             * @private
             */
            this._ELEMENT_NODE = 1;

            /**
             * Сюда кладем получившиеся колонки
             * @type {$}
             * @private
             */
            this._target = target;

            if (!size) {
                this.size = defaltData.size;
            } else {
                this.size = size;
                if (!this.size.width) this.size.width = defaltData.size.width;
                if (!this.size.height) this.size.height = defaltData.size.height;
            }

            this.setContent(content);

            return this;
        },

        /**
         * Устанавливает новый контент, который надо разбить
         * @param {Element|String} content
         * @returns {Columinizer}
         */
        setContent: function (content) {
            if ($.isString(content)) {
                this.content = $.createElement("div", "", "", content);
            } else {
                this.content = content;
            }
            return this;
        },

        /**
         * Приостанавливает разбивку контента
         * @returns {Columinizer}
         */
        pause: function () {

            if (this.getState() == "worked") {

                this._target.removeClass("kibble");
                this._target.addClass("pause");

                this.state = "pause";

                clearTimeout(this._timeout);

            }

            return this;
        },

        /**
         * Продолжает разбивку контента
         * @returns {Columinizer}
         */
        play: function () {

            if (this.getState() == "pause") {

                this._target.removeClass("pause");
                this._target.addClass("kibble");

                clearTimeout(this._timeout);

                /**
                 * Таймаут для фоного разбивания на слайды
                 * @type {number}
                 * @private
                 */
                this._timeout = setTimeout(function () {

                    this.kibbleContent();

                }.bind(this), this._waitTime);

            }

            return this;

        },

        /**
         * Создаём временные переменные
         * @returns {Columinizer}
         */
        initKibble: function () {

            /**
             * Количество неудачных попыток создания слайда (для того чтобы понять что элемент не влезет в сцену даже один)
             * @type {number}
             * @private
             */
            this._failCount = 0;
            /**
             * Сохраняем оригинальный контент
             * @type {$}
             * @private
             */
            this._originalContent = this.content.clone();
            this._target.html("");

            return this;
        },

        /**
         * Добавляет элемент для отладки контена
         * @private
         */
        _addDebug: function () {
            // Создаем промежуточный блок для формирования очередной колонки
            /**
             * Элемент в котором проверяется текущая колонка
             * @type {HTMLElement}
             * @private
             */
            this._debug = document.createElement('div');
            this._debug.className = "debug";
            // Добавляем промежуточный блок в конечный блок, чтобы учесть наследование стилей при разбиении
            this._target.append(this._debug);
            // Задаем ширину промежуточного блока для вычисления высоты новой колонки
            this._debug.style.width = this.size.width + 'px';
            // Тип inline-block нужен для того, чтобы исключить влияние отступов margin внутренних блоков на вычисляемую высоту очередной колонки
            this._debug.style.display = 'inline-block';
            this._debug.style.verticalAlign = 'top';
        },

        /**
         * Устанавливает новый размер для разбивки колуминайзера
         * @param {Object} size
         * @returns {Columinizer}
         */
        setSize: function (size) {

            this.size = size;
            return this;

        },

        /**
         * Получаем размер блока колуминайзера
         * @returns {{width: number, height: number}}
         */
        getSize: function () {
            return {
                width: this.size.width,
                height: this.size.height
            }
        },

        /**
         * Запускаем разбивку контента
         * @param {Boolean} [needFast] Параметр регулирует в потоке ли просчитаются все слайды,
         * или по очереди в разных потоках
         */
        kibbleContent: function (needFast) {

            this.state = "worked";

            this._target.addClass("kibble");

            if (!this._debug) {
                this._addDebug();
            }

            /**
             * Ссылка либо на _debug либо на элемент внутри него для заполнения _debug актуальной версткой
             * @type {HTMLElement}
             * @private
             */
            this._activeDebug = this._debug;

            this._kibble();
            this._saveColumn();

            if (this._originalContent.node.childNodes.length) {
                if (needFast) {
                    this.kibbleContent(needFast);
                } else {
                    this._timeout = setTimeout(function () {
                        this.kibbleContent();
                    }.bind(this), this._waitTime);
                }
            } else {
                this.close();
                this.trigger("Columinizer:finish");
            }

        },

        /**
         * Сохраняем колонку
         * @private
         */
        _saveColumn: function () {

            var column = this._debug.cloneNode(true);
            var $column = $(column);

            if (!$column.hasText() && !$column.find("img").length) {
                this._debug.parentNode.removeChild(this._debug);
                delete  this._debug;
                return false;
            }

            this._target.append(column);
            column.className = "";

            this._debug.parentNode.removeChild(this._debug);
            delete  this._debug;

            this.trigger("Columinizer:column ready", [column]);

        },

        /**
         * Получаем колонку
         * Рекурсивная функция
         * @param {Node} [checkNode] нода которую проверяем (если не указано - то первый запуск)
         * @returns {Boolean}
         * @private
         */
        _kibble: function (checkNode) {

            var nodes, node, i = 0;

            if (checkNode) {
                nodes = checkNode.childNodes;
            } else {
                nodes = this._originalContent.node.childNodes;
            }

            /**
             * Идем в цикле пока у ноды есть дети
             */
            do {
                node = nodes[i];

                if (!this._isPlaced(node)) {
                    if (this._isElement(node)) {
                        if (node.childNodes.length) {
                            this._setActiveNode(node);
                            return this._kibble(node);
                        } else {
                            this._failCount++;
                            if (this._failCount >= 2) {
                                this._setFuckingSizeForNode(node)
                            }
                            return false;
                        }
                    } else {
                        return this._addTextNode(node);
                    }
                } else {
                    this._failCount = 0;
                }

            } while (nodes.length);

        },

        /**
         * Если это какаянибудь хрень, которая одна не влазит в слайд, то принудительно уменьшаем её
         * Обычно это картинки (случай очень редкий, если стили заданы книгой.
         * От больших картинок защита в NarrEpub)
         * @param {Element} node
         * @returns {string}
         * @private
         */
        _setFuckingSizeForNode: function (node) {

            this._activeDebug.appendChild(node);
            node.style.height = (parseInt(node.offsetHeight) - (this._debug.offsetHeight - this.size.height) - 10) + "px";
            return "yeah!";

        },

        /**
         * Устанавливаем активный элемент внутри _debug в который сейчас идет наполнение
         * @param {Node} node
         * @returns {Columinizer}
         * @private
         */
        _setActiveNode: function (node) {
            var activeNode = node.cloneNode(true);
            activeNode.innerHTML = "";
            this._activeDebug.appendChild(activeNode);
            this._activeDebug = activeNode;
            return this;
        },

        /**
         * Проверяем влезает ли элемент в колонку
         * Если да - оставляем его в колонке
         * Если нет - удаляем его из колонки
         * @param {Node} node
         * @returns {boolean}
         * @private
         */
        _isPlaced: function (node) {

            var parent = node.parentNode;

            this._activeDebug.appendChild(node);

            if (this._debug.offsetHeight < this.size.height) {
                return true;
            } else {
                parent.insertBefore(node, parent.firstChild);
                return false;
            }

        },

        /**
         * Проверяет влезает ли половина текста
         * Если да - проверяет влезает ли след. четверть текта
         * Если нет - Пробуем четверть текста
         * @param {Node|Array} text
         * @param {Node} [node]
         * @returns {Boolean}
         * @private
         */
        _addTextNode: function (text, node) {

            var lastHalf, firstHalf;

            if ($.isArray(text)) {
                lastHalf = text;
                if (text.length == 1) {
                    return false;
                }
            } else {
                /**
                 * Здесь эта переменная равна всему массиву, но после _shareArray это будет последняя половина массива,
                 * а firstHalf - первая
                 * @type {Array}
                 */
                lastHalf = text.textContent.split(/\s/);
                node = text;
            }

            firstHalf = this._shareArray(lastHalf);

            if (firstHalf.length) {
                if (this._isTextPlaced(firstHalf)) {
                    this._replaceParentTextNode(node, firstHalf);
                    return this._addTextNode(lastHalf, node);
                } else {
                    return this._addTextNode(firstHalf, node);
                }
            }

            return false;
        },

        /**
         *
         * @param {Node} node
         * @param {Array} firstHalf
         * @return Columinizer
         * @private
         */
        _replaceParentTextNode: function (node, firstHalf) {

            var content = node.textContent.split(/\s/);
            content.splice(0, firstHalf.length);
            node.textContent = content.join(" ") + " ";
            return this;
        },

        /**
         * Возвращает первую или вторую половину массива
         * @param {Array} array
         * @param {Boolean} [direction] false - первая; true - вторая
         * @returns {Array}
         * @private
         */
        _shareArray: function (array, direction) {

            var length = parseInt(array.length / 2);

            if (direction) {
                return array.splice(length, length + 1);
            } else {
                return array.splice(0, length);
            }

        },

        /***
         * Проверяет влезает ли половина массива
         * Если да - оставляет половину в _debug
         * Если нет - удаляет её
         * @param {Array} array
         * @returns {boolean}
         * @private
         */
        _isTextPlaced: function (array) {

            var textNode = document.createTextNode(array.join(" ") + " ");

            this._activeDebug.appendChild(textNode);

            if (this._debug.offsetHeight < this.size.height) {
                return true;
            } else {
                this._activeDebug.removeChild(textNode);
                return false;
            }

        },

        /**
         * Проеряет тип ноды (элемент или textNode)
         * @param {Node} node
         * @returns {boolean}
         * @private
         */
        _isElement: function (node) {
            return node.nodeType == this._ELEMENT_NODE;
        },

        /**
         * @returns {string}
         */
        getState: function () {
            return this.state;
        },

        /**
         * Прерываем работу колуминайзера
         * @returns {Columinizer}
         */
        close: function () {

            if (this._timeout) {
                clearTimeout(this._timeout);
                delete this._timeout;
            }

            this._target.removeClass("kibble");
            this._target.removeClass("pause");
            this.state = "ready";

            return this;
        },
        static: {
            create: function (target) {
                return new Columinizer(null, null, target);
            }
        }
    });

    return Columinizer;
});