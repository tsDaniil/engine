
import Head = require("../../base/Head");


class EpubManager extends Head implements HeadI {

    constructor(parent:EpubI, content:Array<string>, node:$i, singlePage:boolean) {

        super(parent, content, node, singlePage);

    }

}

export = EpubManager;