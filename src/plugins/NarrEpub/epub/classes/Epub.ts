/// <reference path="../interfaces/EpubI.d.ts" />
/// <reference path="../interfaces/HeadI" />
/// <reference path="../../../../interface/PreloaderI" />

import CollectionControl = require("../base/CollectionControl");
import PDFEpub = require("./pdf/PDFEpub");
import EpubHead = require("./epub/EpubHead");
import Preloader = require("../../../../utils/utils/Preloader");
import $ = require("$");
import AnimationManager = require("./AnimationManager");

class Epub extends CollectionControl implements EpubI {

    public active:number;
    public collection:Array<HeadI>;

    public animanions:boolean;
    public speed:number;
    public node:$i;

    private fontSize:number;
    private changeCallbacks:Array<(state:StateI, progress:number) => any>;
    private singlePageMode:boolean;
    private preloader:PreloaderI = Preloader;
    private started:boolean = false;
    private coff:number = 0;

    private pdfMode:boolean;
    private animationManager;

    constructor(content:Array<string>, node:HTMLElement, pdfMode:boolean, singlePageMode:boolean, changeOptions:any) {
        super();
        this.active = 0;
        this.node = $(node.querySelector(".epub-content"));
        this.collection = [];
        this.fontSize = 50;
        this.changeCallbacks = [];
        this.singlePageMode = singlePageMode;
        this.pdfMode = pdfMode;
        this.animationManager = new AnimationManager(this);

        this.init(content, pdfMode, singlePageMode);
    }

    public getPageMode():boolean {
        return this.singlePageMode;
    }

    public getActive():HeadI {
        return this.getByIndex(this.active);
    }

    public getByIndex(index:number):HeadI {
        return this.collection[index]
    }

    public getAnimationManager() {
        return this.animationManager;
    }

    public start():void {

        if (!this.started) {
            if (this.has(this.active)) {
                this.getActive().preLoad();
                this.getActive().show();
                this.change();
            }
        }

    }

    public jumpTo(page:number):void {
        if (page !== this.active && this.has(page)) {
            var direction:number = page - this.active;
            this.getActive().hide(direction);
            this.active = page;
            this.getActive().show(direction);
            this.change();
        }
    }

    public jumpByPercent(percent:number):void {

        var direction:number = percent - this.getProgress();
        if (!this.pdfMode) {
            this.active = Math.floor(percent / this.getStep());
        }

        this.getActive().hide(direction);
        this.getActive().jumpByPercent(percent);
        this.getActive().show(direction);
        this.change();
    }

    public jumpByPage(page:number):void {
        if (this.pdfMode) {
            this.getActive().hide();
            this.getActive().jumpByPage(page);
            this.getActive().show();
        } else {
            console.error("Метод только для епаба без изменения размера шрифта! 'Epub.jumpByPage'");
        }
    }

    public onChange(callback:(state:StateI, progress:number) => void):void {
        this.changeCallbacks.push(callback);
    }

    public getProgress() {
        return (this.getStep() * this.active) + this.getActive().getProgress(this.getStep());
    }

    public getState() {
        return {
            activeHead: this.active,
            activeSlide: this.getActive().getActiveIndex(),
            fontSize: this.fontSize,
            singlePageMode: this.singlePageMode,
            progress: this.getProgress(),
            page: this.getPage()
        }
    }

    public change():void {
        var state:StateI = this.getState();
        var progress:number = this.getProgress();
        this.changeCallbacks.forEach(function (callback:(state:StateI, progress:number) => void) {
            callback(state, progress);
        });
    }

    public loadByState(state:StateI):void {
        this.getActive().hide();
        this.active = state.activeHead;
        this.getActive().jumpTo(state.activeSlide);
        this.started = true;
    }

    public setSingePageMode(mode:boolean) {
        if (mode != this.singlePageMode) {
            this.singlePageMode = mode;
            this.getActive().hide();
            this.collection.forEach(function (head:HeadI) {
                head.setSingePageMode(mode);
            });
            this.change();
            this.getActive().show();
        }
    }

    public redrow():void {
        this.getActive().hide();
        this.getActive().show();
    }

    public showPreloader():void {
        this.preloader.show();
    }

    public hidePreloader():void {
        this.preloader.hide();
    }

    public setNextFontSize():void {
        this.fontSize = this.getFontSizeBy(true);
        this.redrow();
    }

    public setPrevFontSize():void {
        this.fontSize = this.getFontSizeBy(false);
        this.redrow();
    }

    public scale(coff:number, left:number, top:number, size:SizeI):void {
        if (this.coff != coff) {
            this.coff = coff;
            this.node.css({
                "position": "absolute",
                "left": left + "px",
                "top": top + "px",
                "width": size.width + "px",
                "height": size.height + "px"
            }).dropMatrix().scale(this.coff);
        }
    }

    public getCoff():number {
        return this.coff;
    }

    public dropScale():void {
        this.coff = 0;
    }

    private getFontSizeBy(direction:boolean):number {
        if (direction) {
            this.fontSize = this.fontSize + 25;
        } else {
            this.fontSize = this.fontSize - 25;
        }
        if (this.fontSize < 0) {
            return 0;
        }
        if (this.fontSize > 100) {
            return 100;
        }
        return this.fontSize;
    }

    public getStep() {
        return this.collection.length <= 1 ? 100 : (100 / (this.collection.length - 1));
    }

    private getPage():number {
        if (this.pdfMode) {
            return this.getActive().getPage()
        } else {
            return this.getProgress();
        }
    }

    private init(content:Array<string>, pdfMode:boolean, singlePageMode:boolean):void {

        if (pdfMode) {
            this.collection.push(new PDFEpub(this, content, this.node, singlePageMode));
        } else {
            content.forEach(function (headText:string) {
                this.collection.push(new EpubHead(this, headText, this.node, singlePageMode));
            }, this);
        }

    }

}
export = Epub;