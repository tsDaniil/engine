/// <reference path="../../interface/defaultDataI.d.ts" />
/// <reference path="../../interface/Utils.d.ts" />
/// <reference path="../../interface/templateElementType.d.ts" />


interface MainDataI {

    scenes: Array<MainSceneI>;
    originSize: SizeI;
    preLoadedScenes?: number;
    bookControl?: MainSceneI;

}

interface MainSceneI {

    mainElement: templateElementType;
    originSize: SizeI;

}