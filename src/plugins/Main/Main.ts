/// <reference path="../PluginI.d.ts" />
/// <reference path="./MainData.d.ts" />
/// <reference path="../../interface/EatAtHomeI.d.ts" />

import Module = require("../../utils/base/Module");
import $ = require("$");
import Downloader = require("../../utils/utils/Downloader");
import SceneManager = require("../../utils/app/animationEngine/classes/Scene/SceneManager");
import View = require("../../utils/app/animationEngine/classes/View/View");

class Main extends Module implements PluginI, EatAtHomeI {

    private sceneManager:SceneManagerI = SceneManager;
    private node:$i;

    public run(data:MainDataI):void {

        Downloader.loadEngineStyle("plugins/Main/css/style.css");

        this.createScenes(data);
        this.loaded();
        this.fireEvent("ready");
        this.sceneManager.drawScene();

    }

    public onResize(width:number, height:number) {
        /**
         * TODO запилить ресайз
         */
    }

    public setPage(page:any) {
        /**
         * TODO запилить переход по страницам через навигатор
         */
    }

    public getPageCount():number {
        /**
         * TODO Дописать выдачу количества страниц
         */
        return 0;
    }

    public showRecept():void {
        console.log("EatAtHome.showRecept");
        /**
         * TODO написать показ рецепта
         */
    }

    public showIngredients():void {
        console.log("EatAtHome.showIngredients");
        /**
         * TODO написать показ ингредиентов
         */
    }

    private createScenes(data:MainDataI):void {

        this.sceneManager.createScenes(data, this.view, data.preLoadedScenes || 1);

    }

    public togglePlayVideo():void {
        console.log("Проигрываем действие плагина от data.js! togglePlayVideo");
    }

}
export = Main;