/// <reference path="../interface/ModuleI.d.ts" />

interface PluginI extends ModuleI {

    run(data:any):void;
    onResize(width:number, height:number);
    setPage(page:any);

    getPageCount():number;

}