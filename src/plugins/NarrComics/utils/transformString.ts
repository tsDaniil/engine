/**
 * Created by usercom on 01.10.14.
 */
/// <reference path="../interfaces/transformStringI.d.ts" />

class transformString implements transformStringI { //[1]=>x   [3]=>y              [5] => rotate         [7] =>sX    [9] =>sy
	static construct:any[] = ["translate(" , "0" , "," , "0" , ") rotate(" , "0" , "deg) scale(" , "1" , "," , "1" , ")"];

	public toString(x:number, y:number, rotate:number, scaleX:number, scaleY?:number, metric?:string) {
		if (metric != "px" && metric != "%") metric = "px";
		if (scaleY === undefined) {
			transformString.construct[9] = scaleX;
		} else {
			transformString.construct[9] = scaleY;
		}

		transformString.construct[1] = x + metric;
		transformString.construct[3] = y + metric;
		transformString.construct[5] = rotate;
		transformString.construct[7] = scaleX;

		return transformString.construct.join("");
	}
}

var transform:transformToStringI = new transformString().toString;

export = transform;
