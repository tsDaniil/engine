/// <reference path="../interfaces/FrameManager.d.ts" />
/// <reference path="../interfaces/NarrComics.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />

import Templater = require("../../../utils/app/Templater");
import Transformation = require("./Transformation");
import $ = require("$");

class FrameObject implements FrameManagerI {
	private transform:TransformationI[] = [];
	private view:$i;
	private frames:$i[] = [];
	private curentFrame:FrameI;
	private width:number;
	private height:number;
	private _hide:boolean = true;
	private rColor:number;
	private gColor:number;
	private bColor:number;
	private style:$i;

	constructor() {
		this.style = $.createElement("style", "", true);
		$("head").append(this.style);
	}

	private getColor(color:string):number[] {
		var colors = [];
		switch (color) {
			case "white":
				colors = [255, 255, 255];
				break;
			case "black":
				colors = [0, 0, 0];
				break;
			case "red":
				colors = [255, 0, 0];
				break;
			case "blue":
				colors = [0, 0, 255];
				break;
			case "green":
				colors = [0, 255, 0];
				break;
			default :
				colors = [255, 255, 255];
		}
		return colors;
	}

	public resize(width:number, height:number):void {
		if(!(width && height)) return;
		this.width = width;
		this.height = height;
		var x:number = width / 2;
		var y:number = height / 2;
		this.frames[0].width(x).height(height);
		this.frames[1].width(x).height(height);
		this.frames[2].width(width).height(y);
		this.frames[3].width(width).height(y);
	}

	private resizeCurentFrame():void {
		if (this.curentFrame) this.calculate(this.curentFrame);
	}

	public hide(time:number):void {
		if (!this._hide) {
			// this._hide = true;
			// this.view.addClass("hide");
			this.applyFrameTransform({x: -150, y: -150, width: this.width + 300, height: this.height + 300}, 1, "white");
			this.transform[0].setTime(time).apply();
			this.transform[1].setTime(time).apply();
			this.transform[2].setTime(time).apply();
			this.transform[3].setTime(time).apply();
		}
	}

	public show(time:number):void {
		if (this._hide) {
			this._hide = false;
			this.view.removeClass("hide");
		}
	}

	private applyFrameTransform(frameSize:rectI, t:number, color:string):void {
		if (!t) t = 0;
		if (!color) color = "white";
		var colors = this.getColor(color);

		if (colors[0] != this.rColor || colors[1] != this.gColor || colors[2] != this.bColor) {
			this.rColor = colors[0];
			this.gColor = colors[1];
			this.bColor = colors[2];
			this.style.html(Templater.getTemplate("frame.tpl.html", {rColor: this.rColor, gColor: this.gColor, bColor: this.bColor}));
		}

		this.transform[0].setPos((-1 * this.width / 2 + frameSize.x + 30) + "px", "0px", "0px").setTime(t).add();
		this.transform[1].setPos((frameSize.x + frameSize.width - 30 ) + "px", "0px", "0px").setTime(t).add();
		this.transform[2].setPos("0px", (-1 * this.height / 2 + frameSize.y + 30) + "px", "0px").setTime(t).add();
		this.transform[3].setPos("0px", (frameSize.y + frameSize.height - 30) + "px", "0px").setTime(t).add();
	}


	public createFrame(parentView:$i, width:number, height:number):void {
		this.view = $.createElement("div", "frameView hide", true);
		this.frames[0] = $.createElement("div", "frameLeft frame ", true);
		this.transform[0] = new Transformation(this.frames[0]);

		this.frames[1] = $.createElement("div", "frameRight frame", true);
		this.transform[1] = new Transformation(this.frames[1]);

		this.frames[2] = $.createElement("div", "frameTop frame", true);
		this.transform[2] = new Transformation(this.frames[2]);

		this.frames[3] = $.createElement("div", "frameBottom frame", true);
		this.transform[3] = new Transformation(this.frames[3]);


		this.view.append(this.frames[0]);
		this.view.append(this.frames[1]);
		this.view.append(this.frames[2]);
		this.view.append(this.frames[3]);

		parentView.append(this.view);

		this.resize(width, height);
	}

	public calculate(frame:FrameI, time?:number):void {

		if (time == undefined) time = frame.time;
		var frameSize:rectI = { x: frame.rect.x, y: frame.rect.y, width: 1, height: 1 };
		var winRate = this.width / this.height;
		var frameRate = frame.rect.width / frame.rect.height;

		if (winRate < frameRate) {
			frameSize.width = this.width;
			frameSize.height = this.width * frame.rect.height / frame.rect.width;
		} else {
			frameSize.height = this.height;
			frameSize.width = this.height * frameRate;
		}
		frameSize.x = (this.width - frameSize.width) / 2;
		frameSize.y = (this.height - frameSize.height) / 2;

		this.applyFrameTransform(frameSize, time, frame.color);
	}
}

var frame:FrameManagerI = new FrameObject();
export = frame;