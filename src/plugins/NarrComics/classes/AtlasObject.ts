/// <reference path="./../interfaces/AtlasObject" />
/// <reference path="../interfaces/NarrComics.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />

import PlatformManager = require("../../../utils/app/PlatformManager");
import $ = require("$");
import Downloader = require("../../../utils/utils/Downloader");
import Preloader = require("../../../utils/utils/Preloader");
import Transformation = require("./Transformation");
import Modal = require("../../../utils/utils/Modal");
import UserData = require("../../../utils/app/user/UserData");

class AtlasObject implements AtlasObjectI {

    public bgSound;
    private loadCounter:number = 0;
    public transform:TransformationI = new Transformation();
    public data:AtlasI;
    public _cashed:boolean = false;
    public loaded:boolean = false;
    public srcArray:string[] = [];
    public callback:()=>void = function () {
    };
    public casheImg:Array<HTMLImageElement> = [];
    public context:CanvasRenderingContext2D;
    public scale:number;
    public error:boolean = false;
    private firstLoad:boolean = true;

    constructor(data:AtlasI) {
        this.data = data;
        if (this.data) {
            for (var i = 0; i < this.data.imgs.length; i++) {
                this.srcArray.push(this.data.imgs[i].src);
            }
        }
        if (this.srcArray.length == 0) this.loaded = true;
    }


    public progress(load, size) {
        console.log(load, size);
    }

    public _onLoad(message:ImageDataI):void {
        if (this.loaded) return;

        for (var i = 0; i < this.data.imgs.length; i++) {
            for (var img in message) {
                if (!message.hasOwnProperty(img)) continue;
                if (img.indexOf(this.data.imgs[i].src) + 1) {

                    if (!message[img].state) {

                        if (this.loadCounter++ == 2) {
                            this.error = true;
                            this.loadCounter = 0;
                            this.callback ? this.callback() : console.log("Загрузил кртинки");
                            return undefined;
                        }

                        console.log("Заново качаем");
                        Downloader.loadImages(this.srcArray, this._onLoad.bind(this), 100, this.progress.bind(this));
                        return undefined;
                    }

                    if (img.indexOf("logo.png") + 1 && this.firstLoad) {
                        var tmp = message[img].image.width / message[img].image.height;
                        this.data.imgs[i].rect.width = this.data.imgs[i].rect.height * tmp;
                        this.firstLoad = false;
                    }

                    this.casheImg[i] = message[img].image;
                    break;
                }
            }
        }
        this.scale = this.casheImg[0].height / this.data.imgs[0].rect.height;
        this.loaded = true;
        this.callback ? this.callback() : console.log("Загрузил кртинки");
        this.callback = function () {
        };
    }


    public loadTo(context:CanvasRenderingContext2D):void {
        if (!context || !this.data || !this.data.rect) {
            console.log("Некуда рисовать!!! Или неизвестен размер");
            return undefined;
        }
        this.error = false;
        this.loadCounter = 0;
        this.context = context;
        this.context.canvas.width = 1;
        this.context.canvas.height = 1;
        this.transform.setNode($(this.context.canvas));
        this.load();
        this._cashed = true;

    }

    public draw():void {
        if (!this.context || !("drawImage" in this.context)) return;
        if (this.srcArray.length == 0) {
            this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
            return;
        }

        var slowDevice = !UserData.isModernWebkit();


        if (this.data.imgs.length == 1 && this.casheImg[0]) {
            var x = this.data.imgs[0].rect.x * this.scale;
            var y = this.data.imgs[0].rect.y * this.scale;
            var w = this.casheImg[0].width;
            var h = this.casheImg[0].height;

            this.context.canvas.width = 1;
            this.context.canvas.height = 1;

            this.context.canvas.parentElement.appendChild(this.casheImg[0]);
            this.casheImg[0].width = this.data.rect.width * this.scale;
            this.casheImg[0].height = this.data.rect.height * this.scale;
            this.transform.setNode($(this.casheImg[0]));
            //		this.context.canvas.width = this.data.rect.width * this.scale;
            //      this.context.canvas.height = this.data.rect.height * this.scale;
            //      this.context.drawImage(this.casheImg[0], x, y, w, h );

        } else {

            this.context.canvas.width = this.data.rect.width * this.scale;
            this.context.canvas.height = this.data.rect.height * this.scale;

            for (var i = 0; i < this.data.imgs.length; i++) {

                var x = this.data.imgs[i].rect.x * this.scale;
                var y = this.data.imgs[i].rect.y * this.scale;
                var w = this.data.imgs[i].rect.width * this.scale;
                var h = this.data.imgs[i].rect.height * this.scale;

                if (!this.casheImg[i]) continue;

                this.context.drawImage(this.casheImg[i], x, y, w, h);

            }
        }
        this.afterDraw();
    }

    public afterDraw():void {

    }

    public onLoad(callback:()=>void, context?:any):void {
        this.callback = context ? callback.bind(context) : callback;
    }

    public load():void {
        if (this.loaded) {
            this.draw();
        } else {
            Downloader.loadImages(this.srcArray, this._onLoad.bind(this), 60000);
        }
    }

    public cashed():boolean {
        return this._cashed;
    }

    public delete():void {
        if (this.context.canvas.width < 2 && this.casheImg[0]) {
            if (this.casheImg[0].parentElement) {
                this.context.canvas.parentElement.removeChild(this.casheImg[0]);
            }
        }
        this.context = undefined;
        this.callback = function () {
        };
        this.casheImg = [];
        this.loaded = false;
        this._cashed = false;
    }

    public onPageStart():void {
        console.log("pageStart");
    }


    public animate(x:number, y:number, scale:number, time:number):void {  //TODO ADD ANIMATION
        this.transform.setTime(time).setScale(scale + "," + scale).setPos(x + "px", y + "px", "0px").add();
    }

    public transformation(x:number, y:number, scale:number):void {
        this.transform.setScale(scale + "").setPos(x + "px", y + "px", "0px").setTime(0).apply();
    }

}

export = AtlasObject;