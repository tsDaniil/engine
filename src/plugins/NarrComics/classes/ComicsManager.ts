/**
 * Created by usercom on 01.10.14.
 */
/// <reference path="../interfaces/comicsManager.d.ts" />
/// <reference path="../interfaces/Arrow.d.ts" />
/// <reference path="../interfaces/AtlasObject.d.ts" />
/// <reference path="../interfaces/dataI.d.ts" />
/// <reference path="../interfaces/Page.d.ts" />
/// <reference path="../interfaces/CalculateFrame.d.ts" />
/// <reference path="../interfaces/FrameManager.d.ts" />
/// <reference path="../../../interface/DownloaderI.d.ts" />
/// <reference path="../../../interface/PlatformManagerI.d.ts" />
/// <reference path="../../../interface/PreloaderI.d.ts" />
/// <reference path="../interfaces/Arrow.d.ts" />

import Preloader = require("../../../utils/utils/Preloader");
import Page = require("./Page");
import FrameManager = require("./FrameManager");
import Atlas = require("./AtlasObject");
import Downloader = require("../../../utils/utils/Downloader");
import PlatformManager = require("../../../utils/app/PlatformManager");
import CalculateFrame = require("./CalculateFrame");
import PagesManager = require("./PagesManager");
import CoverCreator = require("./CoverCreator");
import $ = require("$");
import Audio = require("../../../utils/app/Audio");

var newmanger:ComicsManagerI;

class ComicsManager implements ComicsManagerI {

	private endPageNumber:number = 0;
	private mainView:$i;
	private data:contentComicsI[];

	public create(mainView:$i, data:dataComicsI):void {

		var frames = [],
			atlases = [];

		this.mainView = mainView;

		var curentPage = new Page(0, mainView, mainView.width(), mainView.height());
		var nextPage = new Page(1, mainView, mainView.width(), mainView.height());


		//Downloader.loadStyle("plugins/NarrComics/css/comics.css?" + new Date().getTime());
		this.data = data.content;
		this.createCoverData();
		this.setFinalData();
		this.endPageNumber = this.data.length;

		for (var i = this.endPageNumber; i--;) {
			var obj:contentComicsI = this.data[i];
			frames[i] = this.creatFrameData(obj.frames);
			atlases[i] = this.creatAtlas(obj.atlas, i);
			atlases[i].bgSound = obj.bgSound;
			if(obj.bgSound && "src" in obj.bgSound) Audio.addBg(obj.bgSound["src"], obj.bgSound["src"]);
		}



		frames[1][0].time = 0.3;
		PagesManager.addAtlas(atlases);
		PagesManager.addFrames(frames);
		PagesManager.addPages(curentPage, nextPage);

		atlases = undefined;
		frames = undefined;

		FrameManager.createFrame(this.mainView, this.mainView.width(), this.mainView.height());
	}

	public toState(state:stateI):void {
		PagesManager.toPage(state.pageNumber);
		PagesManager.toFrame(state.frameNumber);
	}

	public next():void {
		PagesManager.nextFrame();
	}

	public perv():void {
		PagesManager.lastFrame();
	}

	public toPage(pageNumber:number):void {
		PagesManager.toPage(pageNumber);
	}

	public toFrame(frameNumber:number):void {
		PagesManager.toFrame(frameNumber);
	}

	public  onChange(callback:(state?:stateI) => void):void {

	}

	public  getState():stateI {
		return PagesManager.curentState();
	}

	public  change():void {

	}

	private creatFrameData(framesOnPage:FrameI[]):FrameI[] {
		framesOnPage = this.framePutOffset(framesOnPage, 10);
		if (!framesOnPage || framesOnPage.length == 0) {
			framesOnPage = [
				{
					"id": 0,
					"rect": {
						"x": 671, "y": 0,
						"width": 824, "height": 768
					},
					"time": 0.3,
					"color": "white",
					"animation": "easeInOutCubic"
				}
			];
		}


		return framesOnPage;
	}

	private creatAtlas(atlasData:AtlasI, i:number):AtlasObjectI {
		if (i == 0) return new CoverCreator(atlasData);
		return  new Atlas(atlasData);
	}

	private createCoverData():void {

		this.data[0].atlas = {
			"rect": {
				"x": 0,
				"y": 0,
				"width": 2000,
				"height": 768
			},
			"imgs": [
				{
					"src": "img/cover/bg.jpg",
					"rect": {
						"width": 2000,
						"height": 768,
						"x": 0,
						"y": 0
					}
				},
				{
					"src": "img/cover/cover_preview.jpg",
					"rect": {
						"width": 750,
						"height": 344,
						"x": 658,
						"y": 232
					}
				},
				{
					"src": "img/cover/ramka_16x16.png",
					"rect": {
						"width": 778,
						"height": 379,
						"x": 644,
						"y": 215
					}
				},
				{
					"src": "img/cover/logo.png",
					"rect": {
						"width": 300,
						"height": 170,
						"x": 658,
						"y": 20
					}
				}
			]
		};
	}

	public createCover():void {
		var cover = PagesManager.getCover();
		cover["addText"]();
	}

	private setFinalData():void {
		this.data[this.data.length - 1].atlas = {
			"rect": {
				"x": 0,
				"y": 0,
				"width": 2200,
				"height": 768
			},
			"imgs": [
				{
					"src": "img/cover/bg.jpg",
					"rect": {
						"width": 2000,
						"height": 768,
						"x": 0,
						"y": 0
					}
				},
				{
					"src": "cover.jpg",
					"rect": {
						"width": 2000,
						"height": 1100,
						"x": 100,
						"y": -120
					}
				}
			]
		};
	}

	private framePutOffset(framesOnPage:FrameI[], offset:number):FrameI[] {
		for (var i = 0; i < framesOnPage.length; i++) {
			var frame:FrameI = framesOnPage[i];
			frame.rect.x -= frame.rect.width * (offset / 155);
			frame.rect.y -= frame.rect.height * (offset / 200);
			frame.rect.width += frame.rect.width * (offset / 100);
			frame.rect.height += frame.rect.height * (offset / 100);
		}
		return framesOnPage;
	}

	public resize(width:number, height:number):void {
		PagesManager.resize(width, height);
	}


}

newmanger = new ComicsManager();

export = newmanger;