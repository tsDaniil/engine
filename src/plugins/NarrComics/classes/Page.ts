/// <reference path="./../interfaces/Page.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="../../../interface/greensock.d.ts" />
/// <reference path="../interfaces/TransformationI.d.ts" />
/// <reference path="../interfaces/Canvas.d.ts" />

import Transformation = require("./Transformation");
import Canvas = require("./Canvas");
import $ = require("$");

class Page implements PageI {
	private pageNumber:number;
	private view:$i;
	private  canvas:CanvasI;
	public transform:TransformationI = new Transformation();

	constructor(pageNumber:number, appendTo:$i, pageWidth?:number, pageHeight?:number, visible?:boolean) {
		this.pageNumber = pageNumber;

		this.view = $.createElement("div", "page", true).width(pageWidth).height(pageHeight);
		this.view.css("overflow","hidden");
		this.canvas = new Canvas();
		this.canvas.appendTo(this.view);

		this.transform.setNode(this.view).setPos("0px", "0px", "0px").setRotate("0deg").setScale("1,1").apply();
		visible ? this.show() : this.hide();
		appendTo.append(this.view);
	}

	public getContext():CanvasRenderingContext2D {
		return this.canvas.context;
	}

	public appendToPage(element:$i) {
		this.view.append(element);
	}

	public moveLeft(time?:number):void {
//       this.view.css("overflow","hidden");
        this.transform.setPos("0%", "0%", "0px").setTime(0).apply();
        this.transform.setPos("-100%", "0%", "0%").setTime(time).add();
//		TweenLite.fromTo(this.view.node, time, {x: "0%"}, {x: "-100%"});
	}

	public moveFromLeft(time?:number):void {
//		this.view.css("overflow","hidden");
//		TweenLite.fromTo(this.view.node, time, {x: "-100%"}, {x: "0%"});

       this.transform.setPos("-100%", "0%", "0px").setTime(0).apply();
       this.transform.setPos("0%", "0%", "0px").setTime(time).add();
	}

	public moveFromRight(time?:number):void {
//		this.view.css("overflow","hidden");
		this.transform.setPos("100%", "0%", "0px").setTime(0).apply();
		this.transform.setPos("0%", "0%", "0px").setTime(time).add();
		//TweenLite.fromTo(this.view.node, time, {x: "100%"}, {x: "0%"});
	}

	public moveRight(time?:number):void {
//       this.view.css("overflow","hidden");
       this.transform.setPos("0%", "0%", "0px").setTime(0).apply();
       this.transform.setPos("100%", "0%", "0px").setTime(time).add();
//		TweenLite.fromTo(this.view.node, time, {x: "0%"}, {x: "100%"});
	}

	public deletePageContent():void {
		this.canvas.context.clearRect(0, 0, this.canvas.context.canvas.width, this.canvas.context.canvas.height);
	}

	public show():void {
		this.view.show();
	}

	public hide():void {
		this.view.hide();
	}

	public resize(width:number, height:number):void {
		this.view.width(width);
		this.view.height(height);
	}

	public indexUp():void {
		//this.view.css("z-index", "10");
	}

	public indexDown():void {
		//this.view.css("z-index", "5");
	}
}

export = Page;