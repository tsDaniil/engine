/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="../interfaces/Arrow.d.ts" />

import $ = require("$");

class Arrow {

	public element:$i;
	private timer;
	private img:$i;

	constructor(className:string,img:string, callback?:()=>void, touchObj?) {
		this.create(className,img, callback, touchObj);
	}

	public create(className:string, img:string, callback?:()=>void, touchObj?):ArrowI {
		this.element = $.createElement("div", className+"div", true);
		this.element.css(touchObj);
		if(callback !== undefined){
			this.element.tap(function(e){e.originEvent.preventDefault(); callback();});
		}
		this.img = $.createElement("img", className, true);
		this.img.attr("src", img);
		this.element.append(this.img);

		return this;
	}

	public pause():ArrowI {
		this.img.removeClass("flash");
		return this;
	}

	public play():ArrowI {
		this.img.addClass("flash");
		return this;
	}

	public hide():ArrowI {
		clearTimeout(this.timer);
		this.pause();
		this.img.css("opacity" , "0");
		return this;
	}

	public show():ArrowI {
		this.img.css("opacity" , "1");
		return this;
	}

	public temporaryHide(time:number):void {
		this.hide();
		this.timer = setTimeout(this.show.bind(this), time * 1000);
	}
}

export = Arrow;