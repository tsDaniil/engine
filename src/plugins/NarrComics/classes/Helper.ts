/// <reference path="./../interfaces/HelperI" />
import UserData = require("../../../utils/app/user/UserData");
import Templater = require("../../../utils/app/Templater");
import Module = require("../../../utils/base/Module");
import $ = require("$");

class Helper extends Module implements HelperI{

	public isShow: boolean = false;
	public container: $i;
	public isCreate: boolean;
	private assincTimer;
	private callback: ()=>void;

	private create():void{
		this.isCreate = true;
		this.container = $.createElement("div", "Helper", true);
		this.view.append(this.container);
		var HtmlText = Templater.getTemplate("helper.tpl.html",{});
		if(UserData.isWeb()) {
			HtmlText.replace("arrowLT", "disableElement");
		}

		this.container.html( HtmlText );
		this.container.on("User:tap", this.del.bind(this));
	}

	public show():void{
		if(this.isShow || this.isCreate) return undefined;
		clearTimeout(this.assincTimer);
		this.assincTimer = setTimeout(this.create.bind(this),300);
	}

	public del(e):boolean{
		if(!this.isCreate) return undefined;
		this.isCreate = false;
		this.isShow = true;
		this.container.off("User:tap");
		this.container.remove();
		e.originEvent.preventDefault();
		e.originEvent.stopPropagation();
		this.onRead();
		return false;
	}

	public onRead():void{


	}


	public onShow(callback: ()=>void): void{
		this.callback = callback;
	}
}

var helper: HelperI = new Helper();

export = helper;