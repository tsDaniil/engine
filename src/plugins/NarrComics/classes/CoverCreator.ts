/**
 * Created by usercom on 14.10.14.
 */
/// <reference path="../interfaces/AtlasObject.d.ts" />
/// <reference path="./AtlasObject" />

import AtlasObject = require("./AtlasObject");
import Downloader = require("../../../utils/utils/Downloader");
import Preloader = require("../../../utils/utils/Preloader");
import Transformation = require("./Transformation");
import CalculateFrame = require("./CalculateFrame");
import PlatformManager = require("../../../utils/app/PlatformManager");
import Helper = require("./Helper");

class CoverCreator extends AtlasObject implements AtlasObjectI {

	private name:string = "Дисней коммикс";
	private episode:string = "Выпуск 0";
	private nameIsLoad:boolean = false;
	private isCreate:boolean = false;
	private textAdded:boolean = false;
	private showHelp:boolean = false;


	constructor(data:AtlasI) {
		super(data);
	}

	private OnceRun(){
		this.draw();
		this.OnceRun = function(){};
	}

	public afterDraw():void{

		this.getText();

		this.context.shadowBlur = 10;
		this.context.shadowColor = "grey";
		this.context.strokeStyle = "white";
		this.context.fillStyle = "black";
		this.context.font = this.scale*50 + "px 'lobster_1.4regular'";
		this.context.lineWidth = 4;
		this.context.strokeText(this.name, this.scale*658, this.scale*675);
		this.context.fillText(this.name, this.scale*658, this.scale*675);

		this.context.fillStyle = "black";
		this.context.font = this.scale*30 + "px 'lobster_1.4regular'";
		this.context.strokeText(this.episode, this.scale*658, this.scale*730);
		this.context.fillText(this.episode, this.scale*658, this.scale*730);
		this.textAdded = true;

	}

	public getText():void{
		var data = PlatformManager.getBookData();

		if (data && "issueName" in data && data.issueName) {
			this.name = data.issueName;
			this.episode = "Выпуск " + data.issueNumber;
		}

		if(data && "showHelp" in data && data.showHelp){
			this.showHelp = true;
		}

	}

	public onPageStart():void{
		if(this.showHelp) this.goToFirstPageHelper();
		this.showHelp = false;
	}

	public addText(data?:EpisodeI):void {
		if(this.load) this.draw();
	}

	private goToFirstPageHelper():void{
		Helper.show();
		setTimeout(function(){
			this.clickDisable = false;
			this.pageFlip = true;
		}.bind(this), 4000);
	}

//	public delete():void{
//		//this.casheImg = [];
//		this._cashed = false;
//		this.textAdded = false;
//	}
}


export = CoverCreator;