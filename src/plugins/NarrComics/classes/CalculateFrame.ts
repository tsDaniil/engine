/// <reference path="../interfaces/CalculateFrame.d.ts" />
/// <reference path="../interfaces/AtlasObject.d.ts" />

class CalculateFrame implements CalculateFrameI {

	public getImgScale(rectWindow:rectI, rectFrame:rectI, scale?:number):rectI {

		scale = scale!==undefined ? scale : 1;

		var x =  scale * rectFrame.x;
		var y =  scale * rectFrame.y;
		var w =  scale * rectFrame.width;
		var h =  scale * rectFrame.height;
		var s = 1;

		var winRate = rectWindow.width / rectWindow.height;
		var frameRate = w / h;

		var frameSize:rectI = { x: 0 , y: 0  , width: 0, height: 0 };

		if (winRate < frameRate) {
			frameSize.width = rectWindow.width;
			frameSize.height = rectWindow.width * (1/frameRate);
			s = rectWindow.width / w;
		} else {
			frameSize.height = rectWindow.height;
			frameSize.width = rectWindow.height * frameRate;
			s = rectWindow.height / h;
		}

		frameSize.x = (rectWindow.width - frameSize.width) /2;
		frameSize.y = (rectWindow.height - frameSize.height) /2;

		return { x: -1*x*s + frameSize.x , y: -1*s*y + frameSize.y , width: s, height: s };
	}

}

var calc:CalculateFrameI = new CalculateFrame();
export = calc;