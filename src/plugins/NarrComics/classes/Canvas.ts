/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="../interfaces/Canvas.d.ts" />

import $ = require("$");

class Canvas {

	public context:CanvasRenderingContext2D;
	public node:$i;

	constructor(view?:$i) {
		this.node = $.createElement("canvas", "canvas", true);
		this.context = this.node.node["getContext"]('2d');
	}

	public appendTo(view:$i):CanvasI {
		view.append(this.node);
		return this;
	}

	public setSize(width:number, height:number):CanvasI {
		this.node.width(width).height(height);
		return this;

	}

	public drawImage(img:HTMLImageElement, sx?:number, sy?:number, swidth?:number, sheight?:number, x?:number, y?:number, width?:number, height?:number):CanvasI {
		arguments[1] = arguments[1] || 0;
		arguments[2] = arguments[2] || 0;
		this.context.drawImage.apply(this.context, arguments);
		return this;
	}

	public toDataURL():CanvasI {
		this.node.node["toDataURL"]();
		return this;
	}

}

export = Canvas;