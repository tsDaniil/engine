/**
 * Created by usercom on 10.10.14.
 */
/// <reference path="../interfaces/TransformationI.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />

import $ = require("$");

/**
 * Клас по работе со строкой трансформации для css
 * и Анимации с помощью transition и дом лайт
 */
class Transformation implements TransformationI {

	static transforms:Array<TransformationI> = [];

	private node:$i;

	private x:number;
	private xMiter:string;
	private y:number;
	private yMiter:string;
	private z:number;
	private zMiter:string;

	private r:number;
	private rMiter:string;

	private scaleX:number;
	private scaleY:number;

	private time:number;

	private css:any = {
		"transform": "",
		"transition": "",
		"transform-origin" : "0 0 0"
	};

	constructor(node?:$i, x?:string, y?:string, rotate?:string, scale?:string, time?:number) {
		this.setNode(node).setPos(x, y).setRotate(rotate).setScale(scale).setTime(time);
	}

	public StartAnimations():void {
		for (var i = Transformation.transforms.length; i--;) {
			if (!Transformation.transforms[i]) continue;
			Transformation.transforms[i].apply();
			Transformation.transforms[i] = undefined;
		}
	}


	public setTime(time:number):TransformationI {
		this.time = time > 0 ? time : 0;
		return this;
	}

	public setPos(x:string, y:string, z?:string):TransformationI {
		x = x || "0px";
		y = y || "0px";
		z = z || "0px";
		this.x = parseFloat(x) || 0;
		this.xMiter = x.replace(this.x.toString(), "") || "px";
		this.y = parseFloat(y) || 0;
		this.yMiter = y.replace(this.y.toString(), "") || "px";
		this.z = parseFloat(y) || 0;
		this.zMiter = y.replace(this.z.toString(), "") || "px";
		return this;
	}

	public setScale(scale:string):TransformationI {
		scale = scale || "1";
		var sc:string[] = scale.split(",");
		this.scaleX = parseFloat(sc[0]) || 1;
		this.scaleY = parseFloat(sc[1]) || parseFloat(sc[0]) || 1;
		return this;
	}

	public setRotate(rotate:string):TransformationI {
		rotate = rotate || "0deg";
		this.r = parseFloat(rotate) || 0;
		this.rMiter = rotate.replace(this.r.toString(), "") || "deg";
		return this;
	}

	public setNode(node:$i):TransformationI {
		this.node = node || undefined;
		return this;
	}

	public getTransformString():string {
		return "translate3d(" + this.x + this.xMiter + "," + this.y + this.yMiter + ",0) rotate3d(0,1,0," + this.r + this.rMiter + ") scale3d(" + this.scaleX + "," + this.scaleY + ",1)";
	}

	public getTransitionString():string {
		return "all " + this.time + "s";
	}

	public apply():TransformationI {
		this.css.transform = this.getTransformString();
		this.css.transition = this.getTransitionString();

		if (this.node) {
			this.node.css(this.css);
			return this;
		}
		this.error(1);
		return undefined;
	}

	public add() {
		Transformation.transforms.push(this);
		return this;
	}

	private error(n:number):void {
		var error = "No errors";
		if (n == 1) error = "Need node";
		if (n == 2) error += " Need true Data";
		console.log(error);
	}
}

export = Transformation;