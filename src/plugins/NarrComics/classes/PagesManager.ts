/**
 * Created by usercom on 12.10.14.
 */
/// <reference path="../interfaces/comicsManager.d.ts" />
/// <reference path="../interfaces/PagesManager.d.ts" />
/// <reference path="../interfaces/CalculateFrame.d.ts" />
/// <reference path="../interfaces/AtlasObject.d.ts" />
/// <reference path="../interfaces/Arrow.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />

interface rectI {
    x:number;
    y:number;
    width:number;
    height:number;
}

import $ = require("$");
import Preloader = require("../../../utils/utils/Preloader");
import Page = require("./Page");
import FrameManager = require("./FrameManager");
import Atlas = require("./AtlasObject");
import Calculate = require("./CalculateFrame");
import Transformation = require("./Transformation");
import PlatformManager = require("../../../utils/app/PlatformManager");
import Arrow = require("./Arrow");
import Audio = require("../../../utils/app/Audio");
import UserData = require("../../../utils/app/user/UserData");

class PagesManager implements PagesManagerI {

    private cashTimer:number;
    private firstBoot:boolean = true;
    private frames:FrameI[][] = [];
    private atlases:AtlasObjectI[] = [];
    private activePage:PageI;
    private preloadPage:PageI;
    private curentPage:number = -1;
    private cashedPage:number = -1;
    private curentFrame:number = 0;
    private viewSize:rectI = {x: 0, y: 0, width: 768, height: 768};
    public clickDisable:boolean = false;
    public pageFlip:boolean = true;
    private frameJump:boolean = true;
    private loading:boolean = false;
    private loadingPage:number = -1;
    private rightArrow:ArrowI;
    private leftArrow:ArrowI;
    private spinerTimer;
    private pageStartTimer;
    private arrowsTrigger:boolean;

    constructor() {
        var view = $(".bigrect");
        this.rightArrow = new Arrow("rightArrow","img/buttons/nav_right_new.png", this.nextFrame.bind(this));
        view.append(this.rightArrow.element);
        this.leftArrow = new Arrow("leftArrow","img/buttons/nav_left_new.png",  this.lastFrame.bind(this));
        view.append(this.leftArrow.element);
        this.rightArrow.show();
    }


    private static onerror():boolean {
        return window.confirm("Соединение с сетью отсутствует.\n Проверьте соединение. Попробовать снова?");
    }

    public nextPage():void {
        if (!this.pageFlip) return undefined;
        this.curentFrame = 0;
        this.toPage(this.curentPage + 1);
    }

    public lastPage():void {
        if (!this.pageFlip) return undefined;

        this.curentFrame = 0;

        this.toPage(this.curentPage - 1);
    }


    public setFirstPage(pageNumber:number):void {
        if (pageNumber != this.loadingPage && this.loadingPage != -1) {
            this.atlases[this.loadingPage].delete();
            this.loadingPage = -1;
            this.loading = false;
        }
        this.rightArrow.play();
        this.leftArrow.play();
        this.pageFlip = true;
        this.frameJump = true;
        this.firstBoot = true;
        this.curentPage = -1;
        this.cashedPage = -1;
        this.curentFrame = 0;
        this.pageLoad(pageNumber);
    }

    public toPage(pageNumber:number):void {

        console.log("from : " + this.curentPage + " => to " + pageNumber);
        if (this.atlases[pageNumber].bgSound && this.atlases[pageNumber].bgSound.src) {
            Audio.setVolumeForSoundTo(this.atlases[pageNumber].bgSound.src, this.atlases[pageNumber].bgSound.volume);
            Audio.playBg(this.atlases[pageNumber].bgSound.src);
        }
        if (!this.pageOutOfRange(pageNumber)) return undefined;
        if (!this.pageLoad(pageNumber)) return undefined;
        clearTimeout(this.cashTimer);
        this.pageFlip = false;
        this.clickDisable = true;


        if (this.firstBoot) {
            this.curentFrame = 0;
            this.swapPages();
            this.activePage.show();
        } else if (pageNumber == this.atlases.length - 1) {
            this.goToFinalPage(pageNumber);
        } else if (pageNumber > this.curentPage) {
            this.goToNextPage(pageNumber);
        } else if (pageNumber < this.curentPage) {
            this.goToPerviosPage(pageNumber);
        }

        this.atlases[pageNumber].draw();

        this.cashAfterAnimate(this.frames[pageNumber][0].time, this.curentPage, pageNumber);

        clearTimeout(this.spinerTimer);
        Preloader.hide();
        if (this.curentPage !== -1) PlatformManager.sectionFinished(this.curentPage);
        this.curentPage = pageNumber;
        this.visibleFrameManager(pageNumber);
        this.toFrame(this.curentFrame, true);
        PlatformManager.setSectionNumber(this.curentPage, this.atlases.length);

        //console.log("On Page: " + this.curentPage);
    }


    private goToPerviosPage(pageNumber:number):void {
        if (pageNumber == 0) {
            this.rightArrow.play();
            this.leftArrow.play();
        }
        this.preloadPage.show();
        this.preloadPage.moveFromLeft(this.frames[pageNumber][0].time);
        this.activePage.moveRight(this.frames[pageNumber][0].time);
    }

    private goToNextPage(pageNumber:number):void {
        this.curentFrame = 0;
        this.preloadPage.show();
        this.preloadPage.moveFromRight(this.frames[pageNumber][0].time);
        this.activePage.moveLeft(this.frames[pageNumber][0].time);
    }

    private goToFinalPage(pageNumber:number):void {
        var that = this;
        this.goToNextPage(pageNumber);
        this.rightArrow.hide();
        this.leftArrow.hide();
        setTimeout(function () {
            PlatformManager.sectionFinished(that.curentPage);
        }, 500);
    }

    private visibleFrameManager(pageNumber:number, time?:number):void {
        if (pageNumber == 0 || pageNumber == (this.atlases.length - 1)) {
            FrameManager.hide((time || 0));
        } else {
            FrameManager.show((time || 0));
        }
    }

    private pageOutOfRange(pageNumber:number):boolean {
        if (pageNumber < 0 || pageNumber >= this.atlases.length) {
            console.log("page " + pageNumber.toString() + " out in range 0 - " + (this.atlases.length - 1).toString());
            return false;
        } else if (pageNumber == this.curentPage) {
            console.log("page " + pageNumber.toString() + " No download - iTself ");
            return false;
        }
        return true;
    }

    private pageLoad(pageNumber:number):boolean {

        if (pageNumber != this.loadingPage && this.loadingPage != -1) {
            this.atlases[this.loadingPage].delete();
            this.loadingPage = -1;
            this.loading = false;
        }

        if (this.loading) return false;

        ///Ошибка скачки
        if (this.atlases[pageNumber].error) {

            if (UserData.getSearch().win8 == "1") return false;

            if (!PagesManager.onerror()) {
                PlatformManager.exit();
                return false;
            }
        }

        this.atlases[pageNumber].error = false;

        if (this.atlases[pageNumber].loaded) return true;

        clearTimeout(this.spinerTimer);
        this.spinerTimer = setTimeout(function () {
            Preloader.show();
        }, 500);

        this.loadingPage = pageNumber;

        this.atlases[pageNumber].onLoad(function () {
            //this.pageFlip = true;
            this.loading = false;
            this.loadingPage = -1;
            this.toPage(pageNumber);
        }, this);

        this.atlases[pageNumber].loadTo(this.preloadPage.getContext());
        this.loading = true;
        this.clickDisable = true;
        this.pageFlip = false;

        return false;
    }


    public nextFrame(event?):void {

        if (event) {
            event.originEvent.stopPropagation();
        }
        if (this.clickDisable || !this.frameJump || this.arrowsTrigger) return undefined;
        this.arrowsTrigger = true;
        setTimeout(function () {
            this.arrowsTrigger = false
        }.bind(this), 100);
        this.toFrame(this.curentFrame + 1);
    }

    public lastFrame(event?):void {

        if (event) {
            event.originEvent.stopPropagation();
        }

        if (this.clickDisable || !this.frameJump || this.arrowsTrigger) return undefined;

        if (this.curentPage == 0 && this.curentFrame == 0) {
            return undefined;
        }
        this.arrowsTrigger = true;
        setTimeout(function () {
            this.arrowsTrigger = false
        }.bind(this), 100);
        this.toFrame(this.curentFrame - 1);
    }

    public toFrame(frameNumber:number, force?:boolean):void {

        var that = this;

        if (this.curentPage < 0 || this.curentPage >= this.atlases.length) return;

        if (this.clickDisable && !force) {
            this.curentFrame = frameNumber;
            return undefined;
        }

        this.frameJump = false;

        if (frameNumber < this.frames[this.curentPage].length && frameNumber > -1) {

            var time = frameNumber == 0 ? 0 : this.frames[this.curentPage][frameNumber].time;

            if (this.curentPage == 0 && frameNumber == 0) {
                this.rightArrow.show();
                this.rightArrow.play();
                this.leftArrow.hide();
            } else if (this.curentPage != this.atlases.length - 1) {
                this.rightArrow.pause();
                this.leftArrow.pause();
                this.rightArrow.temporaryHide(time + 0.5);
                this.leftArrow.temporaryHide(time + 0.5);
            } else {
                this.rightArrow.hide();
                this.leftArrow.show();
                this.leftArrow.play();
            }

            this.visibleFrameManager(this.curentPage, this.frames[this.curentPage][frameNumber].time);

            var tmpRect = Calculate.getImgScale(this.viewSize, this.frames[this.curentPage][frameNumber].rect, this.atlases[this.curentPage].scale);
            this.atlases[this.curentPage].animate(tmpRect.x, tmpRect.y, tmpRect.width, time);


            if ((this.curentPage != 0 && this.curentPage != (this.atlases.length - 1))) {
                FrameManager.calculate(this.frames[this.curentPage][frameNumber]);
            }

            setTimeout(function () {
                new Transformation().StartAnimations();
            }, 10);


            clearTimeout(this.pageStartTimer);
            this.pageStartTimer = setTimeout(this.atlases[this.curentPage].onPageStart.bind(this.atlases[this.curentPage]), time * 1000);

            console.log("page - " + this.curentPage + " this frame - " + frameNumber);

            setTimeout(function () {
                that.frameJump = true;
                that.pageFlip = true;
            }, this.frames[this.curentPage][frameNumber].time * 1000);
            this.curentFrame = frameNumber;
        } else if (frameNumber >= this.frames[this.curentPage].length && this.atlases[this.curentPage + 1]) {

            this.nextPage();

        } else if (frameNumber < 0 && this.atlases[this.curentPage - 1]) {

            this.lastPage();

        } else if (frameNumber > this.frames[this.curentPage].length - 1) {

        } else {
            FrameManager.hide(0);
            console.log("Крайние случаи...");
        }
    }


    public resize(width:number, height:number):void {
        this.viewSize.width = width;
        this.viewSize.height = height;
        FrameManager.resize(width, height);
        this.activePage.resize(width, height);
        this.preloadPage.resize(width, height);
        this.toFrame(this.curentFrame);
    }

    public addPages(curentPage:PageI, nextPage:PageI):void {
        this.activePage = curentPage;
        this.preloadPage = nextPage;
    }

    public addAtlas(atlases:AtlasObjectI[]):void {
        this.atlases = atlases;
    }

    public addFrames(frames:FrameI[][]):void {
        this.frames = frames;
    }

    public curentState():stateI {
        return {pageNumber: this.curentPage, frameNumber: this.curentFrame};
    }

    public getCover():AtlasObjectI {
        return this.atlases[0];
    }

    private swapPages():void {
        var swap1:PageI = this.activePage;
        //var swap2:PageI = this.preloadPage;
        this.activePage = this.preloadPage;
        this.preloadPage = swap1;
        this.activePage.indexUp();
        this.preloadPage.indexDown();
        //this.preloadPage.hide();
    }

    private cashAfterAnimate(time:number, curent:number, next:number):void {

        if (this.firstBoot) {
            this.firstBoot = false;
            this.clickDisable = false;
            this.pageFlip = true;
            this.activePage.transform.setPos("0%", "0%", "0px").setTime(0).apply();
            this.preloadPage.transform.setPos("100%", "0%", "0px").setTime(0).apply();
            this.atlases[next + 1].loadTo(this.preloadPage.getContext());
            this.cashedPage = next + 1;
            clearTimeout(this.cashTimer);
            return undefined;
        }

        if (next + 1 == this.atlases.length) {
            this.swapPages();
            this.clickDisable = false;
            this.pageFlip = true;
            clearTimeout(this.cashTimer);
            return undefined;
        }

        var that = this;

        PlatformManager.sectionFinished(curent);

        if (next == -1 || next == this.atlases.length) {
            that.clickDisable = false;
            that.pageFlip = true;
            this.atlases[this.cashedPage].delete();
            clearTimeout(this.cashTimer);
            return undefined;
        }

        if (next != this.cashedPage) {
            this.atlases[this.cashedPage].delete();

        }


        clearTimeout(this.cashTimer);

        function cashed() {

            if (curent !== -1 && that.atlases[curent]) that.atlases[curent].delete();

            that.swapPages();


            if (next > curent || curent == undefined || next == 0) {
                that.atlases[next + 1].loadTo(that.preloadPage.getContext());
                that.cashedPage = next + 1;
                that.preloadPage.transform.setPos("100%", "0%", "0px").setTime(0).apply();
                //that.preloadPage.hide();
            } else {
                that.atlases[next - 1].loadTo(that.preloadPage.getContext());
                that.cashedPage = next - 1;
                that.preloadPage.transform.setPos("-100%", "0%", "0px").setTime(0).apply();
                //that.preloadPage.hide();
            }

            that.activePage.transform.setPos("0%", "0%", "0px").setTime(0).apply();
            that.clickDisable = false;
            that.pageFlip = true;
            that.frameJump = true;
        }


        this.cashTimer = setTimeout(cashed, time * 1000);

    }
}
var manager:PagesManagerI = new PagesManager();
window["pm"] = manager;
export = manager;