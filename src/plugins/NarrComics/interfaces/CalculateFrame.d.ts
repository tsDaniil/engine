/**
 * Created by usercom on 09.10.14.
 */

/// <reference path="./AtlasObject.d.ts" />

interface CalculateFrameI {

	getImgScale(rectImg:rectI, rectFrame:rectI, scale?:number):rectI;

}