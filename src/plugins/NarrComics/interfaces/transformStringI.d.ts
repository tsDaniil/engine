/**
 * Created by usercom on 01.10.14.
 */
interface transformStringI {
	toString(x:number, y:number, rotate:number, scaleX:number, scaleY?:number, metric?:string):string;
}

interface  transformToStringI {
	(x:number, y:number, rotate:number, scaleX:number, scaleY?:number, metric?:string) :string;
}

declare var transformToString:transformToStringI;