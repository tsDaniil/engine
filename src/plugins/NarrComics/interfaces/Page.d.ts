/**
 * Created by usercom on 28.09.14.
 */
/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="./TransformationI.d.ts" />


interface PageI {
	appendToPage(content:$i):void;
	moveLeft(time:number):void;
	moveFromLeft(time:number):void;
	moveFromRight(time:number):void;
	moveRight(time:number):void;
	resize(width:number, height:number):void;
	deletePageContent():void;
	show():void;
	hide():void;
	getContext():CanvasRenderingContext2D;
	transform : TransformationI;
	indexUp():void;
	indexDown():void;
}