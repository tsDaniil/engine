/// <reference path="../../../interface/DomLite.d.ts" />
/// <reference path="NarrComics.d.ts" />

interface rectI {
	x:number;
	y:number;
	width:number;
	height:number;
}

interface imgI {
	rect : rectI;
	src  : string;
}

interface AtlasI {
	rect : rectI;
	imgs : imgI[];
}

interface AtlasObjectI {
	data: AtlasI;
	animate(x:number, y:number, scale:number, time:number):void;
	delete():void;
	cashed():boolean;
	onLoad(callback:()=>void, context?:any):void;
	loaded: boolean;
	transformation(x:number, y:number, scale:number):void;
	loadTo(context:CanvasRenderingContext2D):void;
	error: boolean;
	bgSound;
	onPageStart():void;
	draw():void;
	scale:number;
}
