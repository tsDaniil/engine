/**
 * Created by usercom on 28.09.14.
 */
/// <reference path="NarrComics.d.ts" />
/// <reference path="AtlasObject.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />


interface FrameI {
	id: number;
	rect: rectI;
	time: number;
	color : string;
	animation: string;
}

interface FrameManagerI {
	calculate(frame:FrameI, time?:number):void;
	resize(width:number, height:number): void;
	createFrame(parentView:$i, width:number, height:number):void;
	show(time:number):void;
	hide(time:number):void;
}

