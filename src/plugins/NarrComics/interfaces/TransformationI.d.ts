/**
 * Created by usercom on 10.10.14.
 */
/// <reference path="../../../interface/DomLite.d.ts" />


interface TransformationI {
	setTime(time:number): TransformationI;
	setPos(x:string, y:string, z?:string): TransformationI;
	setScale(scale:string): TransformationI;
	setRotate(rotate:string): TransformationI;
	setNode(node:$i): TransformationI;
	getTransformString():string;
	getTransitionString():string;
	apply(): TransformationI;
	add(): TransformationI;
	StartAnimations():void;
}