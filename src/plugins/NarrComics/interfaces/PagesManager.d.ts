/**
 * Created by usercom on 12.10.14.
 */
/// <reference path="./comicsManager.d.ts" />
/// <reference path="./CoverI.d.ts" />
/// <reference path="./AtlasObject.d.ts" />
/// <reference path="./dataI.d.ts" />
/// <reference path="./Page.d.ts" />
/// <reference path="./CalculateFrame.d.ts" />
/// <reference path="./FrameManager.d.ts" />
/// <reference path="../../../interface/DownloaderI.d.ts" />
/// <reference path="../../../interface/PlatformManagerI.d.ts" />
/// <reference path="../../../interface/PreloaderI.d.ts" />

interface PagesManagerI {

	clickDisable:boolean;

	nextPage():void;
	lastPage():void;
	toPage(pageNumber:number):void;

	nextFrame():void;
	lastFrame(): void;
	toFrame(frameNumber:number):void;

	resize(width:number, height:number):void;

	addPages(curentPage:PageI, nextPage:PageI): void;
	addAtlas(atlases:AtlasObjectI[]): void;
	addFrames(frames:FrameI[][]):void;
	curentState():stateI;
	getCover():AtlasObjectI;
	setFirstPage(pageNumber:number):void;
}