/// <reference path="./../interfaces/AtlasObject.d.ts" />
/// <reference path="../interfaces/NarrComics.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />

interface CoverI extends AtlasObjectI {
	createCover():void;
	resize(width:number, height:number):void;
}