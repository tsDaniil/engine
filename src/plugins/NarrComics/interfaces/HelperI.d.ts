/**
 * Created by usercom on 11.11.14.
 */

interface HelperI{

	isShow: boolean;
	show():void;
	onShow(callback: ()=>void): void;
}