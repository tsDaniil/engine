/**
 * Created by usercom on 01.10.14.
 */
/// <reference path="../interfaces/dataI.d.ts" />
/// <reference path="../interfaces/FrameManager.d.ts" />
/// <reference path="../interfaces/Arrow.d.ts" />

interface stateI {
	pageNumber: number;
	frameNumber: number;
}


interface  ComicsManagerI {
	create(parentView:$i, data:dataComicsI):void;
	resize(width:number, height:number):void;

	toState(state:stateI):void;
	toPage(pageNumber:number):void;
	toFrame(frameNumber:number):void;
	next():void;
	perv():void;

	onChange(callback:(state?:stateI) => void):void;
	getState():stateI;
	change():void;

	createCover():void;

}
