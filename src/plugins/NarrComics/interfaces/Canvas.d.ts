/// <reference path="../../../interface/DomLite.d.ts" />


interface CanvasI {
	context:CanvasRenderingContext2D;
	appendTo(view:$i) : CanvasI;
	setSize(width:number, height:number): CanvasI;
	drawImage(img:HTMLImageElement, sx?:number, sy?:number, swidth?:number, sheight?:number, x?:number, y?:number, width?:number, height?:number): CanvasI;
}