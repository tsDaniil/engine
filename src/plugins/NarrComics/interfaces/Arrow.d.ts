/**
 * Created by usercom on 14.10.14.
 */
/// <reference path="../../../interface/DomLite.d.ts" />

interface ArrowI {
	pause():ArrowI;
	play():ArrowI;
	hide():ArrowI;
	show():ArrowI;
	temporaryHide(time:number):void;
	element:$i;
}

declare class Arrow implements ArrowI {
	constructor(className:string, callback?:()=>void);

	create(className:string, callback?:()=>void):ArrowI;

	pause():ArrowI;

	play():ArrowI;

	hide():ArrowI;

	show():ArrowI;

	temporaryHide(time:number):void;

	element:$i;
}