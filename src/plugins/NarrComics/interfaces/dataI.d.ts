/// <reference path="./FrameManager.d.ts" />
/// <reference path="./AtlasObject.d.ts" />

interface dataComicsI {
	mode?: string;
	pdfMode?: boolean;
	singlePageMode?: boolean;
	path?: string;
	styles?: Array<string>;
	content: Array<contentComicsI>;
	module: string;
	subjectList?: Array<Object>;
}

interface contentComicsI {
	id:number;
	color : string;
	frames : FrameI[];
	atlas : AtlasI;
	bgSound: Object;
}

