/// <reference path="../PluginI.d.ts" />
/// <reference path="../../interface/DomLite.d.ts" />
/// <reference path="./interfaces/dataI.d.ts" />
/// <reference path="../../interface/require.d.ts" />
/// <reference path="../../interface/greensock.d.ts" />
/// <reference path="../../interface/Events.d.ts" />
/// <reference path="../../interface/DownloaderI.d.ts" />
/// <reference path="../../utils/init.ts"/>

import Preloader = require("../../utils/utils/Preloader");
import UserData = require("../../utils/app/user/UserData");
import Module = require("../../utils/base/Module");
import ComicsManager = require("./classes/ComicsManager");
import PlatformManager = require("../../utils/app/PlatformManager");
import PageManager = require("./classes/PagesManager");
import $ = require("$");
import Downloader = require("../../utils/utils/Downloader");
import Audio = require("../../utils/app/Audio");
import FontsInit = require("./classes/font");
import KeyBoardManager = require("../../utils/app/control/KeyBoardManager");

class NarrComics extends Module implements PluginI {

    private lButt:$i;
    private rButt:$i;
    private data:dataComicsI;

    /**
     * Вход в модуль из движка
     * @param data //- данные из tsEngineData.js
     */
    public run(data:dataComicsI):void {

	    this.preloadInitStyles();

		Preloader.show();

        /**
         * Запоминаем данные из tsEngineData.js
         */
        this.data = data;

        /**
         * Номер страницы с которой начинаем чтение, пытаемся взять из урла
         */
        var pageNumber:number = parseInt(UserData.getSearch().page_index) || 0;

        /**
         * Номер фрэйма с которого начинаем смотреть
         */
        var framenumber:number = 0;

        /**
         * сохраняем контекст в замыкании
         */
        var CManager = this;

        /**
         * Подписываемся на загрузку данных, если уже произошло выполнится
         */

        var that = this;
        PlatformManager.onEvent("bookDataLoaded", function () {
            that.dataLoad();
        });

        /**
         * кнопки навигации
         */


        this.rButt = $.createElement("img", "rButton", true);
        this.rButt.on("User:tap", function (tapEvent:TapI) {
            tapEvent.stopPropagation();
            PlatformManager.showNavigation();
        });
        this.rButt.node["src"] = "img/buttons/kniga.png";

        if(!UserData.isWeb()) {
            this.lButt = $.createElement("img", "lButton", true);
            this.lButt.on("User:tap", function (tapEvent:TapI) {
                tapEvent.stopPropagation();
                PlatformManager.showSettings();
            });
            this.lButt.node["src"] = "img/buttons/home_new.png";
        }

        /**
         * Загрузили темплейты и данные - готовы стартовать
         */
        this.onLoad(function () {

            ComicsManager.create(this.view, data);
            ComicsManager.resize(this.view.width(), this.view.height());
            this.view.append(that.rButt);

            if (!UserData.isWeb()) {
                this.view.append(that.lButt);
            }

            /**
             * подписка на переход на новую страницу
             */
            this.view.on("User:tap", ComicsManager.next.bind(ComicsManager));
            this.view.on("User:swipe:horizontal:right", PageManager.lastPage.bind(PageManager));
            this.view.on("User:swipe:horizontal:left", PageManager.nextPage.bind(PageManager));

            this.listenTo(KeyBoardManager, KeyBoardManager.Keys.space, ComicsManager.next, ComicsManager);

            CManager.fireEvent("ready");

            PlatformManager.onSectionChanged(function (pageNumber:number) {
                CManager.setPage(pageNumber);
            });

        });

    }

    /**
     * Переход на следующую страницу 1 фрэйм
     */
    private static nextPage():void {

        PageManager.nextPage();
    }

    /**
     * Переход на прошлую страницу 1 фрэйм
     */
    private static lastPage():void {

        PageManager.lastPage();
    }

    /**
     * Переход на определенный пэйдж
     * @param pageNumber
     * @param frameNumber
     */
    public setPage(pageNumber:number, frameNumber?:number):void {
        var pNumber = 0;
        if (pageNumber) pNumber = pageNumber;

        var fNumber = 0;
        if (frameNumber) fNumber = frameNumber;

        ComicsManager.toPage(pNumber);
        ComicsManager.toFrame(fNumber);
    }


    public getPageCount():number {
        return this.data.content.length;
    }

    /**
     * Первая страница из темплейта - создать надо когда прийдут сведения о серии
     * и перейти на переданную страницу
     */
    public dataLoad():void {
        ComicsManager.createCover();
        var pageNumber = 0;
        var section = PlatformManager.getBookData().sectionNumber;
        if (section) pageNumber = section;
        PageManager.setFirstPage(pageNumber);
        console.log("Need jump to page nomber:", pageNumber);
    }

    /**
     * Загрузка темплейта ковера
     */
    private loadTemplate():void {

        var CManager:NarrComics = this;
        require(["./comicsTpl"], function () {
            setTimeout(function () {
                CManager.loaded();
            }, 300);
        });

    }

    /**
     * Ресайз рабочей области
     * @param width
     * @param height
     */
    public onResize(width:number, height:number):void {
        ComicsManager.resize(width, height);
    }

    private preloadInitStyles():void {
        FontsInit(this.view);

        var width = this.view.width();
        var height = this.view.height();

        $("body").width(width).height(height).css("overflow", "hidden");
        $("html").width(width).height(height).css("overflow", "hidden");

        Downloader.loadEngineStyle("plugins/NarrComics/css/comics.css?" + new Date().getTime(), this.loadTemplate.bind(this));

    }
}

export =  NarrComics;
