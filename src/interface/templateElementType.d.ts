interface TemplateSizeI {
    /**
     * ширина в пикселях либо в процентах от родителя
     */
    width ?: string;

    /**
     * высота в пикселях либо в процентах от родителя
     */
    height ?: string;
}

interface TemplateMarginI {

    /**
     * отступ сверху в пикселях либо в процентах от родителя
     */
    top? : string;

    /**
     * отступ слева в пикселях либо в процентах от родителя
     */
    left?: string;

    /**
     * отступ снизу в пикселях либо в процентах от родителя
     */
    bottom?: string;

    /**
     * отступ справа в пикселях либо в процентах от родителя
     */
    right?: string;
}

interface TemplateTextI {

    /**
     * Текст
     */
    text?      : string;

    /**
     * Массив путей до шрифта в разных форматах
     */
    fontSource?: Array<string>;

    /**
     * Высота строки
     */
    lineHeight?: string; //px

    /**
     * Размер текста
     */
    fontSize?  : string; //px

    /**
     * Имя используемого шрифта
     */
    fontName?  : string;

    /**
     * Переносимость слов по пробелам
     */
    nowrap?    : boolean;

    /**
     * Жирный ? default = false
     */
    bold?       : boolean;

    /**
     * Курсив ? default = false
     */
    italic?     : boolean;

    /**
     * blink - Текст будет мигать.
     * line-through - Делает текст перечеркнутым.
     * overline - Надчёркивание текста.
     * underline - Подчеркивание текста.
     * none - Текст без оформления. (default)
     */
    textDecoration?: string;


    /**
     * top (default)
     * center
     * bottom
     */
    horizontalAlign?: string;
}

interface TemplateImageI {
    /**
     * Путь до картинки
     */
    src : string;
}


interface templateElementType {
    /**
     * id
     */
    id: string;

    /**
     * Тип объекта "image" | "container" | "video" | "text";
     */
    elementType: string;

    /**
     * размер параметры не обязательно
     */
    size: TemplateSizeI;

    /**
     * отступы в пикселях лябо в процентах от родителя
     */
    margin?: TemplateMarginI;

    /**
     * Прозрачность
     * От 0 до 1;
     */
    alpha: number;

    /**
     * Горизонтальное выравнивание
     * "left" | "right" | "center" | "stretch"; //Растянуть с учетом марджинов
     */
    horizontalAlign?: string;

    /**
     * вертикальное выравнивание
     * "left" | "right" | "center" | "stretch"; //Растянуть с учетом марджинов
     */
    verticalAlign?: string;

    /**
     *  Имя класса дом элемента (добавляет к "view-element")
     */
    className? : string;

    zIndex?:number;

    /**
     * Текстовая нода - надо еще продумать
     */
    text?: TemplateTextI;

    /**
     * картиночная - пока только src
     */
    image?: TemplateImageI;

    video?: TemplateVideoI

    interactive?: Array<InteractiveI>;
    animation?: Array<AnimationTemplateI>;

    content?: Array<templateElementType>;

}

interface TemplateVideoI {
    src: Array<VideoSorcesI>;
    autoPlay: boolean;
    interactive: Array<InteractiveI>;
    videoControls: VideoControlI;
}

interface VideoControlI {
    parentId: string;
    size: TemplateSizeI;
    margin?: TemplateMarginI;

    /**
     * Горизонтальное выравнивание
     * "left" | "right" | "center" | "stretch"; //Растянуть с учетом марджинов
     */
    horizontalAlign?: string;

    /**
     * вертикальное выравнивание
     * "left" | "right" | "center" | "stretch"; //Растянуть с учетом марджинов
     */
    verticalAlign?: string;

    progress?: {
        margin: TemplateMarginI;
        size: TemplateSizeI;
        progressStyle?: VideoProgressLineI;
        totalStyle?: VideoProgressLineI;
        bufferStyle?: VideoProgressLineI;
    }
    play?: VideoControlButtonI;
    pause?: VideoControlButtonI;
    togglePlay?: VideoControlButtonI;
    stop?: VideoControlButtonI;
}

interface VideoProgressLineI {
    color: string;
    image: string;
}

interface VideoControlButtonI {
    margin: TemplateMarginI;
    size: TemplateSizeI;
    image: string;
}

interface VideoSorcesI {

    url: string;
    type?: string;

}

interface AnimatedPositionI {

    percent: number;
    size?: TemplateSizeI;
    margin?: TemplateMarginI;
    alpha?:number;
    rotateZ?: number;
    rotateX?: number;
    rotateY?: number;
    scaleX?:number;
    scaleY?: number;
    originX?:number;
    originY:number;

}

interface AnimationTemplateI {

    /**
     * "linear" (default) | "ease" | "bounceOut";
     */
    timingFunction?: string;

    /**
     * Имя анимации
     */
    name: string;

    /**
     *  "toggle" |  "normal"
     */
    animationDirection?: string

    time?:number;

    positions: Array<AnimatedPositionI>;

}

interface  InteractiveI {

    /**
     * "animation"(default)
     * "pluginMethod"
     * "videoControl"
     * "episodeControl"
     * "platformControl"
     */
    interactiveType?: string;

    /**
     * Тип события по которому срабатывает обработчик
     * Для всех кроме "videoControl"
     * -----------------------------------------
     * "tap"(default) | "touch:start" | "touch:end" | "move" | "swipe"
     * -----------------------------------------
     * Только для "videoControl"
     * "ended" | "error" | "pause" | "play" | "canplay" (default)
     */
    actionType?: string;

    /**
     * id анимируемого объекта
     * -----------------------------------------
     * "episodeControl"   ==>  "scene"
     * "platformControl"  ==>  "platform"
     */
    animateId: string

    /**
     * Имя анимации которую хотим проиграть
     * -----------------------------------------
     * "episodeControl"   ==>  "next" | "prev" | "jumpTo:1"
     * "platformControl"  ==>  "showSettings" | "showNavigation" | "exit"
     * "videoControl"     ==>  "pause" | "play" | "playToggle"
     */
    animateName: string;

}