/// <reference path="./ModuleI.d.ts" />

interface OrientationManagerI extends ModuleI {

    eventName:string;
    getOrientation():string;

}