interface CoordsI {
	x:number;
	y:number;
}

interface SizeI {
	width: number;
	height: number;
}

interface RectI {
	coords: CoordsI;
	size: SizeI;
}

interface DistanceI {
	x: number;
	y: number;
	general: number;
}

interface SpeedI {
	x: number;
	y: number;
	general: number;
}

interface pathsI {
	enginePath:string;
	buildPath:string;
}