/// <reference path="./Events.d.ts" />
/// <reference path="./DomLite.d.ts" />

/**
 * тач события типа User:touch:start
 */
interface InteractiveRectI {

    /**
     * Подписываемся на событие однократно, затем обработчик удалится
     * @param eventName имя события
     * @param handler обработчик события
     * @param [context] контекст выполнения события
     */
    once(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any)=>any, context?:any):InteractiveRectI;

    /**
     * Подписываемся на событие
     * @param eventName имя события
     * @param handler обработчик события
     * @param [context] контекст выполнения обработчика
     */
    on(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any)=>any, context?:any):InteractiveRectI;

    /**
     * Отписываемся от события/событий
     * @param eventName
     * @param handler
     */
    off(eventName?:string, handler?:(a?:any, b?:any, c?:any, d?:any)=>any):InteractiveRectI;

    /**
     * Включаем обработчики которые мы передали в область
     */
    enable():InteractiveRectI;

    /**
     * Выключаем обработчики которые мы передали в область
     */
    disable():InteractiveRectI;

    /**
     * Проверяем должны ди мы остановить выполнение других событий в очереди (UserEventManager)
     */
    isStop():boolean;

    /**
     * Получаем уровень приоритета события (по нему будет выполнена сортировка событий в UserEventManager)
     */
    getLevel():number;

    /**
     * Проверяем попало ли событие в нашу область
     * @param event
     */
    hitTest(event:UserEventI):boolean;

    /**
     * Передвигаем область
     * @param deltaX
     * @param deltaY
     */
    move(deltaX:number, deltaY:number):InteractiveRectI;

    /**
     * Устанавливаем новые координаты для области
     * @param x
     * @param y
     */
    setPosition(x:number, y:number):InteractiveRectI;

    /**
     * Получаем область
     */
    getRect():RectI;

    /**
     * Задаем новую область
     * @param rect
     */
    setRect(rect:RectI):InteractiveRectI;
}