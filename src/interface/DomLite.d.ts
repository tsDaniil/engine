/// <reference path="./Utils.d.ts" />
interface domEventsI {

    /**
     * Строка подписки на событие (аналогия - "mousedown")
     */
    start:string;
    /**
     * Строка подписки на событие (аналогия - "mousemove")
     */
    move:string;
    /**
     * Строка подписки на событие (аналогия - "mouseup")
     */
    end:string;
}

interface $i {

    /**
     * Этемент объекта $
     */
    node:HTMLElement;

    /**
     * Заменяем содержимое html элемента на строку
     * @param html
     */
    html(html:string):$i;
    /**
     * Получаем содержимое элемента
     */
    html():string;

    /**
     * Устанавливаем содержимое элемента (аналогично html)
     * @param text
     */
    text(text:string):$i;
    /**
     * Получаем текст внутри элемента (только содержимое текстовых нод)
     */
    text():string;

    /**
     * Проверяем есть ли текст в элементе (пустые ноды не считаются)
     */
    hasText():boolean;

    /**
     * Добавляем класс элементу
     * @param className
     */
    addClass(className:string):$i;
    /**
     * Удаляем класс у элемента
     * @param className
     */
    removeClass(className:string):$i;
    /**
     * Удаляем все классы у элемента
     */
    removeClass():$i;
    /**
     * Добавляем/удаляем класс
     * @param className
     */
    toggleClass(className:string):$i;

    /**
     * Проверяем наличие класса
     * @param className
     */
    hasClass(className):boolean;

    /**
     * Добавляем в конец детей елемент собранный из строки
     * @param elem
     */
    append(elem:string):$i;
    /**
     * Добавляем в конец детей элемент
     * @param elem
     */
    append(elem:HTMLElement):$i;
    /**
     * Добавляем в конец детей элемент $i
     * @param elem
     */
    append(elem:$i):$i;
    append(elem:$$):$i;

    /**
     * Добавляем в начало детей элемент собранный из строки
     * @param elem
     */
    prepend(elem:string):$i;
    /**
     * Добавляем в начало детей элемент
     * @param elem
     */
    prepend(elem:HTMLElement):$i;
    /**
     * Добавляем в начало детей элемент $i
     * @param elem
     */
    prepend(elem:$i):$i;
    prepend(elem:$$):$i;

    /**
     * навешивает обработчик события $i.events.end  ( между началом и завершением тапа прошло менее 200мс и палец прошел дистанцию менее 20 px) на элемент
     * используют on
     * @param handler
     */
    tap(handler:(a?:any, b?:any, c?:any, d?:any) => any):$i;

    /**
     * Запускаем событие клика на элементе
     */
    tap():$i;

    wrap(tagName:string, className?:string, param?:boolean):$i;

    clone():$i;

    /**
     * Только десктопное!
     * Обработчики "mouseenter" и "mouseout"
     * используют on
     * @param handlerEnter
     * @param handlerOut
     */
    hover(handlerEnter:(event?:MouseEvent) => any, handlerOut:(event?:MouseEvent) => any):$i;

    /**
     * Слушаем событие изменения элемента
     * используют on
     * @param handler
     */
    change(handler:(event?:MouseEvent) => any):$i;

    /**
     * Универсальный навешиватель обработчиков событий
     * @param eventName
     * @param handler
     */
    on(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any, context?:any):$i;

    addEventListener(eventName:string, handler):$i;
    removeEventListener(eventName:string, handler?)

    /**
     * Снимаем все обработчики с элемента
     */
    off():$i;

    /**
     * Устанавливает будет ли пропускать всплытие событий элемент (true - не пропускаем, false - пропускаем; default = false);
     * @param mode
     */
    setDelegationMode(mode:boolean):$i;

    /**
     * Снимаем все обработчики данного события с элемента
     * @param eventName
     */
    off(eventName:string):$i;

    /**
     * Снимаем конкретный обработчик с элемента
     * @param eventName
     * @param handler
     */
    off(eventName:string, handler:(a?:any, b?:any) => any):$i;

    /**
     * Получаем значение стиля элемента
     * @param name
     */
    css(name:string):string;

    /**
     * Устанавливаем значение стиля элемента
     * @param name
     * @param value
     */
    css(name:string, value:string):$i;

    /**
     * Устанавливаем значение стиля элемента через объект вида:
     * {"имя стиля":"Значение стиля"}
     * @param styleObj
     */
    css(styleObj:Object):$i;

    /**
     * Скрываем/показываем элемент (использует show/hide)
     */
    displayToggle():$i

    /**
     * Двигаем элемент
     * @param deltaX
     * @param deltaY
     */
    move(deltaX:number, deltaY:number):$i;

    /**
     * Получаем значение атрибута
     * @param name
     */
    attr(name:string):string;

    /**
     * Устанавливаем значение атрибута стиля
     * @param name
     * @param value
     */
    attr(name:string, value:string):$i;

    /**
     * Устанавливаем value элемента
     * @param value
     */
    val(value:string):$i;

    /**
     * Получаем value элемента
     */
    val():string;

    /**
     * Ищем элемент
     * @param selector
     */
    find(selector:string):$$;

    findElement(selector:string):$i;

    /**
     * Получаем коллекцию детей элемента
     */
    children():$$;

    /**
     * Прячем элемент
     */
    hide():$i;

    /**
     * Показываем элемент
     */
    show():$i;

    /**
     * Удаляем элемент из Dom
     */
    remove():$i;

    /**
     * Получаем оффсет элемента
     * С параметром - рекурсивно до #bigrect
     * @param recursive
     */
    offset(recursive?:boolean):$Offset;

    /**
     * Получаем родителя
     */
    parent():$i;

    /**
     * Получаем ширину элемента
     */
    width():number;

    /**
     * Устанавливаем ширину элемента
     * @param width
     */
    width(width:number):$i;

    /**
     * Получаем высоту элемента
     */
    height():number;

    /**
     * Устанавливаем высоту элемента
     * @param height
     */
    height(height:number):$i;

    /**
     * Очищаем содержимое html элемента
     */
    empty():$i;

    /**
     * Получаем область в которой находится элемент на экране не учитывает transform
     * @param touchX //запас для тача
     * @param touchY //запас для тача
     */
    getRect(touchX?:number, touchY?:number):RectI;

    //--------------------------------//
    //-- css plugin --//

    /**
     * Пропорционально масштабируем
     * @param coff
     */
    scale(coff:number):$i;

    /**
     * Масштабируем
     * @param x коэфицент масштабирования по Х
     * @param y коэфицент масштабирования по У
     */
    scale(x:number, y:number):$i

    /**
     * Сдвигаем элемент
     * @param x
     * @param y
     */
    translate(x:string, y:string):$i;

    /**
     * Вращаем элемент по оси Z
     * @param rz радиан
     */
    rotate(rz:number):$i;

    /**
     * Вращаем элемент
     * @param rx радиан
     * @param ry радиан
     * @param rz радиан
     */
    rotate(rx:number, ry:number, rz:number):$i;

    /**
     * Анимируем изменение стилей
     * @param time Время анимации в секундах
     * @param [styleName] имя стиля для анимации (или "all")
     */
    transition(time:number, styleName?:string):$i;

    /**
     * Сбрасываем матрицу
     */
    dropMatrix():$i;

    //--------------------------------//
    //-- textNode plugin --//

    fullChildren():Array<$i>;

    isElement():boolean;

}

interface $$ {

    /**
     * Получаем последний элемент коллекции
     */
    last():$i;
    /**
     * Получаем первый элемент коллекции
     */
    first():$i;

    /**
     * Устанавливаем стили для всей коллекции
     * @param name
     * @param value
     */
    css(name:string, value:string):$$;

    /**
     * Устанавливаем стили для всей коллекции
     * @param styleObj
     */
    css(styleObj:Object):$$;

    /**
     * Удаляем из ДОМ все элементы коллекции
     */
    remove():$$

    /**
     * Подписываем клик на все элементы коллекции
     * @param handler
     */
    tap(handler:(a?:any, b?:any, c?:any, d?:any) => any):$$;

    /**
     * Подписываем change на все элементы коллекции
     * @param handler
     */
    change(handler:(event?:any) => any):$$;

    /**
     * Подписываем hover на все элементы коллекции
     * @param handlerEnter
     * @param handlerOut
     */
    hover(handlerEnter:(event?:MouseEvent) => any, handlerOut:(event?:MouseEvent) => any):$$;

    /**
     * Подписываем на событие все элементы коллекции
     * @param eventName имя события
     * @param handler обработчик
     */
    on(eventName:string, handler:(a?:any) => any):$$;

    /**
     * Добавляем класс на все элементы коллекции
     * @param className
     */
    addClass(className:string):$$;

    /**
     * Удаляем класс у всех элементов коллекции
     * @param className
     */
    removeClass(className:string):$$;

    /**
     * Удаляем/добавляем класс элементам коллекции
     * @param className
     */
    toggleClass(className:string):$$;

    /**
     * Показываем элементы
     */
    show():$$;

    /**
     * Скрываем элементы
     */
    hide():$$;

    /**
     * Изменяем коллекцию
     * @param mapFunc
     */
    map(mapFunc:(element:$i, index?:number) => any):$$;

    /**
     * Фильтруем коллекцию
     * @param filterFunc
     */
    filter(filterFunc:(element:$i, index?:number) => boolean):$$;

    /**
     * Обходим коллекцию
     * @param callback
     * @param context
     */
    forEach(callback:(elem:$i, index?:number) => any, context?:any): $$;

    /**
     * Ищем в коллекции
     * @param callback
     * @param context
     */
    some(callback:(elem:$i, index?:number) => any, context?:any): boolean;

    /**
     * Показываем/прячем элементы коллекции (вызывает displayToggle для каждого элементак коллекции)
     */
    displayToggle():$$;

    /**
     * Получаем элемент коллекции по номеру
     * @param index
     */
    eq(index:number):$i;

    /**
     * Индекс последнего элемента коллекции +1
     */
    length: number;

    push(some:$i);

}

interface $Offset {
    /**
     * Оффсет слева от элемента
     */
    left:number;
    /**
     * Оффсет над элементом
     */
    top:number;
}

interface touchI {
    x: number;
    y: number;
    identifier: number;
}

interface $Event {
    type: string;
    altKey: boolean;
    babbles: boolean;
    cancelBubble: boolean;
    cancelable: boolean;
    clipboardData?: string;
    ctrlKey: boolean;
    currentTarget: HTMLElement;
    defaultPrevented: boolean;
    eventPhase: number;
    layerX: number;
    layerY: number;
    metaKey?: boolean;
    path: NodeList;
    retreturnValue?: boolean;
    shiftKey: boolean;
    srcElement: HTMLElement;
    target: HTMLElement;
    view: any;
    originEvent: any;
    preventDefault():void;
    stopPropagation():void;
    button?: number;
    x: number;
    y: number;
    touches?: Array<touchI>
}

interface AnimateOptionsI {

    duration: number;

    step:(progress:number)=>void;
    callback?:()=>void;

}

interface AnimationProgressI {
    end():void;
    callback:()=>void;
}

interface $Static {

    (selector:string):$i;
    (element:any):$i;

    parseHTML(html:string):HTMLElement;
    parseHTML(html:string, native:boolean):$i;

    isType(some:any, type:any):boolean;

    isArray(some:any):boolean;

    isString(some:any):boolean;

    isObject(some:any):boolean;

    isNumber(some:any):boolean;

    isElement(some:any):boolean;

    each(some:NodeList, callback:(node:Node, index:number) => any):void;
    each(some:NodeList, callback:(node:HTMLElement, index:number) => any):void;
    each(some:Array<any>, callback:(node:any, index:number) => any):void;
    each(some:IArguments, callback:(node:any, index:number) => any):void;
    each(some:Object, callback:(node:any, index:string) => any):void;

    toArray(some:Object):Array<any>;
    toArray(some:any):Array<any>;

    createElement(tagName:string, classList:string):HTMLElement;
    createElement(tagName:string, classList:string, isdom:boolean):$i;

    createCollection(elements:Array<$i>):$$;

    cloneLite(param:Object):any;
    cloneLite(param:Array<any>):Array<any>;

    events:domEventsI;
    setEvents(events:domEventsI):void;
    /**
     * Устанавливаем режим, от которого будут вешаться обработчики + $.events
     * @param mode "mobile" || "mouse" || "winMobile" || default = "mouse"
     */
    setTouchMode(mode?:string):void;

    hasInArray(arr:Array<any>, value:any):boolean;

    animate(options:AnimateOptionsI):AnimationProgressI;
}

declare var Lite:$Static;
declare module "$" {
    export = Lite;
}