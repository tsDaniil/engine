/// <reference path="./Events.d.ts" />
/// <reference path="./InteractiveRectI.d.ts" />
/// <reference path="./BaseI.d.ts" />

interface UserEventManagerI {
    /**
     * Запуск событий тача
     * @param event
     */
    startEvent(event:TouchI):void
    startEvent(event:TapI):void
    startEvent(event:MoveI):void

    /**
     * Запуск событий тача из другого кода
     * @param eventName
     * @param args
     */
    trigger(eventName:string, args:Array<any>): UserEventManagerI;

    /**
     * Подписка на события тача
     * @param eventName
     * @param handler
     * @param rect
     * @param context
     */
    on(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any, rect:InteractiveRectI, context?:any): UserEventManagerI;

    /**
     * Отписка от событий тача
     * @param eventName
     * @param handler
     */
    off(eventName?:string, handler?:(a?:any, b?:any, c?:any, d?:any) => any): UserEventManagerI;

}