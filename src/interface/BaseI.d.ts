interface BaseI {
    /**
     * Запускаем событие
     * @param eventName имя события
     * @param args
     */
    trigger(eventName:string, args:Array<any>): BaseI;

    /**
     * Подписываемся на событие из самого себя
     * @param eventName
     * @param handler
     * @param context
     */
    on(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any, context?:any): BaseI;
    /**
     * Подписываемся на одно событие (обработчик автоматически удаляется)  из самого себя
     * @param eventName
     * @param handler
     * @param context
     */
    once(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any, context?:any): BaseI;
    /**
     * Отписывемся от события из самого себя
     * @param eventName
     * @param handler
     */
    off(eventName?:string, handler?:(a?:any, b?:any, c?:any, d?:any) => any): BaseI;

}