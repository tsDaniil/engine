/// <reference path="./ModuleI" />

interface LocalStorageManagerI extends ModuleI {

	/*   ВНИМАНИЕ!! РАБОТА С ЛОКАЛЬНЫМ ХРАНИЛИЩЕМ АСИНХРОННА!   */

	/*
	 * При превышении таймаута ожидания данных выполнится коллбэк с пустым ответом
	 * дефолтное время ожидания данных 250мс
	 */

	/**
	 * Устанавливаем уникальный префикс для работы с локальным хранилищем
	 * @param prefix имя сериала + имя эпизода
	 */
	setPrefix(prefix:string):void;

	/**
	 * Устанавливаем значение локального хранилища
	 * @param name префикс +  ключ
	 * @param value значение
	 */
	setItem(name:string, value:string):void;

	/**
	 * Получем значение из локального хранилища
	 * @param name
	 * @param callback
	 * @param [timeout]
	 */
	getItem(name:string, callback:(data:string)=>void, timeout?:number):void;

	/**
	 * Кладем объект в локальное хранилище
	 * @param name префикс +  ключ
	 * @param value объект
	 */
	setObject(name:string, value:any):void;

	/**
	 * Получаем объект из локального хранилища
	 * @param name префикс +  ключ
	 * @param callback
	 * @param [timeout]
	 */
	getObject(name:string, callback:(data:any)=>void, timeout?:number):void;

}