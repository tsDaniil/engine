/// <reference path="ModuleI.d.ts" />

interface CollectionI extends ModuleI {

	/**
	 * Номер активного элемента коллекции
	 */
	active:number;
	/**
	 * Коллекция элементов
	 */
	collection:Array<any>;

	/**
	 * Получаем активный элемент коллекции
	 */
	getActive():any;
	/**
	 * Получаем индекс активного элемента коллекции
	 */
	getActiveIndex():number;
	/**
	 * Получаем элемент коллекции по номеру
	 * @param index
	 */
	getByIndex(index:number):any;
	/**
	 * Проверяем наличие элемента в коллекции по индексу
	 * @param index
	 */
	has(index:number):boolean;
	/**
	 * Проверяем наличие следующего от активного элемента коллекции
	 */
	hasNext():boolean;
	/**
	 * Проверяем наличие предыдущего от активного элемента коллекции
	 */
	hasPrev():boolean;

}