interface AjaxI {

    get(options:RequestOptionsI):void;
    post(options:RequestOptionsI):void;

}

interface RequestOptionsI {
    url:string;
    data?: Object;
    success?:(status:number, response:string) => void;
    error?:(error:any, request:any) => void;
}