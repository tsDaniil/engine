interface defaultDataI {

    /**
     * Содержимое данных плагина
     */
    content:any;

    /**
     * Имя модуля который должен загрузиться
     */
    module:string;

}