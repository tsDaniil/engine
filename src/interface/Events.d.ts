/// <reference path="./Utils.d.ts" />

interface UserEventI {

	type:string;

	/**
	 * Проверяем попало ли событие в область
	 * @param rect
	 */
	hitTestByRect(rect:RectI):boolean;
	/**
	 * Полчаем строку которая будет создавать событие
	 * @return {string}
	 */
	getEventString():string;
	/**
	 * Получаем копию события
	 */
	getClone():any;

    /**
     * Останавливаем всплытие событий
     */
    stopPropagation():void;

}

interface DomEventManagerEvents {
	touch?: TouchI;
	tap?: TapI;
	move?: MoveI;
	swipe?: SwipeI;
}

interface TouchI extends UserEventI {
	/**
	 * координаты
	 */
	coords: CoordsI;
	/**
	 * тип события
	 */
	type: string;
	/**
	 * статус ("start"|"end")
	 */
	status: string;
	/**
	 * время события
	 */
	startTime: number;
}

interface TapI extends UserEventI {
	/**
	 * Координаты
	 */
	coords: CoordsI;
	/**
	 * тип
	 */
	type: string;
	/**
	 * время прикосновения
	 */
	startTime: number;
	/**
	 * время
	 */
	timeStamp: number;
}

interface MoveI {
	/**
	 * Координаты тача
	 */
	coords: CoordsI;
	/**
	 * координаты старта тача
	 */
	startCoords: CoordsI;
	/**
	 * координаты тача в прошлый мув
	 */
	lastCoords: CoordsI;
	/**
	 * дистанция пройденная за весь мув
	 */
	distance: DistanceI;
	/**
	 * дистанция пройденная с последнего мува
	 */
	lastDistance: DistanceI;
	/**
	 * скорость общая
	 */
	speed: SpeedI;
	/**
	 * скорость с последнего мува
	 */
	lastSpeed: SpeedI;
	/**
	 * Тип события
	 */
	type: string;
	/**
	 * Время прикосновения
	 */
	startTime: number;
	/**
	 * набрал ли скорость необходимуб для свайпа
	 */
	timeStamp:number;
	/**
	 * текущее время
	 */
	hasSwipeSpeed: boolean;
}

interface SwipeI {
	/**
	 * Координаты
	 */
	coords: CoordsI;
	/**
	 * Координаты прикосновения
	 */
	startCoords: CoordsI;

	/**
	 * Направление свайпа
	 */
	vector: CoordsI;

	/**
	 * Направление ("left"|"right"|"up"|"down")
	 */
	direction: string;
	/**
	 * горизонтальный ли свайп
	 */
	isHorizontal: boolean;
	/**
	 * Тип
	 */
	type:string
}

interface userEvent {
	coords: CoordsI;
	time: number;
	originEvent: any;
}