/// <reference path="./BaseI.d.ts" />

/**
 * интерфейс данных из url
 */
interface SearchI {

	/**
	 * win8 в прилаге
	 */
	win8?:string;

	/**
	 * lang = (ru,en,ko,es,...)
	 */
	lang?:string;

	/**
	 * размер width вебвьюхи переданный из платформы
	 */
	webview_width?: string;

	/**
	 * размер height вебвьюхи переданный из платформы
	 */
	webview_height?: string;

	/**
	 * количество страниц переданный из платформы
	 */
	pages_count?:string;

	/**
	 * Страница с которой начинать читать
	 */
	page_index?:string;

	/**
	 * Принудительно включить режим проигрывания епаба (true|false)
	 */
	singlePageMode?:string;

	model?:string;

	max_height?: string;

	max_width?: string;

}

interface UserDataI extends BaseI {

	/**
	 * Получения данных из url в виде объекта типа SearchI
	 */
	getSearch():SearchI;

	/**
	 * Платформа Андройд
	 */
	isAndroid():boolean;

	/**
	 * Компьютер или
	 */
	isDesktop():boolean;

	isMobile():boolean;
	isTablet():boolean;

	isWindows():boolean;

	isWeb():boolean;

	isModernWebkit():boolean;
}