/**
 * Created by usercom on 13.11.14.
 */
interface SoundMangerI {

	bgSound:string;

	init():void;

	setVolumeForSoundTo (key:string, vol:number):void;

	addBg(key:string, fileName:string):void;

	fadeVolumeTo(key:string, vol:number, dur:number):void;

	playBg (key, dur?):void ;

	pauseBg (key, dur?):void ;

	stopBg (key, dur?):void ;

	addSfx (key, fileName):void ;

	playSfx (key):void ;

	unloadAllSfx():void ;

	pauseAllSounds ():void ;

	resumeAllSounds ():void ;

	setVolume? (volume:number):void ;

}