/// <reference path="./ModuleI.d.ts" />

interface ModalI extends ModuleI {

	setContent(content:string, title:string, size?:string):void;

	show():void;
	hide():void;

	getNode():$i

}