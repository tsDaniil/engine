/// <reference path="./ModuleI.d.ts" />

interface EpisodeI {
    /**
     * Название книги
     */
    bookName: string;
    sectionNumber?: number;
    issueName: string;
    issueNumber: string;
    episodeWord?: string;
    seasonNumber?: number;
	showHelp?:boolean;
}


interface PlatformManagerI extends ModuleI {
    init():void;
    startBook(active?:number, count?:number):void;
    exit():void;
    showNavigation():void;
    showSettings():void;
    setStoredValueForKey(key:string, value:string):void;
    getStoredValueForKey(key:string);
    onStoredValue(callback:(data:string)=>void):void;
    getBookData(): EpisodeI;
    setSectionNumber(pageNumber:number, pagesCount:number):void;
    sectionFinished(pageNumber:number):void;
    onSectionChanged(callback:(pageNumber:number)=>void):void;

}