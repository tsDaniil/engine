/// <reference path="./ModalI.d.ts" />

interface DownloaderI extends ModuleI {

	/**
	 * Скачиваем стилевой файл
	 * @param path путь к файлу
	 * @param [callback] каллбэк на скачивание стиля
	 * @param [time] время ожидания скачивания стиля (по умолчанию 250ms)
	 */
	loadStyle(path:string, callback?:(mesage:string)=>void, time?:number):void;

	loadEngineStyle(path:string, callback?:(mesage:string)=>void, time?:number):void;

	/**
	 * Скачиваем массив стилей
	 * @param paths Массив строк с путями до файла
	 * @param callback каллбэк на скачивание всех стилей
	 * @param time время ожидания скачивания всех стилей (по дефолту 250*количество стилей в мс)
	 */
	loadStyles(paths:Array<string>, callback?:(mesage:string)=>void, time?:number):void;

	loadEngineStyles(paths:Array<string>, callback?:(mesage:string)=>void, time?:number):void;

	/**
	 * Скачиваем картинку
	 * @param path путь до картинки
	 * @param [callback] каллбэк на скачивание картинки
	 * @param [time] время ожидания скачивания картинки (дефолт 500мс)
	 */
	loadImage(path:string, callback?:(mesage:boolean, image:HTMLImageElement)=>void, time?:number):void;
	/**
	 * Скачиваем массив картинок
	 * @param path массив путей
	 * @param [callback] каллбэк на скачивание всех картинок
	 * @param [time] время ожидания скачивания картинок (дефолт 500 * количество картинок мс)
	 */
	loadImages(path:Array<string>, callback?:(mesage:ImageDataI)=>void, time?:number,  progressCallback?:(loadSize, totalSize)=>void):void;

	/**
	 * Проверяем скачен ли стиль
	 * @param path путь до стиля
	 */
	hasStyle(path:string):boolean;

	addLitePlugin(pluginName, callback?:()=>void):void

}

interface ImageStateI {
	image: HTMLImageElement;
	state: boolean;
}

interface ImageDataI {
	each(callback:(value:ImageStateI, imageSrc?:string) => void):void;
	getNames():Array<string>;
}

