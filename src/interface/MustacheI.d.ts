interface MustacheI {

    render(template:string, data:any):string;

}
declare var Mustache:MustacheI;
declare module "mustache" {
    export = Mustache;
}