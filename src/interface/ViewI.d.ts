/// <reference path="./../utils/app/animationEngine/interfaces/CollectionElementsI.d.ts" />
/// <reference path="DomLite.d.ts" />
/// <reference path="../utils/app/animationEngine/interfaces/SceneI.d.ts" />
/// <reference path="../utils/app/animationEngine/interfaces/SceneElementI.d.ts" />

interface ViewI extends ModuleI {

    getSceneElement():SceneElementI;

    getNode():$i;

    render():void;
    draw():void;
    remove():void;
    destroy():void;

    getCalcData():any;

}