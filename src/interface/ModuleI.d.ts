/// <reference path="./BaseI.d.ts" />
/// <reference path="DomLite.d.ts" />

interface ModuleI extends BaseI {

    /**
     * Слушаем событие на объекте
     * @param object объект на котором слушаем событие
     * @param eventName имя события
     * @param handler обработчик события
     * @param [context] контекст выполнения события
     */
    listenTo(object:BaseI, eventName:string, handler?:(a?:any, b?:any, c?:any, d?:any) => any, context?:any): ModuleI;
    /**
     * Слушаем одно событие на объекте, затем отписываемся
     * @param object объект на котором слушаем
     * @param eventName имя события
     * @param handler обработчик
     * @param [context] контекст выполнения события
     */
    listenToOnce(object:BaseI, eventName:string, handler?:(a?:any, b?:any, c?:any, d?:any) => any, context?:any): ModuleI;
    /**
     * Прекращаем слушать события объекта
     * @param object
     * @param eventName
     * @param handler
     */
    stopListening(object:BaseI, eventName:string, handler?:(a?:any, b?:any, c?:any, d?:any) => any): ModuleI;

    /**
     * Запускаем функции пришедшие в каллбэке после запуска события (событие выполняется однократно), в дальнейшем функция из каллбэке выполняется сразу
     * @param event
     * @param callback
     */
    onEvent(event:string, callback:() => any): ModuleI;
    /**
     * Запускаем выполнение onEvent
     * @param event
     */
    fireEvent(event:string): ModuleI;

    /**
     * Проверяем состояние события
     * @param state
     */
    isState(state:string):boolean;

    /**
     * Частный случай onEvent("loaded")
     * @param callback
     */
    onLoad(callback:() => any): ModuleI;
    /**
     * Частный случай fireEvent("loaded");
     */
    loaded():ModuleI;

    /**
     * $("#bigrect")
     */
    view:$i;

}