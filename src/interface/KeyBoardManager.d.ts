/// <reference path="./ModuleI.d.ts" />

interface EventObjectI{
	backspace : string;
	tab : string;
	enter : string;
	shift : string;
	ctrl : string;
	alt : string;
	pause : string;
	capsLock : string;
	escape : string;
	space : string;
	pageUp : string;
	pageDown : string;
	end : string;
	home : string;
	leftArrow : string;
	upArrow : string;
	rightArrow : string;
	downArrow : string;
	insert : string;
	deleteKey : string;
}

interface  KeyBoardManagerI extends ModuleI {
	Keys: EventObjectI;
}