/// <reference path="./UserDataI.d.ts" />
/// <reference path="./LocalStorageManagerI.d.ts" />
/// <reference path="./PlatformManagerI.d.ts" />
/// <reference path="./ModuleI.d.ts" />

interface TemplaterI extends ModuleI {
	/**
	 * Получаем темплейт по имени файла (скомпилированный файл темплейта должен быть уже скачен!)
	 * @param name Имя файла
	 * @param data данные для темплейта
	 */
	getTemplate(name:string, data:Object);

	loadTemplate(templateUrl:string, callback?:()=>void):void;
}