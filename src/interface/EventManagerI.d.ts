/// <reference path="./InteractiveRectI.d.ts" />

interface EventManagerI {
    /**
     * Запуск событий тача
     * @param event
     */
    startEvent(event:UserEventI):void

    /**
     * Запуск событий тача из другого кода
     * @param eventName
     * @param args
     */
    trigger(eventName:string, args:Array<any>): EventManagerI;

    /**
     * Подписка на события тача
     * @param eventName
     * @param handler
     * @param rect
     * @param context
     */
    on(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any, rect:InteractiveRectI, context?:any): EventManagerI;

    /**
     * Отписка от событий тача
     * @param eventName
     * @param handler
     */
    off(eventName?:string, handler?:(a?:any, b?:any, c?:any, d?:any) => any): EventManagerI;

}