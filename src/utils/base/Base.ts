/// <reference path="../../interface/BaseI.d.ts" />

interface baseEventI {

    handler(a?, b?, c?, d?):any;
    context:any;

}

interface _EventsI {
    addEvent(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any, context:any):void;
    removeByEventName(eventName:string):void;
    removeHandler(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any):void;
}

class _Events implements _EventsI {

    public addEvent(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any, context:any):void {
        if (!this[eventName]) {
            this[eventName] = [];
        }
        this[eventName].push({handler: handler, context: context});
    }

    public removeByEventName(eventName:string):void {
        delete this[eventName];
    }

    public removeHandler(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any) => any):void {
        if (this[eventName]) {
            this[eventName] = this[eventName].filter(function (event:baseEventI) {
                return event.handler != handler;
            });
        }
    }

}

class Base implements BaseI {

    private _events:_EventsI = new _Events();

    public trigger(eventName:string, args?:Array<any>):BaseI {

        var base:Base = this;

        if (!eventName) {
            console.error("wrong param!");
            return this;
        }

        if (!args) {
            args = [];
        }

        if (!this._events) {
            return this;
        }

        Base.splitEvent(eventName).forEach(function (event:string, index:number) {

            if (base._events.hasOwnProperty(event)) {
                base.startUserEvent(event, Base.currentArgs(eventName, index, args.slice()));
            }

        });

        return this;
    }

    private startUserEvent(event:string, localArgs:Array<any>):void {
        this._events[event].slice().forEach(function (eventObject:baseEventI) {
            eventObject.handler.apply(eventObject.context, localArgs);
        });
    }

    private static currentArgs(eventName:string, index:number, args:Array<any>):Array<any> {
        var arr = eventName.split(":");
        arr.splice(0, index + 1);
        return arr.concat(args);
    }

    private static splitEvent(eventName:string):Array<string> {

        var events = eventName.split(":"), result = [];

        for (var i = 0; i < events.length; i++) {
            result.push(events.slice(0, i + 1).join(":"));
        }

        return result;
    }

    public on(eventName:string, handler:(a?, b?, c?, d?) => any, context?:any):BaseI {

        if (!eventName || !handler) {
            console.error("Неверные параметры!");
            return this;
        }

        this._events.addEvent(eventName, handler, context || this);

        return this;
    }

    public once(eventName:string, handler:(a?, b?, c?, d?) => any, context?:any):BaseI {

        this.on(eventName, handler, context);

        var offHandler = function () {
            this.off(eventName, handler);
            this.off(eventName, offHandler);
        }.bind(this);

        this.on(eventName, offHandler);

        return this;

    }

    public off(eventName?:string, handler?:(a?, b?, c?, d?) => any):BaseI {

        if (!eventName) {
            this._events = new _Events();
            return this;
        }

        if (!handler) {
            this._events.removeByEventName(eventName);
            return this;
        }

        this._events.removeHandler(eventName, handler);

        return this;
    }

}
export = Base;