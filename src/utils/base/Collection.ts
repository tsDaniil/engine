/// <reference path="../../interface/CollectionI.d.ts" />

import Module = require("./Module");

class Collection extends Module implements CollectionI {

    public active:number = 0;
    public collection:Array<any> = [];

    public getActive():any {
        return this.getByIndex(this.active);
    }

    public getActiveIndex():number {
        return this.active;
    }

    public getByIndex(index:number):any {
        return this.collection[index];
    }

    public has(index:number):boolean {
        return !!this.getByIndex(index);
    }

    public hasNext():boolean {
        return this.has(this.active + 1);
    }

    public hasPrev():boolean {
        return this.has(this.active - 1);
    }

}
export = Collection;