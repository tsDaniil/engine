/// <reference path="../../interface/BaseI.d.ts" />
/// <reference path="../../interface/ModuleI.d.ts" />
/// <reference path="../../interface/DomLite.d.ts" />
declare var $bigrect:$i;

import Base = require("./Base");
import $ = require("$");

class Module extends Base implements ModuleI {

    private outEvents:Object;
    private onLoadCallBacks:Array<(model:ModuleI)=>void> = [];
    private moduleEvents:Object;
    private loadState:boolean;

    public view:$i;

    constructor() {
        super();
        if (!window["$bigrect"]) {
            window["$bigrect"] = $(".bigrect");
        }
        this.view = window["$bigrect"];
        this.loadState = false;
    }

    private clearEvents(object:BaseI, eventName?:string, handler?:(a?, b?, c?, d?)=>any):void {

        if (!eventName) {
            for (var name in this.outEvents) {
                if (this.outEvents.hasOwnProperty(name)) {
                    this.outEvents[name] = this.outEvents[name].filter(function (eventObj) {
                        return eventObj.listener != object;
                    });
                }
            }
        } else if (!handler) {
            if (eventName in this.outEvents) {
                this.outEvents[eventName] = this.outEvents[eventName].filter(function (eventObj) {
                    return eventObj.listener != object;
                });
            }
        } else {
            if (eventName in this.outEvents) {
                this.outEvents[eventName] = this.outEvents[eventName].filter(function (eventObj) {
                    return eventObj.handler != handler;
                });
            }
        }

    }

    public listenTo(object:BaseI, eventName:string, handler:(a?, b?, c?, d?)=>any, context?:any):ModuleI {

        if (!eventName || !handler) {
            console.error("Неверные параметры!");
            return this;
        }

        if (!this.outEvents) {
            this.outEvents = {};
        }

        if (!this.outEvents[eventName]) {
            this.outEvents[eventName] = [];
        }

        this.outEvents[eventName].push({
            eventName: eventName, handler: handler, context: context || window, listener: object
        });

        object.on(eventName, handler, context);

        return this;
    }

    public listenToOnce(object:BaseI, eventName:string, handler:(a?, b?, c?, d?)=>any, context?:any):ModuleI {

        this.listenTo(object, eventName, handler, context);

        var offHandler = function () {
            this.stopListening(object, eventName, handler);
            this.stopListening(object, eventName, offHandler);
        }.bind(this);

        this.listenTo(object, eventName, offHandler, this);

        return this;
    }

    public stopListening(object:BaseI, eventName?:string, handler?:(a?, b?, c?, d?)=>any):ModuleI {

        //TODO Проверить
        object.off(eventName, handler);
        this.clearEvents(object, eventName, handler);

        return this;
    }

    public onEvent(eventName:string, callback:()=>any):ModuleI {

        if (!this.moduleEvents) {
            this.moduleEvents = {};
        }

        if (!this.moduleEvents[eventName]) {
            this.moduleEvents[eventName] = {
                callbacks: [],
                state: false
            }
        }

        if (this.moduleEvents[eventName].state) {
            callback();
        } else {
            this.moduleEvents[eventName].callbacks.push(callback);
        }

        return this;
    }

    public isState(state:string):boolean {
        return (this.moduleEvents && this.moduleEvents[state])
    }

    public fireEvent(eventName:string):ModuleI {

        if (!this.moduleEvents || !this.moduleEvents[eventName]) {
            return this;
        }

        this.moduleEvents[eventName].callbacks.forEach(function (callback) {
            callback.call(this);
        });
        delete this.moduleEvents[eventName].callbacks;
        this.moduleEvents[eventName].state = true;

        return this;
    }

    public onLoad(callback:()=>any):ModuleI {
        if (this.loadState) {
            callback.call(this);
            return this;
        }

        if (!this.onLoadCallBacks) {
            this.onLoadCallBacks = [];
        }
        this.onLoadCallBacks.push(callback);
        return this;
    }

    public loaded():ModuleI {

        this.loadState = true;
        this.onLoadCallBacks.forEach(function (callback) {
            callback.call(this)
        }, this);
        this.onLoadCallBacks = [];
        return this;
    }

}

export = Module;