/// <reference path="../interface/Utils.d.ts" />
declare var baseUrl:string;

declare var paths:pathsI;

paths = (function () {
    return {
        get enginePath() {
            return window["engineAdditionalURL"];
        },
        get buildPath() {
            return window["enginePluginsAdditionalURL"];
        },
        get baseUrl() {
            return baseUrl;
        }
    };
})();

(function () {

    var init = {
        /**
         * запуск инициализации
         */
        run: function () {
            if (console && console.clear) {
                console.clear();
            }
            this.setBaseUrl();
            this.loadScripts(function () {

                window["require"] = function (name) {
                    var bridgeModyles = ["AudioController", "Native", "Navigation", "VideoController", "ViewCreator", "Easing"];
                    if (bridgeModyles.some(function (moduleName) {
                            return moduleName === name;
                        })) {
                        return window["$require"](name);
                    } else {
                        return window["requirejs"].apply(window["requirejs"], arguments);
                    }
                }

            });
        },

        /**
         * Устанавливаем корневую папку для require
         */
        setBaseUrl: function () {
            baseUrl = "./";

            if (window["engineAdditionalURL"]) {
                baseUrl = window["engineAdditionalURL"];
            } else {
                window["engineAdditionalURL"] = "";
            }

            if (!window["enginePluginsAdditionalURL"]) {
                window["enginePluginsAdditionalURL"] = "";
            }
        },

        /**
         * Скачиваем require который запускает дальнейшую инициализацию проекта
         */
        loadScripts: function (callback) {
            var time = new Date().getTime();
            this.addScript(baseUrl + "utils/utils/vendor/r.js", baseUrl + "utils/app/config.js?" + time, callback);
        },

        /**
         * Скачиваем скрипт
         * @param scriptPath
         * @param [dataMain]
         * @return {HTMLElement}
         */
        addScript: function (scriptPath, dataMain, callback) {
            var script = document.createElement("script");
            if (dataMain) {
                script.setAttribute("data-main", dataMain);
            }
            script.onload = function () {
                callback();
            };
            script.src = scriptPath;
            document.head.appendChild(script);
            return script;
        }
    };

    init.run();


})();