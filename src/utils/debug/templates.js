(function() {(window["JST"] = window["JST"] || {})["console.tpl.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div id="console">\n\n    <div class="console-input">\n        <input type="text" class="input-lg"/>\n    </div>\n    <select class="history">\n    </select>\n    <span class="collapse">_</span>\n    <span class="clear">X</span>\n    <pre class="console-body">\n\n    </pre>\n\n</div>';

}
return __p
}})();
(function() {(window["JST"] = window["JST"] || {})["message.tpl.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<p class="' +
((__t = ( elementType )) == null ? '' : __t) +
'">' +
((__t = ( text )) == null ? '' : __t) +
'</p>';

}
return __p
}})();