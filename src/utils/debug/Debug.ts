/// <reference path="../../interface/require.d.ts" />
/// <reference path="../../interface/DomLite.d.ts" />
/// <reference path="../../interface/InteractiveRectI.d.ts" />
/// <reference path="../init.ts" />


import Module = require("../base/Module");
import Templater = require("../app/Templater");
import $ = require("$");

class Debug extends Module {

    private console:$i;
    private consoleBody:$i;
    private history:$i;
    private rect:InteractiveRectI;

    constructor() {
        super();
        this.init();
    }

    private init():void {

        window["debug"] = this;

        var debug:Debug = this;

        this.onLoad(function () {

            debug.addConsole();
            debug.setNewConsole();
            debug.setHandlers();

        });

        this.loadTemplate();
        Debug.loadStyles();
    }

    private setHandlers() {

        var debug:Debug = this;
        var input = this.console.find("input").first();

        window["uncollapse"] = function () {
            debug.console.show();
            $("#bigrect").find(".uncollapse").remove();
        };

        window["collapse"] = function () {
            debug.console.hide();
            $("#bigrect").append("<div class='uncollapse' style='width: 30px; height: 30px; position: absolute; bottom: 0; left: 0; background: #000000;z-index: 10;'></div>");
            setTimeout(function () {
                $(".uncollapse").tap(function () {
                    window["uncollapse"]()
                })
            }, 100);
        };

        debug.console.find(".collapse").tap(function () {
            window["collapse"]();
        });

        input.tap(function () {
            input.node.focus();
        });

        input.change(function () {

            var value:string = this.value;
            debug.addMesage("evaling", value);
            setTimeout(function () {
                debug.addMesage("log", eval(value));
            }, 0);
            debug.history.prepend("<option value='" + value + "'>" + value + "</option>");
            this.value = "";

        });

        this.history.change(function () {
            input.val(this.value);
        });

        this.console.find(".clear").tap(function () {
            console.clear();
        });

    }

    private setNewConsole():void {
        var debug:Debug = this;
        console.log = function () {
            debug.addMesage("log", Debug.concatArguments(arguments));
        };
        console.warn = function () {
            debug.addMesage("warn", Debug.concatArguments(arguments));
        };
        console.error = function () {
            debug.addMesage("error", Debug.concatArguments(arguments));
        };
        console.clear = function () {
            debug.consoleBody.empty();
        };
    }

    private loadTemplate():void {

        var debug:Debug = this;

        require(["./templates"], function () {
            debug.loaded();
        });
    }

    private addConsole():void {
        $("#bigrect").append(Templater.getTemplate("console.tpl.html", {}));
        this.consoleBody = $(".console-body");
        this.console = $("#console");
        this.history = this.console.find(".history").first();
        this.consoleBody.height(this.consoleBody.parent().height() - this.consoleBody.parent().children().first().height());
    }

    private addMesage(type:string, message:string):void {
        this.consoleBody.append(Templater.getTemplate("message.tpl.html", {type: type, text: message}));
        this.consoleBody.node.scrollTop = this.consoleBody.height();
    }

    static loadStyles():void {
        var link = document.createElement("link");
        link.rel = "stylesheet";
        link.href = paths.enginePath + "utils/debug/style.css";
        document.head.appendChild(link);
    }

    static concatArguments(args:IArguments):string {
        var result = "";
        for (var i = 0; i < args.length; i++) {
            if (typeof args[i] != "string") {
                args[i] = window["JSON"]["stringify"](args[i], "", 4);
            }
            result += args[i] + "\n";
        }
        return result;
    }
}
export = Debug;