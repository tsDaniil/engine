/// <reference path="../../../../../interface/ModuleI.d.ts" />
/// <reference path="../../interfaces/SceneManagerI.d.ts" />

import SceneCollection = require("./SceneCollection");
import Scene = require("./Scene");
import $ = require("$");
import View = require("../View/View");
import Preloader = require("../../../../utils/Preloader");

class SceneManager extends SceneCollection implements SceneManagerI {

    private node:$i;
    private mainControl:ViewI;
    private leftArrowShown:boolean = false;
    private rightArrowShown:boolean = false;

    createScenes(scenesData:MainDataI, node:$i, preLoadScenes:number):void {

        this.createMainControl(scenesData, node);

        this.node = this.mainControl.getSceneElement().getNode() || node;
        this.preLoadedScenes = preLoadScenes;

        this.addScenes(scenesData.scenes);

    }

    public getNode():$i {
        return this.node;
    }

    public drawScene(scene?:number) {

        scene = scene || 0;

        if (scene && scene > this.collection.length) {
            throw new Error("Нет такой сцены!");
        }

        this.collection[scene].draw();
        this.initScenes();

    }

    public episodeControlHandler(sceneId:string, method:string) {

        console.log(method, arguments);

        var that = this;

        var Do = {
            "next": function () {
                that.next();
            },
            "prev": function () {
                that.prev();
            }
            //TODO jumpTo:number
        };

        if (method in Do) {
            Do[method]();
        } else {
            throw new Error("Неожиданный метод для управления сценами! " + method);
        }

    }

    public jumpToScene(index:number):void {

        var direction:string;
        var that = this;

        if (this.has(index)) {

            Preloader.show();

            this.getByIndex(index).onLoad(function () {

                Preloader.hide();

                direction = (that.active - index < 0 ? "next" : "prev");
                that.getActive().hide(direction);
                that.active = index;
                that.getActive().show(direction);
                that.initScenes();

                that.trigger("SceneManager:changedScene", [that.active]);

            });

        }

    }

    private createMainControl(scenesData:MainDataI, node:$i):void {
        if (scenesData.bookControl) {
            this.mainControl = new View(node, scenesData.bookControl);
            this.mainControl.draw();
            this.setMainControlHandlers();
            this.initArrows();
        }
    }

    private setMainControlHandlers():void {
        this.listenTo(this.mainControl, "View:episodeControl", this.episodeControlHandler, this);
        this.on("SceneManager:changedScene", this.initArrows, this);
    }

    private initArrows():void {

        var that = this;

        this.mainControl.onLoad(function () {

            if (!that.leftArrowShown && that.active != 0) {
                that.mainControl.trigger("View:animation:prev:show", []);
                that.leftArrowShown = true;
            }

            if (!that.rightArrowShown && that.active != that.collection.length - 1) {
                that.mainControl.trigger("View:animation:next:show", []);
                that.rightArrowShown = true;
            }

            if (that.leftArrowShown && that.active == 0) {
                that.mainControl.trigger("View:animation:prev:hide", []);
                that.leftArrowShown = false;
            }

            if (that.rightArrowShown && that.active == that.collection.length - 1) {
                that.mainControl.trigger("View:animation:next:hide", []);
                that.rightArrowShown = false;
            }

        });
    }

    private addScenes(scenesData:Array<MainSceneI>):void {
        var that = this;
        scenesData.forEach(function (sceneData:MainSceneI, index:number) {
            that.collection.push(new Scene(sceneData, that, index));
        });
    }

}

var manager:SceneManagerI = new SceneManager();
export = manager;