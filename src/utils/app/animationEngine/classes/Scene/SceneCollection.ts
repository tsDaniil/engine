/// <reference path="../../interfaces/SceneCollectionI.d.ts" />
/// <reference path="../../interfaces/SceneI.d.ts" />

import Collection = require("../../../../base/Collection");


class SceneCollection extends Collection implements SceneCollectionI {

    public collection:Array<SceneI> = [];
    public preLoadedScenes:number = 0;

    public sendToCollection(method:string, args?:Array<any>):void {
        this.collection.forEach(function (scene:SceneI) {
            scene[method].apply(scene, args || []);
        });
    }

    public next():void {
        this.jumpToScene(this.active + 1);
    }

    public prev():void {
        this.jumpToScene(this.active - 1);
    }

    public jumpToScene(index:number):void {
        throw new Error("Метод не переопределен!");
    }

    public initScenes():void {

        var active = this.active,
            preloadedScenes = this.preLoadedScenes;

        this.collection.forEach(function (Scene:SceneI, index:number) {
            if (Math.abs(active - index) <= preloadedScenes) {
                Scene.preLoad();
            } else {
                Scene.unLoad();
            }
        });
    }

}

export = SceneCollection;