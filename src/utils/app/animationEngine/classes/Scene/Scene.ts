/// <reference path="../../interfaces/SceneManagerI.d.ts" />
/// <reference path="../../interfaces/SceneI.d.ts" />


import Module = require("../../../../base/Module");
import View = require("../View/View");


class Scene extends Module implements SceneI {

    private data:MainSceneI;
    private parent:SceneManagerI;
    private $view:ViewI;
    private index:number = 0;

    constructor(data:MainSceneI, parent:SceneManagerI, index:number) {
        super();

        this.index = index;
        this.data = data;
        this.parent = parent;
    }

    public preLoad():void {
        var that = this;
        if (!this.$view) {
            this.$view = new View(this.parent.getNode(), this.data);
            this.setHandlers();
            this.$view.onLoad(function () {
                that.loaded();
            });
        }
    }

    public draw():void {
        if (!this.$view) {
            this.preLoad();
        }
        this.$view.draw();
    }

    public unLoad():void {
        if (this.$view) {
            this.$view.remove();
        }
    }

    public remove():void {
        this.$view.remove();
    }

    public next():void {
        this.parent.next();
    }

    public prev():void {
        this.parent.prev();
    }

    public jumpTo(index:number):void {
        this.parent.jumpToScene(index);
    }

    public show(direction:string):void {
        this.$view.getSceneElement().show("show-" + direction);
    }

    public hide(direction:string):void {

        this.$view.getSceneElement().hide("hide-" + direction);

    }

    private setHandlers():void {
        this.listenTo(this.$view, "View:episodeControl", this.parent.episodeControlHandler, this);
    }

}
export = Scene;