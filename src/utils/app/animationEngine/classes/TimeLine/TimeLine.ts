/// <reference path="../../interfaces/TimeLineI.d.ts" />
/// <reference path="../../../../../interface/tweenjs.d.ts" />

import Module = require("../../../../base/Module");
import Tween = require("createjs");

class TimeLine extends Module implements TimeLineI {

    private listeners:Array<ModuleI> = [];
    private needTimeLine:boolean = false;

    constructor() {
        super();
        this.setHandlers();
    }

    private setHandlers():void {
        this.once("TimeLine:startTimeLine", this.startTimeLine, this);
    }

    private startTimeLine():void {
        console.log("timelineStart")
        this.needTimeLine = true;
        var that = this;
        var oldTime = new Date().getTime();

        var timeLine = function () {
            var time = new Date().getTime();
            Tween.Tween.tick(time - oldTime);
            oldTime = time;
            that.trigger("TimeLine:render");
            if (that.needTimeLine) {
                requestAnimationFrame(timeLine);
            } else {
                console.log("timelineStop")
            }
        };

        this.once("TimeLine:stopTimeLine", this.stopTimeLine, this);
        requestAnimationFrame(timeLine);
    }

    private stopTimeLine():void {
        console.log("Stop");
        this.setHandlers();
        this.needTimeLine = false;
    }

    public addListener(object:ModuleI):void {
        this.listeners.push(object);
        this.trigger("TimeLine:startTimeLine", []);
    }

    public removeListener(object:ModuleI):void {
        this.listeners = this.listeners.filter(function (listener:ModuleI) {
            return listener !== object;
        });
        if (!this.listeners.length) {
            this.trigger("TimeLine:stopTimeLine", []);
        }
    }

}
var timeLine:TimeLineI = new TimeLine();
export = timeLine;