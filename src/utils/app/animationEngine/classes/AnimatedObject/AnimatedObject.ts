/// <reference path="../../interfaces/AnimatedObjectI.d.ts" />

import Module = require("../../../../base/Module");

class AnimatedObject extends Module implements AnimatedObjectI {

    public x:number = 0;
    public y:number = 0;
    public width:number = 0;
    public height:number = 0;
    public alpha:number = 1;
    public scaleX:number = 1;
    public scaleY:number = 1;
    public rotateX:number = 0;
    public rotateY:number = 0;
    public rotateZ:number = 0;
    public originX:number = 0.5;
    public originY:number = 0.5;

    constructor(params) {
        super();
        this.setParams(params);
    }

    private setParams(params):void {
        for (var paramName in params) {
            if (params.hasOwnProperty(paramName)) {
                this[paramName] = params[paramName];
            }
        }
    }

    public draw():animatedParams {
        return {
            transform: this.getTransformStr(),
            width: this.width + "px",
            height: this.height + "px",
            opacity: this.alpha,
            transformOrigin: (this.width * this.originX) + "px " + (this.height * this.originY) + "px"
        }
    }

    private getTransformStr():string {
        return "translate(" + this.x + "px, " + this.y + "px)"
            + " rotate(" + this.rotateZ + "deg)"
            + " scale(" + this.scaleX + ", " + this.scaleY + ")";
    }

    public static map:Array<string> = [
        "width", "height", "alpha", "transform", "origin"
    ];

}
export = AnimatedObject;