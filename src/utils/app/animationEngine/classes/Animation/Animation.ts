/// <reference path="../../../../../interface/tweenjs.d.ts" />
/// <reference path="../../interfaces/TimeLineI.d.ts" />
/// <reference path="../../interfaces/ViewElementI.d.ts" />
/// <reference path="../../interfaces/AnimationI.d.ts" />


import Module = require("../../../../base/Module");
import Tween = require("createjs");
import TimeLine = require("../TimeLine/TimeLine");

class Animation extends Module implements AnimationI {

    private static animations:any = {};
    private static playAnimations:any = {};
    public static pause:boolean = false;
    public static play:boolean = false;
    public static fps:number = 30;

    constructor() {
        super();
        this.listenTo(TimeLine, "TimeLine:render", this.update, this);
    }

    public animateViewElement(ViewElement:ViewElementI, data, callback):string { 
        var id = ViewElement.getCalcData().id + ":" + data.name;

        if (!Animation.animations[id]) {
            this.addAnimation(id, ViewElement.getAnimatedObject(), data, ViewElement.draw.bind(ViewElement), callback);
        }

        this.playAnimation(id);

        return id;
    }

    public addAnimation(id:string, element:any, data:AnimationAI, render?:()=>any, callback?:()=>any):string {

        id = id || Date.now().toString();

        if (Animation.animations[id]) {
            console.log("Анимация с таким id уже существует");
            return undefined;
        }


        Animation.animations[id] = {
            id: id,
            element: element,
            draw: (render ? render : function () {
            }),
            callback: (render ? render : function () {
            }),
            state: false,
            isPlay: false,
            isEnd: false,
            isStart: false,
            time: (data.time === undefined ? 300 : data.time),
            timingFunction: data.timingFunction,
            animationDirection: data.animationDirection,
            positions: data.positions
        };

        this.parsePositions(id);

        return id;
    }

    public removeAnimation(id:string):void {
        TimeLine.removeListener(Animation.animations[id].element);
        Tween.Tween.removeTweens(Animation.animations[id].element);
        delete Animation.playAnimations[id];
        delete Animation.animations[id];
    }

    public pauseAll():void {
    }

    public playAll():void {
    }

    public togglePlay():void {

    }

    public playTo(element:any, data:animationSourceI, render?:()=>any):void {

    }

    public playFrom(element:any, data:animationSourceI, render?:()=>any):void {

    }

    public play(element:any, data:Array<animationSourceI>, render?:()=>any, calback?:()=>any):void {

    }

    public playAnimation(id:string):void {
        var tween = Tween.Tween.get(Animation.animations[id].element, {override: true});
        var animation = Animation.animations[id];
        var i:number;
        Animation.playAnimations[id] = true;
        Animation.play = true;

        if (animation.animationDirection == "normal" || !animation.state) {
            for (i = 0; i < animation.positions.length; i++) {
                tween.to(animation.positions[i].params, animation.positions[i].stageTime, Tween.Ease[animation.timingFunction]);
            }

            if (animation.animationDirection == "toggle") {
                animation.state = true;
            }
        } else if (animation.animationDirection == "toggle" && animation.state) {
            for (i = animation.positions.length; i--;) {
                tween.to(animation.positions[i].params, animation.time - animation.positions[i].stageTime, Tween.Ease[animation.timingFunction]);
            }
            animation.state = false;
        }

        tween.call(function () {
            TimeLine.removeListener(Animation.animations[id].element);
            Animation.animations[id].draw();
            Animation.animations[id].callback();
            delete Animation.playAnimations[id];
        });

        TimeLine.addListener(Animation.animations[id].element);

    }

    private update():void {

        for (var id in Animation.playAnimations) {
            if (Animation.playAnimations.hasOwnProperty(id)) {
                Animation.animations[id].draw();
            }
        }

        if (!id) {
            Animation.play = false;
        }
    }

    private parsePositions(id:string):void {

        var usedParams = {};

        var start = false;
        var end = false;

        var parseData = Animation.animations[id];
        var time = parseData.time;
        var positions = parseData.positions;
        var self = this;

        var animateTo;

        for (var i = 0; i < positions.length; i++) {

            if (positions[i].percents == 0) start = true;
            if (positions[i].percents == 100) end = true;

            animateTo = {
                stageTime: time * (positions[i].percents || 0) / 100,
                params: {}
            };

            for (var param in positions[i]) {
                if (!positions[i].hasOwnProperty(param) || param == "percents") continue;
                animateTo.params[param] = positions[i][param];
                usedParams[param] = true;
            }

            Animation.animations[id].positions[i] = animateTo;

        }

        //////////////Не тестировал///////////////

        if(!(start || end)){
            throw "Не задано начало и конец анимации";
        }else if(!start) {
            animateTo.stageTime = 0;
            animateTo.params = {};
            for(var param in usedParams){
                animateTo.params[param] = Animation.animations[id].element[param];
            }
            Animation.animations[id].positions.push(animateTo);
        } else if(!end){
            animateTo.percents =  time;
            animateTo.params = {};
            for(var param in usedParams){
                animateTo.params[param] = Animation.animations[id].element[param];
            }
            Animation.animations[id].positions.push(animateTo);
        }

        //////////////Не тестировал///////////////

        Animation.animations[id].positions.sort(function (a, b) {
            return a.stageTime - b.stageTime
        });
    }

}

var animation:AnimationI = new Animation();

export = animation;