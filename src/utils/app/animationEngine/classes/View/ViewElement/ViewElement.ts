/// <reference path="../../../interfaces/ViewElementI.d.ts" />

import CollectionElements = require("../CollectionElements");
import AnimatedObject = require("../../AnimatedObject/AnimatedObject");
import Calc = require("../Calc");
import $ = require("$");
import Animation = require("../../Animation/Animation");

interface InteractiveType {
    actionType?: string;
}

interface HasNameI {
    name:string;
}

class ViewElement extends CollectionElements implements ViewElementI {

    /**
     * Текущий элемент
     */
    public node:$i;
    /**
     * Объект хранящий онформацию о положении элемента
     * Возвращает стили для отрисовки
     */
    public animtedObject:AnimatedObjectI;

    /**
     * Закешированные и просчитанные данные анимаций (с пересчетом на локальные пиксели)
     */
    public animationData:Array<CurrentedAnimationPositionsI>;

    public data:templateElementType;

    constructor(data:templateElementType, parent:any) {
        super();

        this.data = data;
        this.parent = parent;

        this.init();
    }

    public getNode():$i {
        return this.node;
    }

    /**
     * Инициализация коллекции
     * Переопределен в ContainerElement
     */
    public initCollection():void {
    }

    /**
     * Создаем ноду (переопределен во всех наследниках)
     */
    public createNode():void {
        throw new Error("Метод не переопределен!");
    }

    /**
     * Повторные отрисовки элементов
     */
    public render():void {
        //TODO написать
    }

    /**
     * Первичная отрисовка элементов
     */
    public draw():void {

        if (!this.node.node.parentNode) {
            this.getParent().getNode().append(this.node);
        }

        this.node.css(this.animtedObject.draw());
        this.sendDraw();
    }

    /**
     * Удаляем дом
     */
    public remove():void {
        if (this.node) {
            this.node.remove();
        }
    }

    public removeHandlers():void {
        if (this.data.animation && this.data.animation.length) {
            this.stopListening(this.getRoot());
        }
    }

    /**
     * Удаляем дом и все коллекции (для окончательного удаления объектов)
     */
    public destroy():void {
        this.remove();
        this.node = null;
        this.sendDestroy();
        this.collection = [];
    }

    /**
     * Получаем анимационный объект
     * @return {AnimatedObjectI}
     */
    public getAnimatedObject():AnimatedObjectI {
        return this.animtedObject;
    }

    /**
     * Устанавливаем id на элемент
     */
    public setId():void {
        this.node.attr("id", this.data.id);
    }

    /**
     * Устанавливаем классы на элементы
     */
    public setClasses():void {
        this.node.addClass("view-element" + (this.data.className ? " " + this.data.className : ""));
    }

    /**
     * Устанавливаем z-index элемента
     */
    public setLayerIndex():void {
        if (this.data.zIndex) {
            this.node.css("z-index", this.data.zIndex.toString());
        }
    }

    /**
     * Получаем строку для запуска события на рутовом объекте
     * @param interactive
     * @return {string}
     */
    public getInteractiveTriggerStr(interactive:InteractiveI):string {
        return "View:" + ViewElement.getInteractiveType(interactive) +
            ":" + interactive.animateId + ":" + interactive.animateName;
    }

    /**
     * Получаем строку для слушанья события на текущем элементе
     * @param interactive
     */
    public getStrForBindInteractive(interactive:InteractiveType):string {
        return "User:" + ViewElement.getInteractiveAction(interactive);
    }

    /**
     * Получаем строку для подписки на выполнение анимации на рутовом объекте
     * @param animation
     */
    public getStrForBindAnimation(animation:AnimationTemplateI):string {
        return "View:animation:" + this.data.id + ":" + animation.name;
    }

    public calcAnimation(calculate):void {
        if (this.data.animation) {
            this.animationData = calculate.getAnimation();
        }
    }

    public calcText(calculate):void {

    }

    /**
     * Инициализация
     */
    private init():void {

        this.currentSize();
        this.initNode();
        this.setHandlers();
        this.initCollection();

    }

    /**
     * Инициализация дом элемента
     */
    private initNode():void {

        this.createNode();
        this.setId();
        this.setClasses();
        this.setLayerIndex();

    }

    /**
     * Рассчет размеров элемента, анимационных положений и текстовых параметров
     */
    private currentSize():void {

        var calculate = Calc.calculate(this.getParent().getCalcData(), this.getCalcData());
        this.calcAnimation(calculate);
        this.calcText(calculate);
        this.data.size = calculate.getNotCurrentSize();
        this.animtedObject = new AnimatedObject(calculate.getParams());
    }

    /**
     * Назначаем обработчики
     */
    public setHandlers():void {

        var that = this;

        if (this.data.interactive && that.data.interactive.length) {
            this.data.interactive.forEach(function (interactive:InteractiveI) {
                that.getNode().on(that.getStrForBindInteractive(interactive), function (event) {
                    that.getRoot().trigger(that.getInteractiveTriggerStr(interactive), [event]);
                }, that);
            });
        }

        if (this.data.animation && this.data.animation.length) {
            this.data.animation.forEach(function (animation:AnimationTemplateI) {
                that.listenTo(that.getRoot(), that.getStrForBindAnimation(animation), function () {
                    console.log("Проигрываем пнимацию для " + this.data.id);
                    Animation.animateViewElement(this, that.getCaculatedAnimationData(animation));
                }, that);
            });
        }

    }

    public getCaculatedAnimationData(animation:HasNameI) {
        var result;
        this.animationData.some(function (data) {
            if (data.name == animation.name) {
                result = data;
                return true;
            }
        });
        return result;
    }

    /**
     * Получаем тип события для подписки на элементе
     * @param interactive
     * @return {string|string}
     */
    private static getInteractiveAction(interactive:InteractiveType):string {
        return interactive.actionType || "tap";
    }

    /**
     * Получаем тип интерактива
     * @param interactive
     * @return {string|string}
     */
    private static getInteractiveType(interactive:InteractiveI) {
        return interactive.interactiveType || "animation";
    }
}

export = ViewElement;