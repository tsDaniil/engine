import ViewElement = require("./ViewElement");
import $ = require("$");

import ImageElement = require("./ImageElement");
import VideoElement = require("./VideoElement");
import TextElement = require("./TextElement");

class ContainerElement extends ViewElement implements ViewElementI {

    public createNode():void {
        this.node = $(document.createElement("div"));
    }

    public initCollection():void {
        this.createCollection();
    }

    public createCollection():void {

        var that = this;

        var Do = {
            "image": function (element:templateElementType) {
                that.collection.push(new ImageElement(element, that));
            },
            "container": function (element:templateElementType) {
                that.collection.push(new ContainerElement(element, that));
            },
            "text": function (element:templateElementType) {
                that.collection.push(new TextElement(element, that));
            },
            "video": function (element:templateElementType) {
                that.collection.push(new VideoElement(element, that));
            }
        };

        if (this.data.content) {
            this.data.content.forEach(function (element:templateElementType) {
                if (element.elementType in Do) {
                    Do[element.elementType](element);
                } else {
                    throw new Error("Неверный тип элемента!");
                }
            });
        }

    }

}
export = ContainerElement;