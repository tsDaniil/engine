import ViewElement = require("./ViewElement");
import $ = require("$");

class ImageElement extends ViewElement implements ViewElementI {

    public createNode():void {
        this.node = $(new Image());
        if (!this.data.image || !this.data.image.src) {
            throw new Error("Нет картинки или пути картинки!");
        }
        this.setLoadHandlers();
        this.node.node["src"] = this.data.image.src;
    }

    private setLoadHandlers():void {
        var that = this;
        var onload = function () {
            that.getRoot().trigger("View:loaded:" + that.data.id, []);
        };
        var onerror = function () {
            that.getRoot().trigger("View:loaded:" + that.data.id, []);
            console.error("Не удалось скачать картинку!", this);
        };
        this.node.node.onload = onload;
        this.node.node.onerror = onerror;
    }

}
export = ImageElement;