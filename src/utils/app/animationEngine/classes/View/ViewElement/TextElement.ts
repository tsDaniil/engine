import ViewElement = require("./ViewElement");
import $ = require("$");

class TextElement extends ViewElement implements ViewElementI {

    /**
     * Закешированные и просчитанные данные текста (с пересчетом на локальные пиксели)
     */
    public textData:TemplateTextI;

    public createNode():void {
        this.node = $(document.createElement("p"));
        this.setTextStyle();
    }

    public calcText(calculate):void {
        if (this.data.text) {
            this.textData = calculate.getTextData();
        }
    }

    private setTextStyle():void {

        if (!this.textData) {
            throw new Error("Нет объекта описания текста!");
        }

        var that = this;

        var Do = {
            "text": function (text:string) {
                that.node.html(text.replace(/\n/g, "<br />"));
            },
            "lineHeight": function (lieHeight:string) {
                that.node.css("line-height", lieHeight);
            },
            "fontSize": function (fontSize:string) {
                that.node.css("font-size", fontSize);
            },
            "fontName": function (name:string) {
                that.node.css("font-family", name);
            },
            "nowrap": function (isNowrap:boolean) {
                that.node.css("white-space", isNowrap ? "nowrap" : "normal");
            },
            "bold": function (isBold:boolean) {
                that.node.css("font-weight", isBold ? "bold" : "normal");
            },
            "italic": function (isItalic:boolean) {
                that.node.css("font-style", isItalic ? "italic" : "normal");
            },
            "textDecoration": function (textDecoration:string) {
                that.node.css("text-decoration", textDecoration);
            },
            "horizontalAlign": function (align:string) {
                that.node.css("text-align", align);
            }
        };

        $.each(this.textData, function (value, paramName) {
            if (paramName in Do) {
                Do[paramName](value);
            } else {
                throw new Error("Неверный формат данных text! " + paramName);
            }
        });

    }

}
export = TextElement;