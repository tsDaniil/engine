import ViewElement = require("./ViewElement");
import $ = require("$");
import MediaObject = require("../../../../../utils/videoManager/MediaObject");

class VideoElement extends ViewElement implements ViewElementI {

    public mediaManager:MediaObjectI;

    public createNode():void {

        this.mediaManager = new MediaObject();

        this.mediaManager.createMediaElement({
            mediaType: "video",
            src: this.data.video.src,
            width: this.getAnimatedObject().width,
            height: this.getAnimatedObject().height
        });
    }

    public draw():void {
        if (!this.mediaManager.getMediaElement().parentNode) {
            this.mediaManager.appendTo(this.getParent().getNode());
            $(this.mediaManager.getMediaElement()).css(this.animtedObject.draw());
            if (this.data.video.autoPlay) {
                this.mediaManager.play();
            }
        }
    }

    public setHandlers():void {

        var that = this;

        if (this.data.interactive) {

            if (this.data.interactive && this.data.interactive.length) {

                this.data.interactive.forEach(function (interactive:InteractiveI) {

                    that.getNode().on("User:" + (interactive.actionType || "tap"), function (event) {
                        that.getRoot().trigger("View:" + (interactive.interactiveType || "animation") + ":" + interactive.animateId + ":" + interactive.animateName, [event]);
                    });

                });

            }

        }

        this.listenTo(this.getRoot(), "View:stop", function () {

            this.mediaManager.stop();

        }, this);

        this.listenTo(this.getRoot(), this.getEventStrForBind(), this.playAction, this);

        if (this.data.video.interactive && this.data.video.interactive.length) {

            this.data.video.interactive.forEach(function (interactive:InteractiveI) {

                var handler = function (eventName:string) {
                    that.listenTo(that.mediaManager, eventName, function (event) {
                        that.getRoot().trigger(this.getInteractiveTriggerStr(interactive), [event]);
                    }, that);
                };

                var Do = {
                    "canplay": function () {
                        handler(MediaObject.allEvents.canplay);
                    },
                    "play": function () {
                        handler(MediaObject.allEvents.play);
                    },
                    "pause": function () {
                        handler(MediaObject.allEvents.pause);
                    },
                    "error": function () {
                        handler(MediaObject.allEvents.error);
                    },
                    "ended": function () {
                        handler(MediaObject.allEvents.ended);
                    }
                };

                if (interactive.actionType in Do) {
                    Do[interactive.actionType]();
                } else {
                    throw new Error("Неправильный тип события! " + interactive.actionType);
                }

            });

        }

    }

    public setId():void {
        this.mediaManager.getMediaElement().setAttribute("id", this.data.id);
    }

    public setClasses():void {
        this.mediaManager.getMediaElement().className = "view-element" + (this.data.className ? " " + this.data.className : "");
    }

    public getEventStrForBind():string {
        return "View:videoControl:" + this.data.id;
    }

    /**
     * Действие которое должен выполнить видеоплеер
     * @param actionType
     * "pause" | "play" | "playToggle"(default)
     */
    private playAction(actionType:string):void {

        var that = this;

        var Do = {
            "pause": function () {
                that.mediaManager.pause();
            },
            "play": function () {
                that.mediaManager.play();
            },
            "playToggle": function () {
                that.mediaManager.playToggle();
            }
        };

        if (actionType in Do) {
            Do[actionType]();
        } else {
            throw new Error("Неизвестный тип действия для управления видео! " + actionType);
        }
    }
}
export = VideoElement;