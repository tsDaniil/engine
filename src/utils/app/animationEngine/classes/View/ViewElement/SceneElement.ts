/// <reference path="../../../../../../interface/ViewI.d.ts" />


import ContainerElement = require("./ContainerElement");
import Animation = require("../../Animation/Animation");


class SceneElement extends ContainerElement implements ViewElementI {

    public parent:ViewI;

    /**
     * Назначаем обработчики
     */
    public setHandlers():void {}

    public hide(direction:string):void {

        var that = this;

        this.getRoot().trigger("View:stop", []);

        this.getRoot().trigger("Scene:animation:start", []);
        Animation.animateViewElement(this, this.getCaculatedAnimationData({name: direction}), function () {
            that.getRoot().trigger("Scene:animation:end", []);
        });

    }

    public show(direction:string):void {

        if (!this.node.node.parentNode) {
            this.draw();
        }

        var that = this;
        that.getRoot().trigger("Scene:animation:start", []);
        Animation.animateViewElement(this, this.getCaculatedAnimationData({name: direction}), function () {
            that.getRoot().trigger("Scene:animation:end", []);
        });

    }

    public getStrForBindAnimation():string {
        return "View:episodeControl:" + this.data.id;
    }

    /**
     * Устанавливаем классы на элементы
     */
    public setClasses():void {
        this.node.addClass("view-element scene-element" + (this.data.className ? " " + this.data.className : ""));
    }

}
export = SceneElement;