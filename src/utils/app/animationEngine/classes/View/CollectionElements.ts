/// <reference path="../../interfaces/CollectionElementsI.d.ts" />

import Module = require("../../../../base/Module");
import $ = require("$");

class CollectionElements extends Module implements CollectionElementsI {

    public collection:Array<CollectionElementsI> = [];
    public parent:any;
    public data:any;
    public node:$i;

    public getParent():CollectionElementsI {
        return this.parent;
    }

    public getCalcData():templateElementType {
        return $.cloneLite(this.data);
    }

    public getNode():$i {
        return this.node;
    }

    public getRoot():CollectionElementsI {
        return this.getParent().getRoot();
    }

    public sendRender():void {
        this.sendToCollection("render", []);
    }

    public sendDraw():void {
        this.sendToCollection("draw", []);
    }

    public sendRemove():void {
        this.sendToCollection("remove", []);
    }

    public sendDestroy():void {
        this.sendToCollection("destroy", []);
    }

    public sendToCollection(method:string, args?:Array<any>):void {
        if (this.collection.length) {
            this.collection.forEach(function (element:CollectionElementsI) {
                element[method].apply(element, args || []);
            });
        }
    }

}
export = CollectionElements;