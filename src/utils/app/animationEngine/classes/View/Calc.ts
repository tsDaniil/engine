/// <reference path="../../interfaces/Calc.d.ts" />

class CalculateObjectSize implements CalculateObjectSizeI {

    private width:number;
    private height:number;
    private right:number;
    private top:number;
    private left:number;
    private bottom:number;
    private globalWidth:number = 0;
    private globalHeight:number = 0;
    private calcObject:templateElementType;
    private x:number = 0;
    private y:number = 0;
    private dx:number = 0;
    private dy:number = 0;
    private scaleX:number = 1;
    private scaleY:number = 1;
    private rotateX:number = 0;
    private rotateY:number = 0;
    private rotateZ:number = 0;
    private originX:number = 0;
    private originY:number = 0;
    private alpha:number = 1;
    private static ratio:number = 1;
    private animations:any = {};
    private text:TemplateTextI;

    public calculate(parentObject:templateElementType, calcObject:templateElementType):CalculateObjectSizeI {

        if (!parentObject || !parentObject.size || !parentObject.size.width || !parentObject.size.height) {
            throw "Нет парента и/или его размера size.width, size.height";
        }
        this.clear();

        if (parentObject["id"] == "root") {
            this.setRatio(parentObject);
        } else {
            this.globalWidth = parseInt(parentObject.size.width);
            this.globalHeight = parseInt(parentObject.size.height);
        }

        this.calcObject = calcObject;

        this.calculateHeight();
        this.calculateWidth();
        this.calculateMargin();
        this.calculateHorizontalAlignment();
        this.calculateVerticalAlignment();
        this.calculateAnimations();
        this.calculateText();
        this.calculateOther();

        this.x = (this.left || 0) + this.dx;
        this.y = (this.top || 0) + this.dy;

        return this;
    }

    public getNotCurrentSize():TemplateSizeI {
        return {
            width: this.width + "px",
            height: this.height + "px"
        };
    }

    public getParams():AnimatedObjectParamsI {
        return {
            x: this.x * CalculateObjectSize.ratio,
            y: this.y * CalculateObjectSize.ratio,
            width: this.width * CalculateObjectSize.ratio,
            height: this.height * CalculateObjectSize.ratio,
            alpha: this.alpha,
            scaleX: this.scaleX,
            scaleY: this.scaleY,
            rotateX: this.rotateX,
            rotateY: this.rotateY,
            rotateZ: this.rotateZ,
            originX: this.originX,
            originY: this.originY
        }
    }

    public getTextData():TemplateTextI {
        return this.text;
    }

    public getDefaultTransformString():string {
        return "translate(0px,0px) rotate(0deg) scale(1)";
    }

    public getAbsoluteTransformString():string {
        return "translate(" + (this.x ) * CalculateObjectSize.ratio + "px," +
            (this.y ) * CalculateObjectSize.ratio + "px) rotate(" +
            this.rotateZ + "deg) scale(" + this.scaleX + ", " + this.scaleY + ")";
    }

    public getPosition():ReturnStyleI {
        var style:any = {};

        style.left = this.x * CalculateObjectSize.ratio + "px";
        style.top = this.y * CalculateObjectSize.ratio + "px";
        style.position = "absolute";

        style.transform = this.getDefaultTransformString();
        style.transformOrigin = (this.originX * 100) + "% " + (this.originY * 100) + "%";

        if (this.width) {
            style.width = this.width * CalculateObjectSize.ratio + "px";
        }

        if (this.height) {
            style.height = this.height * CalculateObjectSize.ratio + "px";
        }

        return style;
    }

    public getAnimation():any {
        return this.animations;
    }

    public getTransformPosition():ReturnStyleI {
        var style:any = {};

        style.left = 0 + "px";
        style.top = 0 + "px";
        style.position = "absolute";
        style.transform = this.getAbsoluteTransformString();
        style.transformOrigin = this.originX * 100 + "% " + this.originY * 100 + "%";
        if (this.width) {
            style.width = this.width * CalculateObjectSize.ratio + "px";
        }

        if (this.height) {
            style.height = this.height * CalculateObjectSize.ratio + "px";
        }

        return style;
    }

    private calculateOther():void {
        this.alpha = this.calcObject.alpha;
    }

    private calculateText() {

        this.text = this.calcObject.text;

        if (!this.text) return undefined;

        this.text.fontSize = this.text.fontSize ? parseFloat(this.text.fontSize) * CalculateObjectSize.ratio + "px" : undefined;
        this.text.lineHeight = this.text.lineHeight ? parseFloat(this.text.lineHeight) * CalculateObjectSize.ratio + "px" : undefined;
    }

    private calculateAnimations():void {

        if (!this.calcObject.animation) {
            return undefined;
        }

        var that = this;

        this.animations = [];

        this.calcObject.animation.forEach(function (animation:AnimationTemplateI) {
            that.animations.push({
                time: animation.time || 300,
                timingFunction: animation.timingFunction || "linear",
                animationDirection: animation.animationDirection || "normal",
                positions: animation.positions.map(function (el:AnimatedPositionI) {

                    var newEl:any = {};
                    if (el.percent !== undefined) newEl.percents = el.percent || 0;
                    if (el.alpha !== undefined) newEl.alpha = el.alpha;

                    var currentValue;

                    for (var param in el) {
                        if (!el.hasOwnProperty(param)) continue;
                        currentValue = el[param];
                        switch (param) {
                            case "size":
                                if (currentValue.width !== undefined) newEl.width = parseInt(that.parseAnimateTplValue(currentValue.width)) * CalculateObjectSize.ratio;
                                if (currentValue.height !== undefined) newEl.height = parseInt(that.parseAnimateTplValue(currentValue.height)) * CalculateObjectSize.ratio;
                                break;
                            case "margin":
                                if (currentValue.left !== undefined) newEl.x = parseInt(that.parseAnimateTplValue(currentValue.left)) * CalculateObjectSize.ratio;
                                if (currentValue.right !== undefined) newEl.x = (that.globalWidth - parseInt(that.parseAnimateTplValue(currentValue.right)) - that.width) * CalculateObjectSize.ratio;
                                if (currentValue.top !== undefined) newEl.y = parseInt(that.parseAnimateTplValue(currentValue.top)) * CalculateObjectSize.ratio;
                                if (currentValue.bottom !== undefined) newEl.y = (that.globalHeight - parseInt(that.parseAnimateTplValue(currentValue.bottom)) - that.height) * CalculateObjectSize.ratio;
                                break;
                            default :
                                newEl[param] = currentValue;
                        }
                    }
                    return newEl;
                }, that),
                name: animation.name
            });
        });

    }

    private parseAnimateTplValue(value:string):string {
        return value.replace("{{width}}", this.width.toString())
            .replace("{{height}}", this.height.toString());
    }

    private setRatio(parentObject:templateElementType):void {
        var ow = parseInt(parentObject["size"].width);
        var oh = parseInt(parentObject["size"].height);

        var iw = parseInt(parentObject["originSize"].width);
        var ih = parseInt(parentObject["originSize"].height);

        var rW:number = ow / iw;
        var rH:number = oh / ih;

        var min = Math.min(rW, rH);

        //if (ow / oh > iw / ih) {
        //    min = Math.min(rW, rH);
        //} else {
        //    min = Math.max(rW, rH);
        //}

        CalculateObjectSize.ratio = min;
        this.globalHeight = parseInt(parentObject["size"].height) / min;
        this.globalWidth = parseInt(parentObject["size"].width) / min;
    }

    private clear():void {
        this.animations = undefined;
        this.width = undefined;
        this.height = undefined;
        this.right = undefined;
        this.top = undefined;
        this.left = undefined;
        this.bottom = undefined;
        this.globalWidth = 0;
        this.globalHeight = 0;
        this.x = 0;
        this.y = 0;
        this.dx = 0;
        this.dy = 0;
        this.scaleX = 1;
        this.scaleY = 1;
        this.rotateX = 0;
        this.rotateY = 0;
        this.rotateZ = 0;
        this.originX = 0;
        this.originY = 0;
        this.alpha = 1;
    }

    private calculateMargin() {
        if (this.calcObject.margin) {
            if (this.calcObject.margin.left) {
                this.left = CalculateObjectSize.calculate(this.calcObject.margin.left);
            }
            if (this.calcObject.margin.right) {
                this.left = this.globalWidth - CalculateObjectSize.calculate(this.calcObject.margin.right) - (parseInt(this.calcObject.size.width) || 0 );
            }
            if (this.calcObject.margin.top) {
                this.top = CalculateObjectSize.calculate(this.calcObject.margin.top);
            }
            if (this.calcObject.margin.bottom) {
                this.top = this.globalHeight - CalculateObjectSize.calculate(this.calcObject.margin.bottom) - (parseInt(this.calcObject.size.height) || 0 );
            }
        }

        this.left = this.left || 0;
        this.right = this.right || 0;
    }

    private calculateWidth() {
        var marginWidth:number;
        var selfWidth:number;
        //Расчет стрейтча  или по марджинам если ширина не определена
        if (this.calcObject.size.width === undefined || this.calcObject.horizontalAlign == "stretch") {
            this.width = this.globalWidth - (this.left || 0) - (this.right || 0);
        } else if (this.calcObject.size.width !== undefined) {
            this.width = CalculateObjectSize.calculate(this.calcObject.size.width);
        } else {
            //Расчет по марджинам если ширина определена (выбирается наименьшая)
            marginWidth = this.globalWidth - (this.left || 0) - (this.right || 0);
            selfWidth = CalculateObjectSize.calculate(this.calcObject.size.width);
            this.width = Math.min(marginWidth, selfWidth);
        }

        if (!this.calcObject.size.width) {
            this.calcObject.size.width = this.width + "px";
        }

        if (this.width === undefined) this.width = this.globalWidth;

        if (this.width < 0) this.width *= -1;
    }

    private calculateHeight() {
        var marginHeight:number;
        var selfHeight:number;
        //Расчет стрейтча  или по марджинам
        if (this.calcObject.size.height === undefined || this.calcObject.verticalAlign == "stretch") {
            this.height = this.globalHeight - (this.top || 0) - (this.bottom || 0);
        } else if (this.calcObject.size.height !== undefined) {
            this.height = CalculateObjectSize.calculate(this.calcObject.size.height);
        } else {
            //Расчет по марджинам если высота определена (выбирается наименьшая)
            marginHeight = this.globalHeight - (this.top || 0) - (this.bottom || 0);
            selfHeight = CalculateObjectSize.calculate(this.calcObject.size.height);
            this.height = Math.min(marginHeight, selfHeight);
        }

        if (!this.calcObject.size.height) {
            this.calcObject.size.height = this.height + "px";
        }

        if (this.height === undefined) this.height = this.globalHeight;

        if (this.height < 0) this.height *= -1;
    }

    private calculateHorizontalAlignment() {

        if (this.calcObject.horizontalAlign == "center") {
            this.dx = (this.globalWidth / 2 - (this.width / 2 || 0));
            return undefined;
        }
        if (this.calcObject.horizontalAlign == "right") {
            this.dx = this.globalWidth - (this.width || 0);
            return undefined;
        }

        this.dx = 0;
    }

    private calculateVerticalAlignment() {

        if (this.calcObject.verticalAlign == "center") {
            this.dy = (this.globalHeight / 2 - (this.height / 2 || 0));
            return undefined;
        }
        if (this.calcObject.verticalAlign == "bottom") {
            this.dy = this.globalHeight - (this.height || 0);
            return undefined;
        }
        this.dy = 0;
    }

    private static calculate(objVal:string):number {
        return parseInt(objVal);
    }
}

var calculator:CalculateObjectSizeI = new CalculateObjectSize();
export = calculator;