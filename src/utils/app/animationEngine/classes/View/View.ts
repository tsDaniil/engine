/// <reference path="../../../../../interface/ViewI.d.ts" />
/// <reference path="../../../../../plugins/Main/MainData.d.ts" />
/// <reference path="../../interfaces/SceneI.d.ts" />


import Module = require("../../../../base/Module");
import SceneElement = require("./ViewElement/SceneElement");
import PlatformManager = require("../../../PlatformManager");


class View extends Module implements ViewI {

    public node:$i;
    public mainElement:SceneElementI;
    public data:MainSceneI;

    constructor(node:$i, data:MainSceneI) {

        super();

        this.node = node;
        this.data = data;

        this.init();
    }

    public getSceneElement():SceneElementI {
        return this.mainElement;
    }

    public getNode():$i {
        return this.node;
    }

    public getRoot():ViewI {
        return this;
    }

    public render():void {
        this.mainElement.render();
    }

    public draw():void {
        this.mainElement.draw();
    }

    public remove():void {
        this.mainElement.remove();
    }

    public destroy():void {
        this.mainElement.destroy();
    }

    public getCalcData():any {
        return {
            "id": "root",
            "originSize": {
                "width": 1024,
                "height": 768
            },
            "size": {
                "width": this.view.width() + "px",
                "height": this.view.height() + "px"
            }
        }
    }

    private addCollection():void {

        this.mainElement = new SceneElement(this.data.mainElement, this);

    }

    private getLoadingCounts():number {

        var count = 0;

        var find = function (elements:Array<templateElementType>) {

            elements.forEach(function (element:templateElementType) {

                if (element.elementType === "image") {
                    count++;
                }

                if (element.content) {
                    find(element.content);
                }

            });

        };

        find(this.data.mainElement.content);

        return count;
    }

    private setLoadHandlers():void {

        var loadingElementsCount:number = this.getLoadingCounts();
        var that = this;

        var handler = function () {
            loadingElementsCount--;
            if (loadingElementsCount == 0) {
                that.off("View:loaded", handler);
                that.loaded();
                console.log("loaded!");
            }
        };

        if (!loadingElementsCount) {
            this.loaded();
            console.log("loaded!");
        } else {
            this.on("View:loaded", handler, this);
        }

    }

    private setPlatgormHandlers():void {

        this.on("View:platformControl:platform", function (method:string) {

            var Do = {
                "showSettings": function () {
                    PlatformManager.showSettings();
                },
                "showNavigation": function () {
                    PlatformManager.showNavigation();
                },
                "exit": function () {
                    PlatformManager.exit()
                }
            };

            if (method in Do) {
                Do[method]();
            } else {
                throw new Error("Неожиданный метод! " + method);
            }

        });

    }

    private init():void {

        this.setPlatgormHandlers();
        this.setLoadHandlers();
        this.addCollection();

    }
}
export = View;