/// <reference path="../../../../interface/tweenjs.d.ts" />
/// <reference path="./TimeLineI.d.ts" />
/// <reference path="./ViewElementI.d.ts" />
/// <reference path="./Calc.d.ts" />

interface AnimationAI {
    id:string;
    state:boolean;
    isPlay : boolean;
    isEnd : boolean;
    isStart : boolean;
    time:number;
    timingFunction:string;
    animationDirection: string;
    positions: PositionsI;
    element: AnimateObjectI;
    draw : ()=>any
}

interface animationSourceI{
    stageTime : number;
    params : any;
    timingFunction : string;
}

interface AnimationI {
    /**
     *    Проигрывание анимации для View элемент
     *    Возвращает id анимации
     */
    animateViewElement(viewElement:ViewElementI, data, callback?:()=>void):string;

    /**
     * Проигрывание анимации с id
     * @param id
     */
    playAnimation(id:string):void;

    /**
     * Добавление анимации для объекта
     * @param Object Анимируемый объект
     * @param animation Описание Анимаций
     */
    addAnimation(id: string, element: any, data: AnimationAI, render?:()=>any, callback?:()=>any):string;

    /**
     * Удаление анимации
     * @param id
     */
    removeAnimation(id: string):void

    /**
     * Простое проигрывание анимации
     * @param element
     * @param data
     * @param render
     */
     play(element: any, data: Array<animationSourceI>, render?:()=>any, calback?:()=>any ):void
}

interface AnimateObjectI {
    id: string;
}