

interface SceneElementI extends ViewElementI {

    hide(direction:string);
    show(direction:string);

}