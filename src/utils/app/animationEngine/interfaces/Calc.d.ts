/// <reference path="../../../../interface/templateElementType.d.ts" />

interface AnimatedObjectParamsI {
    x:number;
    y:number;
    width:number;
    height:number;
    alpha:number;
    scaleX:number;
    scaleY:number;
    rotateX:number;
    rotateY:number;
    rotateZ:number;
    originX:number;
    originY:number;
}

interface CalculateObjectSizeI {
    calculate(parentObject:templateElementType, calcObject:templateElementType):CalculateObjectSizeI;
    getAbsoluteTransformString():string;
    getDefaultTransformString():string;
    getPosition(): ReturnStyleI;
    getTransformPosition(): ReturnStyleI;
    getParams():AnimatedObjectParamsI;
    getAnimation():Array<CurrentedAnimationPositionsI>;
    getTextData():TemplateTextI;
    getNotCurrentSize():TemplateSizeI;
}

interface CurrentedAnimationPositionsI {

    /**
     * "linear" | "ease" | "bounceOut";
     */
    timingFunction?: string;

    /**
     * Имя анимации
     */
    name: string;

    /**
     *  "toggle" |  "normal"
     */
    animationDirection?: string

    time?:number;

    positions: Array<PositionsI>;

}

interface PositionsI {

    percent: number;
    width?: number;
    height: number;
    x?:number;
    y?:number;
    alpha?:number;
    rotateZ?: number;
    rotateX?: number;
    rotateY?: number;
    scaleX?:number;
    scaleY?: number;
    originX?:number;
    originY:number;

}

interface ReturnStyleI {
    width?:string;
    height?:string;
    top:string;
    left:string;
    position?:string;
    transform?:string;
    transformOrigin?: string;
}

