/// <reference path="./CollectionElementsI.d.ts" />
/// <reference path="./AnimatedObjectI.d.ts" />

interface ViewElementI extends CollectionElementsI {

    /**
     * Полчаем объект хранящий онформацию о положении элемента
     */
    getAnimatedObject():AnimatedObjectI;

    getNode():$i;

    /**
     * Перерисовываем элемент
     */
    render():void;
    /**
     * Отрисовываем элемент
     */
    draw():void;
    /**
     * Удаляем элемент из ДОМ
     */
    remove():void;
    /**
     * Уничтожаем объект и элемент
     */
    destroy():void;

    /**
     * Получаем строку для запуска события на рутовом объекте
     * @param interactive
     * @return {string}
     */
    getInteractiveTriggerStr(interactive:InteractiveI):string;

    /**
     * Получаем строку для слушанья события на текущем элементе
     * @param interactive
     */
    getStrForBindInteractive(interactive:InteractiveI):string;

    /**
     * Получаем строку для подписки на выполнение анимации на рутовом объекте
     * @param animation
     */
    getStrForBindAnimation(animation:AnimationTemplateI):string

}