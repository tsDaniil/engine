/// <reference path="../../../../interface/CollectionI.d.ts" />

interface SceneCollectionI extends CollectionI {

    sendToCollection(method:string, args?:Array<any>):void;
    next():void;
    prev():void;
    jumpToScene(index:number):void;
    initScenes():void;

}