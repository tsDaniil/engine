/// <reference path="../../../../interface/ModuleI.d.ts" />
/// <reference path="../../../../interface/templateElementType.d.ts" />

interface CollectionElementsI extends ModuleI {

    /**
     * Получаем родительский объект
     */
    getParent():CollectionElementsI;
    /**
     * Получаем корневой объект
     */
    getRoot():CollectionElementsI;
    /**
     * Получаем данные которые пришли для этого объекта
     */
    getCalcData():templateElementType;
    /**
     * Получаем дом элемент объекта
     */
    getNode():$i;

    /**
     * Перересовываем дочерние элементы
     */
    sendRender():void;
    /**
     * Отрисовываем дочерние элементы
     */
    sendDraw():void;
    /**
     * Удаляем дочерние элементы
     */
    sendRemove():void;
    /**
     * Уничтожаем дочерние элементы
     */
    sendDestroy():void;

    /**
     * Выполняем метод на дочерних элементах
     * @param method имя метода
     * @param args аргументы
     */
    sendToCollection(method:string, args?:Array<any>):void;

}