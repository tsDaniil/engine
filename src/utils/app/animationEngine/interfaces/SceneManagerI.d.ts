/// <reference path="./SceneCollectionI.d.ts" />
/// <reference path="../../../../plugins/Main/MainData.d.ts" />

interface SceneManagerI extends SceneCollectionI {

    createScenes(scenesData:MainDataI, node:$i, preLoadedScenes:number):void;
    getNode():$i;
    drawScene(sceneNumber?:number):void;
    episodeControlHandler(sceneId:string, method:string):void;

}