/// <reference path="../../../../interface/ModuleI.d.ts" />

interface SceneI extends ModuleI {

    preLoad():void;
    unLoad():void;
    draw():void;
    remove():void;

    hide(direction:string):void;
    show(direction:string):void;

    next():void;
    prev():void;

    jumpTo(index:number):void;

}