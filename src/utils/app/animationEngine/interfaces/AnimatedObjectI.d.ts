/// <reference path="../../../../interface/ModuleI.d.ts" />

interface AnimatedObjectI extends ModuleI {

    //-----------------------------------------------------------------
    //  Весь рендер на хтмл элементах идет через transform
    //-----------------------------------------------------------------

    //-----------------------------------------------------------------
    // START Для transform

    /**
     * Координата Х элемента относительно родителя
     */
    x:number;
    /**
     * Координата У элемента относительно родителя
     */
    y:number;
    /**
     * Масштабирование элемента по оси Х
     */
    scaleX:number;
    /**
     * Масштабирование элемента по оси У
     */
    scaleY:number;
    /**
     * Поворот Элемента по оси Х TODO
     */
    rotateX:number;
    /**
     * Поворот элемента по оси У TODO
     */
    rotateY:number;
    /**
     * Вращение элемента (2d)
     */
    rotateZ:number;

    // END Для transform
    //-----------------------------------------------------------------

    /**
     * Свойство width элемента
     */
    width:number;
    /**
     * Свойство height элемента
     */
    height:number;
    /**
     * Свойство opacity элемента
     */
    alpha:number;

    /**
     * Свойство transform-origin-x элемента
     */
    originX:number;
    /**
     * Свойство transform-origin-y элемента
     */
    originY:number;

    /**
     * Получаем объект стилей для отрисовки
     */
    draw():animatedParams;
}

interface animatedParams {
    transform?: string;
    width?: string;
    height?: string;
    transformOrigin?: string;
    opacity?: number;
}