/// <reference path="../../../../interface/ModuleI.d.ts" />

interface TimeLineI extends ModuleI {

    /**
     * Добавляем слушателя
     * @param object
     */
    addListener(object:ModuleI):void;
    /**
     * Убираем слушателя
     * @param object
     */
    removeListener(object:ModuleI):void;

}