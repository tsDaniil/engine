/// <reference path="../../../interface/UserDataI.d.ts" />
/// <reference path="../../../interface/DomLite.d.ts" />

import Base = require("../../base/Base");
import $ = require("$");

var platforms = {
    mobile: "mobile",
    desktop: "desktop",
    tablet: "tablet"
};

var userData:UserDataI;
class UserData extends Base implements UserDataI {

    /**
     * Объект с распарсенным урлом
     */
    private search:SearchI;
    private platform:string;

    constructor() {
        super();
        this.init();
    }

    /**
     * Инициализация
     */
    private init():void {
        this.parseSearch();
        this.initPlatform();
    }

    private initPlatform():void {
        var html = $("html");

        if (html.hasClass(platforms.desktop)) {
            this.platform = platforms.desktop;
        } else if (html.hasClass(platforms.mobile)) {
            this.platform = platforms.mobile;
        } else {
            this.platform = platforms.tablet;
        }
    }

    /**
     * Парсим строку запроса
     */
    private parseSearch():void {

        var searchStr = window.location.search,
            search = {},
            searchArr;
        if (!searchStr) {
            this.search = search;
        } else {

            searchStr = searchStr.replace("?", "");
            searchArr = searchStr.split("&");

            searchArr.forEach(function (searchIem) {

                if (searchIem.indexOf("=") != -1) {

                    var searchArr = searchIem.split("=");

                    Object.defineProperty(search, searchArr[0], {
                        value: searchArr[1],
                        writable: false,    // присвоение вызовет ошибку
                        enumerable: true,   // свойство будет в `for(key in user)`
                        configurable: false // удаление "delete user.name" вызовет ошибку
                    });

                } else {

                    Object.defineProperty(search, searchIem, {
                        value: undefined,
                        writable: false,    // присвоение вызовет ошибку
                        enumerable: true,   // свойство будет в `for(key in user)`
                        configurable: false // удаление "delete user.name" вызовет ошибку
                    });

                }

            });

            this.search = search;
        }
    }

    /**
     * Выдаем объект запроса
     * @return {SearchI}
     */
    public getSearch():SearchI {
        return this.search;
    }

    /**
     * Проверяем десктоп ли это
     * @return {boolean}
     */
    public isDesktop():boolean {
        return (this.platform == platforms.desktop);
    }

    public isMobile():boolean {
        return (this.platform == platforms.mobile);
    }

    public isTablet():boolean {
        return (this.platform == platforms.tablet);
    }

    /**
     * Проверяем андройди это
     * @return {boolean}
     */
    public isAndroid():boolean {
        return (navigator.userAgent.toLowerCase().indexOf('android') != -1);
    }

    /**
     * Проверяем интернет эксплорер ли это
     * @return {boolean}
     */
    public isWindows():boolean {
        return (navigator.userAgent.toLowerCase().indexOf('windows') != -1)
    }

    /**
     * Проверяем веб плеер ли это
     * @return {boolean}
     */
    public isWeb():boolean {
        return this.getSearch().model == "web";
    }

    /**
     * проверяем версию вебкита для андройд
     */
    public isModernWebkit():boolean {
        if(this.isAndroid()){
            return (parseInt( window.navigator.userAgent.replace(/[\s\S]*WebKit\//,"")) > 536);
        } else {
            return true;
        }

    }
}

userData = new UserData();
export = userData;