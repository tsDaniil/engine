/// <reference path="../../interface/Howler" />
/// <reference path="../../interface/require.d.ts" />
/// <reference path="../../interface/SoundManagerI" />

interface NativeI {
	exec (val:string, key?:any, key1?:any, key2?:any, key3?:any):void;
}


declare var AudioController;
declare var Native:NativeI;

import Module = require("../base/Module");

 
class SoundManagerNative extends Module implements SoundMangerI {

	private native:NativeI;
	public bgSound:string;

	constructor(){
		super();
		this.init();
	}


	public  init() {
		window["AudioController"] = window["$require"]("AudioController");
		this.native = Native;
	}

	public setVolumeForSoundTo (key:string, vol:number) {
		if (vol === undefined) vol = 1;
		Native.exec("AudioController.setVolumeForSound_to_", key, vol);
	}

	public addBg(key:string, fileName:string):void {
		Native.exec("AudioController.addSoundToList_forKey_ofType_", fileName, key, 2);
	}

	public fadeVolumeTo(key:string, vol:number, dur:number):void {
		if (key === undefined) return;
		if (dur === undefined) dur = 500;
		if (vol === undefined) return;
		Native.exec("AudioController.fadeVolume_dur_to_", key, 0.001 * dur, vol);
	}

	public playBg(key:string, dur?:number):void {
		if (dur == undefined) dur = 500;
		Native.exec("AudioController.fadeInAndPlay_dur_", key, 0.001 * dur);
	}

	public pauseBg(key:string, duration?:number):void {
		if (duration === undefined) {
			duration = 500;
		}
		Native.exec("AudioController.fadeOutAndAct_action_dur_", key, 1, duration * 0.001);
	}

	public stopBg(key:string, duration?:number):void {
		if (duration === undefined) {
			duration = 500;
		}
		Native.exec("AudioController.fadeOutAndAct_action_dur_", key, 2, duration * 0.001);
	}

	public addSfx(key:string, fileName:string):void {
		Native.exec("AudioController.addSoundToList_forKey_ofType_", fileName, key, 1);
	}

	public playSfx(key:string):void {
		Native.exec("AudioController.playSoundAsync_", key);
	}

	public unloadAllSfx():void {
		this.native.exec("AudioController.unloadAllSfx");
	}

	public pauseAllSounds():void {
		this.native.exec("AudioController.pauseAllSounds");
	}

	public resumeAllSounds():void {
		this.native.exec("AudioController.resumeAllSounds");
	}

	public setVolume(vol:number){
		/*
		Note use in Native App
		 */
	}
}

export = SoundManagerNative;
