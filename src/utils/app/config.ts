/// <reference path="../../interface/require.d.ts" />
/// <reference path="../../utils/init" />

requirejs.config({

    baseUrl: baseUrl,

    paths: {
        "$": "utils/utils/vendor/Lite",
        "mustache": "utils/utils/vendor/Mustache",

        "text": "utils/utils/vendor/text",
        "json": "utils/utils/vendor/json",

        "createjs": "utils/utils/vendor/tweenjs-NEXT.min",
        "promise": "utils/utils/vendor/promise-6.0.0.min",
        "$Module": "utils/base/Module"
    },

    shim: {
        createjs: {
            exports: "createjs"
        }
    },

    urlArgs: "engine=tsEngineType_verMajor_verMinor",
    waitSeconds: 25

});
console.log("config loaded!");

/**
 * Отлавливаем ошибки
 * @param errorMsg
 * @param url
 * @param lineNumber
 */
window.onerror = function (errorMsg, url, lineNumber) {
    console.error('------------------\nError: ' + errorMsg + '\nScript: ' + url + '\nLine: ' + lineNumber + "\n------------------");
};

requirejs(["utils/app/Loader", "utils/utils/vendor/underscore", "utils/utils/vendor/platform"], function (Loader) {
	console.log("Loader loaded");
	new Loader();
}, function () {
	console.log("Network error", arguments);
    debugger;
});