/// <reference path="../../interface/PlatformManagerI.d.ts"/>
/// <reference path="../../interface/defaultDataI.d.ts" />
/// <reference path="../../interface/DomLite.d.ts" />
/// <reference path="../../plugins/PluginI.d.ts" />

declare var data:defaultDataI;
declare var Native:any;
declare var Easing:any;
declare var ViewCreator:any;
declare var Navigation:any;
declare var $bigrect:$i;

import Base = require("../base/Base");
import UserData = require("./user/UserData");
import Debug = require("../debug/Debug");
import Downloader = require("../utils/Downloader");
import PlatformManager = require("./PlatformManager");
import $ = require("$");
import OrientationManager = require("../utils/OrientationManager");

class Loader extends Base {

    constructor() {
        super();
        this.init();
    }

    /**
     * Инициализация
     */
    private init():void {

        Downloader.loadEngineStyle("engine.css", function () {

            Loader.loadBridge(function () {

                PlatformManager.init();

                Loader.loadData(function () {

                    Loader.loadMainModule(function (Module) {

                        Loader.fixScreenSize();

                        if (UserData.getSearch()["debug"]) {
                            setTimeout(function () {
                                new Debug();
                            }, 1700);
                        }

                        var module:PluginI = new Module();

                        module.onEvent("ready", function () {
                            PlatformManager.startBook(parseInt(UserData.getSearch().page_index), module.getPageCount());
                        });

                        module.run(data);
                        window["module"] = module;

                        PlatformManager.on("PlatformManager:setViewportSize", function (width, height) {
                            setTimeout(function () {
                                var collection = $.createCollection([$("html"), $("body"), $bigrect]);
                                collection.css({
                                    width: width + "px",
                                    height: height + "px"
                                });
                                module.onResize(width, height);
                            }, 500);
                        });
                    });
                });
            });
        });
    }

    /**
     * Устанавливаем размеры вьюхи
     */
    private static fixScreenSize():void {

        var size = Loader.getSize();
        var collection = $.createCollection([$("html"), $("body"), $bigrect]);
        collection.css(size);

        OrientationManager.on(OrientationManager.eventName, function () {
            var size = Loader.getSize();
            collection.css(size);
        });

    }

    /**
     * Полчаем размеры для вьюхи
     * @return {{width: string, height: string}}
     */
    private static getSize() {

        if (UserData.isDesktop()) {
            var search = UserData.getSearch();
            if (search.webview_height && search.webview_width) {
                return {
                    "width": search.webview_width + "px",
                    "height": search.webview_height + "px"
                }
            }
        } else {
            return {
                "width": window.innerWidth + "px",
                "height": window.innerHeight + "px"
            }
        }

    }

    /**
     * Скачиваем данные
     * @param callback
     */
    private static loadData(callback):void {
        var path:string;
        if (!UserData.getSearch().lang) {
            path = "tsEngineData";
        } else {
            path = UserData.getSearch().lang + "/tsEngineData";
        }

        requirejs([path + ".js"], callback);

    }

    /**
     * Скачиваем главный модуль плагина
     * @param callback
     */
    private static loadMainModule(callback):void {
        require(["plugins/" + data.module + "/" + data.module], callback);
    }

    /**
     * Скачиваем бридж
     * @param callback
     */
    private static loadBridge(callback):void {
        require([
            "../utils/bridje/modules",
            "../utils/bridje/jsocb",
            "../utils/bridje/audioController",
            "../utils/bridje/Native-ios-chained",
            "../utils/bridje/navigation",
            "../utils/bridje/videoController",
            "../utils/bridje/viewCreator",
            "../utils/bridje/Easing"
        ], callback, function () {
            alert("Не удалось скачать бридж!");
            callback();
        });
    }

}

export = Loader;