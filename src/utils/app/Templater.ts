/// <reference path="../../interface/TemplaterI.d.ts" />
/// <reference path="../../interface/require.d.ts" />

import Module = require("../base/Module");

class Templater extends Module implements TemplaterI {

    public getTemplate(name:string, data:Object):string {
        if (!window["JST"]) {
            console.error("Темплейт ещё не загружен!");
        }
        if (!window["JST"][name]) {
            console.error("Такого темплейта не существует!");
        }
        return window["JST"][name](data);
    }

    public loadTemplate(templateURl:string, callback?:()=>void):void {
        var that:Templater = this;
        require([templateURl], function () {
            that.fireEvent(templateURl + ":loaded");
            if (callback) {
                callback();
            }
        }, function () {

            if (window["engineAdditionalURL"]) {
                templateURl = window["engineAdditionalURL"] + templateURl + ".js";
                require([templateURl], function () {
                    that.fireEvent(templateURl + ":loaded");
                    if (callback) {
                        callback();
                    }
                });
            } else {
                console.error("Не удалось скачать файл " + templateURl)
            }

        });
    }

}
var templater:TemplaterI = new Templater();
export = templater;