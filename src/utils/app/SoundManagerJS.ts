/// <reference path="../../interface/Howler" />
/// <reference path="../../interface/require.d.ts" />
/// <reference path="../../interface/SoundManagerI" />

import Module = require("../base/Module");
import UserData = require("./user/UserData");

class SoundManagerJS extends Module implements SoundMangerI {

	private sounds: Object = {};
	private moduleIsReady:boolean = false;
	private pausedSounds;

	public bgSound:string;
	private masterVolume:number = 1;


	constructor() {
		super();
		require(["utils/utils/vendor/howler.min"], this._isLoad.bind(this));
	}

	private _isLoad(Howler:HowlerGlobal):void {
		this.moduleIsReady = true;
	}


	public init():void {

	}

	public addBg(key:string, fileName:string):void {
		this.addFileToList(key, fileName, 0);
	}

	public fadeVolumeTo(key:string, vol:number, dur:number):void {
		this.sounds[key].fadeIn(vol,dur);
		this.sounds[key].myVolume = vol;
	}

	public playBg(key, dur?:number):void {
		console.log(key, this.bgSound);
		if (key && key != this.bgSound) {
			if (!!(this.bgSound && this.sounds[this.bgSound])) {
				this.sounds[this.bgSound].fadeOut(0, 1000, function () {
					this.sounds[this.bgSound].pause().volume(0).loop(true).fadeIn(this.sounds[this.bgSound].myVolume, 1000);
				}.bind(this));
			} else {
				this.sounds[key].pause().volume(0).loop(true).fadeIn(this.sounds[key].myVolume, 1000);
			}
			this.bgSound = key;
		}
	}

	public pauseBg(key, dur?):void {
		this.sounds[this.bgSound].pause();
	}

	public stopBg(key, du?):void {
		this.sounds[this.bgSound].stop();
	}

	public addSfx(key, fileName):void {
		this.addFileToList(key, fileName, 1);
	}

	public playSfx(key):void {
		this.sounds[key].play();
	}

	public unloadAllSfx():void {
	}

	public pauseAllSounds():void {
		for(var i in this.sounds){
			if(!this.sounds.hasOwnProperty(i) || i == this.bgSound) continue;
			this.sounds[i].pause();
		}
	}

	public resumeAllSounds():void {
		for(var i in this.sounds){
			if(!this.sounds.hasOwnProperty(i) || i == this.bgSound) continue;
			this.sounds[i].stop().play();
			this.sounds[i] = undefined;
		}
	}

	public setVolumeForSoundTo (key:string, vol:number) {
		if (vol === undefined) vol = 1;
		this.sounds[key].volume(vol);
		this.sounds[key].myVolume = vol;
	}

	public setVolume(volume:number){
		Howler.volume(volume);
	}

	private addFileToList(key:string, src:string, typeSound:number):void {

		if(key && !this.sounds[key]) {
			this.sounds[key] = new Howl({
				urls: [src, src.replace("mp3", "ogg")],
				loop : !typeSound,
				volume : 1
			});
		} else {
			var a:Howl = this.sounds[key];
			a.urls([src, src.replace("mp3", "ogg")]);
		}
	}



}

export = SoundManagerJS;