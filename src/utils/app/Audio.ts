/**
 * Created by usercom on 13.11.14.
 */

/// <reference path="../../interface/Howler" />
/// <reference path="../../interface/require.d.ts" />
/// <reference path="../../interface/SoundManagerI" />

import SoundManagerJS = require("./SoundManagerJS");
import SoundManagerNative = require("./SoundManagerNative");
import UserData = require("./user/UserData");

var soundclass :  SoundMangerI;

soundclass = new SoundManagerJS();

//if(!UserData.isDesktop() && UserData.getSearch().win8 != "1"){
//	soundclass = new SoundManagerNative();
//} else {
//	soundclass = new SoundManagerJS();
//}

export = soundclass;
