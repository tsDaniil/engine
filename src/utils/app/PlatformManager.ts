/// <reference path="../../interface/PlatformManagerI.d.ts" />
/// <reference path="../../interface/require.d.ts" />


interface NativeI {

}

interface NavigationI {
    onBookLoadedWithSection(callback:(pageInfo:EpisodeI)=>void);
    onSectionChanged(callback:(param:any)=>void);
    engineIsReady():void;
    setSectionNumber(activePage:number, scenesCount:number);
    showSettings():void;
    showNavigation():void;
    exit():void;
    setStoredValueForKey(value:string, key:string):void;
    getStoredValueForKey(key:string):string;
    onStoredValue(callback:(data:string) => void):void;
    sectionFinished(n:number):void;
    setMasterVolume? :(volume:number)=>void;
    setCallbackOnChangeViewPortSize(callback:(width:number, height:number)=>void):void;
    command(key:string):void;
}

declare var Navigation:NavigationI;
declare var Native:NativeI;

import Module = require("../base/Module");
import UserData = require("./user/UserData");
import LocalStorageManager = require("./LocalStorageManager");
import Audio = require( "./Audio" );

var platformManager:PlatformManagerI;
class PlatformManager extends Module implements PlatformManagerI {

    private native:NativeI;
    private navigation:NavigationI;

    private bookData:EpisodeI;

    public init() {

        var that = this;

        window["Navigation"] = window["$require"]("Navigation");

        if (!UserData.isDesktop() || UserData.getSearch().win8 == "1") {
            this.navigation = Navigation;
            this.native = Native;
        } else {
            require(["./navigation"], function (Navigation:any) {
                that.once("PlatformManager:bookDataLoaded", function () {
                    if (window["engineAPI"]["setSettingsHandler"]) {
                        window["engineAPI"]["setSettingsHandler"]();
                    }
                });
                that.navigation.command = that.onCommand;
                that.navigation = Navigation.createNavigation();
                that.navigation.setMasterVolume = Audio.setVolume;
                that.navigation.setCallbackOnChangeViewPortSize(function (width:number, height:number) {
                    that.trigger("PlatformManager:setViewportSize", [width, height]);
                });
            });
        }
    }

    public onCommand(key:string){
        switch (key){
            case "play":
                window["playInteractive"]("View:videoControl:video:playToggle");
                break;
            case "showLeft":
                window["playInteractive"]("View:animation:ingredients-list:toggle-ingredients");
                break;
            case "showRight":
                window["playInteractive"]("View:animation:recipe-list:toggle-recipe");
                break;
            case "showSettings":
                window["playInteractive"]("View:platformControl:platform:showSettings");
                break;
            case "showNavigation":
                window["playInteractive"]("View:platformControl:platform:showNavigation");
                break;
            case "nextScene":
                window["playInteractive"]("View:episodeControl:scene:next");
                break;
            case "lastScene":
                window["playInteractive"]("View:episodeControl:scene:prev");
                break;
        }
    }


    public startBook(active?:number, count?:number):void {

        var that = this;

        this.navigation.onBookLoadedWithSection(function (episodeData:EpisodeI) {
            that.bookData = episodeData;
            LocalStorageManager.setPrefix(episodeData.bookName + ":" + episodeData.issueName + ":" + episodeData.issueNumber);
            that.fireEvent("bookDataLoaded");
            that.trigger("PlatformManager:bookDataLoaded", [episodeData]);
        });

        this.navigation.onSectionChanged(function (pageNumber:number) {
            console.log(pageNumber);
        });

        this.onEvent("bookDataLoaded", function () {
            that.navigation.setSectionNumber((active || 0), (count || 1));
        });

        this.navigation.engineIsReady();

    }

    public onSectionChanged(callback:(pageNumber:number)=>void):void {
        this.navigation.onSectionChanged(callback);
    }

    public setSectionNumber(pageNumber:number, pagesCount:number):void {
        this.navigation.setSectionNumber(pageNumber, pagesCount);
    }

    public sectionFinished(pageNumber:number):void {
        this.navigation.sectionFinished(pageNumber);
    }

    public getBookData():EpisodeI {
        return this.bookData;
    }

    public exit():void {
        this.navigation.exit();
    }

    public showNavigation():void {
        this.navigation.showNavigation();
    }

    public showSettings():void {
        this.navigation.showSettings();
    }

    public setStoredValueForKey(name:string, value:string) {
        this.navigation.setStoredValueForKey(value, name);
    }

    public getStoredValueForKey(key:string) {
        return this.navigation.getStoredValueForKey(key);
    }

    public onStoredValue(callback:(data:string)=>void):void {
        this.navigation.onStoredValue(callback);
    }

}
platformManager = new PlatformManager();
export = platformManager;