/// <reference path="../../interface/LocalStorageManagerI.d.ts" />

import PlatformManager = require("./PlatformManager");
import Module = require("../base/Module");

var localStorageManager:LocalStorageManagerI;
class LocalStorageManager extends Module implements LocalStorageManagerI {

    private prefix:string;

    public setPrefix(ptefix:string):void {
        this.prefix = ptefix + ":";
        this.loaded();
    }

    public setItem(name:string, value:string):void {
        if (window["localStorage"]) {
            localStorage.setItem(this.getName(name), value);
        } else {
            console.warn("Нет локального хранилища!");
            PlatformManager.setStoredValueForKey(value, name);
        }
    }

    public getItem(name:string, callback:(data:string) => void, timeout?:number):void {
        var timer;
        if (window["localStorage"]) {
            callback(localStorage.getItem(this.getName(name)));
        } else {
            console.warn("Нет локального хранилища!");
            timer = setTimeout(function () {
                console.warn("Не удалось получить ответ!");
                callback(null);
            }, timeout || 1500);
            PlatformManager.onStoredValue(function (data:string) {
                clearTimeout(timer);
                callback(data);
            });
            PlatformManager.getStoredValueForKey(name);
        }
    }

    public setObject(name:string, value:any):void {
        this.setItem(name, JSON.stringify(value));
    }

    public getObject(name:string, callback:(data:any)=>void, timeout?:number):void {
        this.getItem(name, function (data:string) {
            var result;
            try {
                result = JSON.parse(data);
            } catch (e) {
                console.error("Не могу распарсить JSON! " + data);
                console.error(e);
                result = null;
            }
            callback(result);
        }, timeout);
    }

    private getName(name:string):string {
        return this.prefix + name;
    }

}
localStorageManager = new LocalStorageManager();
export = localStorageManager;