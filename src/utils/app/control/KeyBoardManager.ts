/// <reference path="../../../interface/KeyBoardManager" />


/**
 * Синголтон обработчика клавиатуры
 */

import Module = require("../../base/Module");

class KeyBoardManager extends Module implements KeyBoardManagerI {

	private _keys : EventObjectI;

	private keyKodes:Object = {
		8: "backspace",
		9: "tab",
		13: "enter",
		16: "shift",
		17: "ctrl",
		18: "alt",
		19: "pause",
		20: "capsLock",
		27: "escape",
		32: "space",
		33: "pageUp",
		34: "pageDown",
		35: "end",
		36: "home",
		37: "leftArrow",
		38: "upArrow",
		39: "rightArrow",
		40: "downArrow",
		45: "insert",
		46: "deleteKey"
	};

	public get Keys():EventObjectI {
		return this._keys;
	}

	constructor() {
		super();
		document.addEventListener("keypress", this.keyEventHandler.bind(this));

		var _keys : any = {};

		for(var key in this.keyKodes) {
			if(this.keyKodes.hasOwnProperty(key)) {
				_keys[this.keyKodes[key]] = "KeyBoardManager:" + this.keyKodes[key];
			}
		}

		this._keys = _keys;
	}

	private keyEventHandler(keyEvent: KeyboardEvent){
		var keyName;

		if(keyEvent.keyCode in this.keyKodes) {
			keyName = this.keyKodes[keyEvent.keyCode];
			this.trigger("KeyBoardManager:" + keyName);
		}
	}
}

var manager : KeyBoardManagerI = new KeyBoardManager();
export = manager;