define([], function () {

    if (!window["engineSettings"]) {
        window["engineSettings"] = {};
    }

    /**
     * @class Navigation
     * @param {Engine} delegate
     * @constructor
     */
    function Navigation(delegate) {
        this.delegate = delegate;

        if (window.engineAPI === undefined) {
            window.engineAPI = {
                setVolume: undefined,
                goToScene: undefined,
                pause: undefined,
                resume: undefined,
                playForward: undefined,
                setViewportSize: undefined,
                setSectionNumberHandler: undefined,
                setProgressHandler: undefined,
                setSceneProgressHandler: undefined,
                setDownloadProgressHandler: undefined,
                sectionFinishedHandler: undefined,
                showSettingsHandler: undefined,
                showNavigationHandler: undefined,
                setSettingsHandler: undefined,
                engineIsReadyHandler: undefined,
                resourceLoadErrorHandler: undefined,
				command : undefined
            };
        }

        var that = this;
		window.engineAPI.command = function(key){
			that.command(key);
		};
        window.engineAPI.setVolume = function (vol) {
            that.setMasterVolume(vol);
        };
        window.engineAPI.goToScene = function (n) {
            that.sectionChange(n);
        };
        window.engineAPI.playForward = function () {
            that.delegate.fireEvent("externalPlayForward");
        };
        window.engineAPI.setViewportSize = function (width, height) {
            if (that.onViewPortSizeChange) {
                that.onViewPortSizeChange(width, height);
            }
            console.log("New size! Width: " + width + "px, Height: " + height + "px");
        };
    }

    Navigation.prototype.setSectionNumber = function (a, b) {
        if (window.engineAPI.setSectionNumberHandler) {
            window.engineAPI.setSectionNumberHandler.call(window, a, b);
        }
    };

	Navigation.prototype.command = function(key){
		playInteractive(key);
	};

    Navigation.prototype.setCallbackOnChangeViewPortSize = function (callback) {
        this.onViewPortSizeChange = callback;
    };

    Navigation.prototype.setProgress = function (progress) {
        if (window.engineAPI.setProgressHandler) {
            window.engineAPI.setProgressHandler.call(window, progress);
        }
    };

    Navigation.prototype.setSceneProgress = function (progress) {
        if (window.engineAPI.setSceneProgressHandler) {
            window.engineAPI.setSceneProgressHandler.call(window, progress);
        }
    };

    Navigation.prototype.setDownloadProgress = function (downloadProgress) {
        if (window.engineAPI.setDownloadProgressHandler) {
            window.engineAPI.setDownloadProgressHandler.call(window, downloadProgress);
        }
    };

    Navigation.prototype.setDownloadProgress = function (progress) {

    };

    Navigation.prototype.sectionFinished = function (a) {
        if (window.engineAPI.sectionFinishedHandler) {
            window.engineAPI.sectionFinishedHandler.call(window, a);
        }
    };

    Navigation.prototype.setSettings = function (settings) {
        if (window.engineAPI.setSettingsHandler) {
            window.engineAPI.setSettingsHandler.call(window, settings);
        }
    };

    Navigation.prototype.resourceLoadError = function (error) {
        if (window.engineAPI.resourceLoadErrorHandler) {
            window.engineAPI.resourceLoadErrorHandler.call(window, error);
        }
    };

    Navigation.prototype.engineIsReady = function () {
        var bookName, issueName, episodeWord, issueNumber, sectionNumber;
        bookName = window.engineSettings.bookName ? window.engineSettings.bookName : "";
        issueName = window.engineSettings.issueName ? window.engineSettings.issueName : "";
        episodeWord = window.engineSettings.episodeWord ? window.engineSettings.episodeWord : "";
        issueNumber = window.engineSettings.issueNumber ? window.engineSettings.issueNumber : "";
        sectionNumber = window.engineSettings.sectionNumber ? window.engineSettings.sectionNumber : 0;
        this.bookLoad({
            bookName: bookName,
            issueName: issueName,
            episodeWord: episodeWord,
            issueNumber: issueNumber,
            sectionNumber: sectionNumber
        });
        if (window.engineAPI.engineIsReadyHandler) {
            setTimeout(function () {
                window.engineAPI.engineIsReadyHandler.call(window);
            }, 1000);
        }
    };

    Navigation.prototype.onSectionChanged = function (f) {
        this.sectionChange = f;
    };


    Navigation.prototype.onBookLoadedWithSection = function (f) {
        this.bookLoad = f;
    };

    Navigation.prototype.onPageBecameKey = function (f) {

    };

    Navigation.prototype.onPageNoLongerKey = function (f) {

    };

    Navigation.prototype.showNavigation = function () {
        if (window.engineAPI.showNavigationHandler) {
            window.engineAPI.showNavigationHandler.call();
        } else {
            console.log("showNavigation");
        }
    };

    Navigation.prototype.exit = function () {
    };

    Navigation.prototype.showSettings = function () {
        if (window.engineAPI.showSettingsHandler) {
            window.engineAPI.showSettingsHandler.call();
        } else {
            console.log("showSettings");
        }
    };

    Navigation.prototype.setStoredValueForKey = function (v, k) {

    };

    Navigation.prototype.getStoredValueForKey = function (v, k) {

    };

    Navigation.prototype.askForAutoPlay = function () {

    };

    Navigation.prototype.anyAction = function (action, additionalParameters) {
        // тут надо будет что-то во внешнем апи вызывать
    };

    Navigation.prototype.log = function (s) {

    };

    Navigation.createNavigation = function () {
        return new Navigation();
    };


    return Navigation;
});
