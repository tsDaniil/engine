/// <reference path="../../../../interface/DomLite.d.ts" />
/// <reference path="../../../../interface/ModuleI.d.ts" />


interface mediaOptionsI {

    /**
     * класс для html элемента
     */
    className?: string;

    /**
     * video/audio
     */
    mediaType: string;

    /**
     *ссылка на файл воспроизведения
     */
    src? : Array<any>;

    /**
     * показывать стандартный контрол
     */
    controls?: boolean;

    /**
     * Ширина
     */
    width?:number;

    /**
     * Высота
     */
    height?:number;

    /**
     * добавить себя в родительский DomLight элемент
     */
    parent?:$i;

    /**
     * предзагрузка
     * нет  | метаданные | авто
     * ------------------------
     * none | metadata   | auto
     */
    preload?:string;

    /**
     * Громкость воспроизведения
     * 0.0 - 1.0
     */
    volume?:number;

    /**
     * Audio mute
     */
    muted?:boolean;

    /**
     * вместо url строка вида
     * '<source src="video.m4v" type="video/mp4" /> <source src="video.ogg" type="video/ogg" />'
     */
    source?:string;

    /**
     * Зацикливание проигрывания
     *  true/false
     */
    loop?:boolean;

    /**
     * Автоматически изменять качество при возможности
     */
    autoQuality?:boolean;


}

interface MediaObjectI extends ModuleI {

    /**
     * Добавить Виде/аудио элемент в дом
     * @param element
     */
    appendTo(element:$i):void;

    /**
     * Создание элемента медиа контента по типу
     * @param elementType
     */
    createMediaElement(elementType: mediaOptionsI):void;

    /**
     * ставим на паузу
     */
    pause():void;

    /**
     * проигрывание
     */
    play():void;

    stop():void;

    /**
     * проигрывание/пауза если пауза/проигрывание
     */
    playToggle():void;

    /**
     * удаление элемента из DOM
     */
    remove():void;

    /**
     * удаление элемента
     */
    destroy():void;

    /**
     * поверяем проигрывание
     */
    isPlay():boolean;

    /**
     * проверяем паузу
     */
    isPaused():boolean;

    /**
     * Готовность к проигрыванию
     */
    isReadyToPlay():boolean;

    /**
     * Время с начала проигрывания в процентах
     */
    getPlayPosition():number;
    setPlayPosition(procent:number):void;

    /**
     * Время с начала проигрывания в секундах
     */
    getCurentTime():number;
    setCurentTime(time: number):void;

    /**
     * Всего время в секундах
     */
    getDuration():number;


    /**
     * Сам элемент
     */
    getMediaElement():HTMLVideoElement;

    /**
     * Получить буфферизированные отрезки  времени
     */
    getDownloadPositions():TimeRanges;

    /**
     * Ширина
     */
    getWidth():number;
    setWidth(width:number):void;

    /**
     * Высота
     */
    getHeight():number;
    setHeight(height:number):void;

    /**
     * Размер
     * @param width
     * @param height
     */
    setSize(width:number, height:number):void;

    /**
     * Манагер событий видеоплеера
     * @param e
     */
    eventStarter(e:Event):void;
}