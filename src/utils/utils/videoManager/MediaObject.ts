/// <reference path="./interface/MediaObject" />
/// <reference path="../../../interface/templateElementType.d.ts" />

import Module = require("../../base/Module");

class MediaObject extends Module implements MediaObjectI{

    public static allEvents = {
        canplay : "MediaObject:canplay",
        canplaythrough : "MediaObject:canplaythrough",
        durationchange : "MediaObject:durationchange",
        ended : "MediaObject:ended",
        error : "MediaObject:error",
        pause : "MediaObject:pause",
        play : "MediaObject:play",
        playing : "MediaObject:playing",
        seeking : "MediaObject:seeking",
        seeked : "MediaObject:seeked",
        loadeddata : "MediaObject:loadeddata",
        loadstart : "MediaObject:loadstart",
        progress : "MediaObject:progress",
        stalled : "MediaObject:stalled",
        suspend : "MediaObject:suspend",
        timeupdate : "MediaObject:timeupdate",
        volumechange : "MediaObject:volumechange",
        all : "MediaObject"
    };

    public static mediaTypes = {audio:"audio", video: "video"};

    private created: boolean = false;
    private parent:$i;
    private autoQuality: boolean = false;
    private media: HTMLVideoElement;
    private width:number;
    private height:number;


    private createSource(urls:Array<VideoSorcesI>):string{
        var result = "";
        urls.forEach(function(el){
            result += '<source src="' + el.url + '" type="' + (el.type || "") + '" \/>';
        });
        return result;
    }

    public appendTo(element:$i):void{
        if(this.created) {
            this.parent = element;
            element.append(this.media);
        }
    }

    public createMediaElement(options:mediaOptionsI):void{
        if(this.created && this.media.parentElement)  this.media.parentElement.removeChild(this.media);

        this.created = true;

        this.media = <HTMLVideoElement>document.createElement(options.mediaType);
        this.media.preload = "auto";
        this.media.controls = false;

        var innerHtml:string = "Your browser does not support " + options.mediaType + " element";

        for (var key in options) {
            if(!options.hasOwnProperty(key)) continue;
            switch (key){
                case "controls": this.media.controls = !!options.controls;
                    break;
                case "width" : this.media.width = options.width;
                    break;
                case "height" : this.media.height = options.height;
                    break;
                case "loop" : this.media.loop = !!options.loop;
                    break;
                case "muted" : this.media.muted = !!options.muted;
                    break;
                case "src" :
                    innerHtml = this.createSource(options.src) + innerHtml;
                    break;
                case "parent" :  this.appendTo(options.parent);
                    break;
                case "source" :  innerHtml = options.source + innerHtml;
                    break;
                case "preload" :  this.media.preload = options.preload;
                    break;
                case "class" : this.media.className = options.className;
                    break;
                case "autoQuality" : this.autoQuality = true;

            }
        }
        this.media.innerHTML = innerHtml;

        if("preload" in options && ((options.preload !== undefined) && options.preload)) this.media.load();

        this.addEventListener();

    }

    public pause():void{
        if(!this.media.paused) this.media.pause();
    }

    public play():void{
        if(this.media.paused) this.media.play();
    }

    public stop():void {
        this.pause();
        this.setPlayPosition(0);
    }

    public playToggle():void{

        if(this.media.paused){
            this.media.play();
        } else {
            this.media.pause();
        }
    }

    public remove():void{
        if(this.parent){
            this.pause();
            this.parent.node.removeChild(this.media);
            this.parent = undefined;
        }

    }

    public destroy():void{
        this.pause();
        this.media.src = undefined;
        this.remove();
        this.media = undefined;
        this.created = undefined;
    }

    public isPlay():boolean{
        return !this.media.paused;
    }

    public isPaused():boolean{
       return !!this.media.paused;
    }

    public isReadyToPlay():boolean{
        return this.media.readyState > 2;
    }

    public getPlayPosition():number{
        return this.media.duration/this.media.currentTime*100;
    }

    public setPlayPosition(procent:number):void{
        this.media.currentTime = procent/100 * this.media.duration;
    }

    public getCurentTime():number{
        return this.media.currentTime;
    }

    public setCurentTime(time: number):void{
        this.media.currentTime = time;
    }

    public getDuration():number{
        return this.media.duration;
    }

    public getMediaElement():HTMLVideoElement{
        return this.media;
    }

    public getDownloadPositions():TimeRanges {
       return this.media.buffered;
    }

    public getWidth():number{
        return this.media.width;
    }

    public setWidth(width:number):void{
        this.media.width = width;
    }

    public getHeight():number{
        return this.media.height;
    }

    public setHeight(height:number):void{
        this.media.height = height;
    }

    public setSize(width:number, height:number):void{
        this.setWidth(width);
        this.setHeight(height);
    }

    public eventStarter(e:Event):void{
        this.trigger("MediaObject:" + e.type, [this, e] );
    }

    private addEventListener():void{
        for(var prop in MediaObject.allEvents){
            this.media["on"+prop] = this.eventStarter.bind(this);
        }
    }

    private bufferAnalizer(playbackStartPoint:number, playbackEndPoint:number,  bufferStartPoint:number , bufferEndPoint:number, duration:number){
        //TODO Возможно надо будет реализовать

        //var oldQualityObj = this.qualityObj,
        //    playbackStart = 0, // new playback start point
        //    bufferStart = 0; // new buffer start point
        //
        //if (oldQualityObj) {
        //    playbackStart =  oldQualityObj.playbackEndPoint;
        //    bufferStart = oldQualityObj.bufferEndPoint;
        //} else {
        //    playbackStart = playbackStartPoint;
        //    bufferStart = bufferStartPoint;
        //}
        //
        //this.qualityObj = {
        //    'playbackEndPoint': playbackEndPoint,
        //    'bufferEndPoint': bufferEndPoint,
        //    'deltaBuffer': bufferEndPoint - bufferStart, // сколько забуферизовано
        //    'bufferSpeed':  (bufferEndPoint - bufferStart) /
        //    (playbackEndPoint - playbackStart),
        //    'deltaPlayback': playbackEndPoint - playbackStart, // сколько было
        //                                                       //воспроизведено
        //    'availTime': bufferEndPoint - playbackEndPoint // разница между буффером и
        //                                                   // позицией воспроизведения
        //}
        //
        //var restTime = duration - playbackEndPoint,
        //    bufferTime = (duration - bufferEndPoint) / this.qualityObj.bufferSpeed;
        //
        //if ((bufferTime > restTime) && ((this.qualityObj.availTime /
        //    this.qualityObj.deltaPlayback ) < 2)) {
        //    if (this.quality == 'normal') {
        //        this.quality = 'low';
        //    }
        //}
    }
}
export = MediaObject;
