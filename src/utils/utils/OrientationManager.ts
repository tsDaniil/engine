/// <reference path="../../interface/OrientationManagerI.d.ts" />

import Module = require("../base/Module");
import UserData = require("../app/user/UserData");

var orientationManager:OrientationManagerI;
class OrientationManager extends Module implements OrientationManagerI {

    private orientation:string;

    public eventName:string = "OrientationChange";
    private size:SizeI = OrientationManager.getSize();

    constructor() {
        super();
        this.setHandlers();
        this.initOrientation();
    }

    private setHandlers():void {
        var that = this;

        var checkOrientation = function() {

            var check = function () {

                if (that.isSizeChanged()) {
                    that.initOrientation();
                    that.trigger(that.eventName, [that.getOrientation()]);
                } else {
                    setTimeout(function () {
                        check();
                    }, 10);
                }

            };

            check();

        };

        window.addEventListener("orientationchange", checkOrientation, false);
        window.addEventListener("resize", checkOrientation, false);
    }

    public getOrientation():string {
        return this.orientation;
    }

    private isSizeChanged():boolean {
        var newSize:SizeI = OrientationManager.getSize();
        if ((this.size.width != newSize.width) || (this.size.height != newSize.height)) {
            this.size = newSize;
            return true;
        } else {
            return false
        }
    }

    private initOrientation():void {
        this.orientation = innerWidth < innerHeight ? "vertical" : "horizontal";
    }

    private static getSize():SizeI {
        return {
            width: innerWidth,
            height: innerHeight
        }
    }

}
orientationManager = new OrientationManager();
export = orientationManager;