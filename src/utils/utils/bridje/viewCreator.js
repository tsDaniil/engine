define(["./modules"], function () {
    defineModule("ViewCreator", function (require, exports) {
        var Native = $require("Native");

        var createViewWithImage = function (image, x, y, w, h) {
            if (x === undefined || y === undefined || w === undefined || h === undefined) return;
            if (image === undefined) return;
            Native.exec("ViewCreator.createViewWithImage_x_y_w_h_", image, x, y, w, h);
        };

        var createScrollViewWithImageForKey = function (image, key, rx, ry, rw, rh, ix, iy, iw, ih, paged) {
            if (key === undefined) key = image;
            if (rx === undefined || ry === undefined || rw === undefined || rh === undefined) return;
            if (ix === undefined || iy === undefined || iw === undefined || ih === undefined) return;
            if (image === undefined) return;
            if (paged === undefined) paged = false;
            Native.exec("ViewCreator.createScrollViewWithImage_forKey_rx_ry_rw_rh_ix_iy_iw_ih_paged_", image, key, rx, ry, rw, rh, ix, iy, iw, ih, paged);
        };

        var showScrollViewForKey = function (key) {
            Native.exec("ViewCreator.showScrollViewForKey_", key);
        };

        var createWebViewWithContents = function (contents, x, y, w, h, paged) {
            if (x === undefined || y === undefined || w === undefined || h === undefined) return;
            if (paged === undefined) paged = 0;
            if (contents === undefined) return;
            Native.exec("ViewCreator.createWebViewWithContent_x_y_w_h_paged_", contents, x, y, w, h, paged);
        };

        var createDraggableViewWithImage = function (imageName, key, rx, ry, rw, rh, ix, iy, iw, ih, angle) {
            if (imageName == undefined || key == undefined) return;
            if (rx == undefined || ry == undefined || rw == undefined || rh == undefined) return;
            if (ix == undefined || iy == undefined || iw == undefined || ih == undefined) return;
            Native.exec("ViewCreator.createDraggableViewWithImage_forKey_rx_ry_rw_rh_ix_iy_iw_ih_angle_", imageName, key, rx, ry, rw, rh, ix, iy, iw, ih, angle);
        };

        var createSpinner = function (x, y, w, h, a) {
            Native.exec("ViewCreator.createSpinnerForKey_x_y_w_h_a_", "spin", x, y, w, h, a);
        }

        var removeSpinner = function () {
            Native.exec("ViewCreator.removeSpinnerForKey_", "spin");
        }

        var removeDraggableViewForKey = function (key) {
            if (key == undefined) return;
            Native.exec("ViewCreator.removeDraggableViewForKey_", key);
        };

        var removeAllViews = function () {
            Native.exec("ViewCreator.removeAllViews");
        };

        var removeAllScrollViews = function () {
            Native.exec("ViewCreator.removeAllScrollViews");
        };

        var removeAllWebViews = function () {
            Naitve.exec("ViewCreator.removeAllWebViews");
        };

        exports.createViewWithImage = createViewWithImage;
        exports.createScrollViewWithImageForKey = createScrollViewWithImageForKey;
        exports.showScrollViewForKey = showScrollViewForKey;
        exports.createWebViewWithContents = createWebViewWithContents;
        exports.createDraggableViewWithImage = createDraggableViewWithImage;
        exports.createSpinner = createSpinner;
        exports.removeSpinner = removeSpinner;
        exports.removeDraggableViewForKey = removeDraggableViewForKey;
        exports.removeAllViews = removeAllViews;
        exports.removeAllScrollViews = removeAllScrollViews;
        exports.removeAllWebViews = removeAllWebViews;
    });
});