define(["./modules"], function () {
    defineModule("Native", function (require, exports) {
        var queue = {
            commands: [/*{arguments: ["Navigation.Something"], recurring: false}*/]
        };

        var lastCallBackId = 0;
        var callbacks = {};
        var iframe = null;
        var sendToPlatform;
        if (location.search.match(/win8/) != null) {
            sendToPlatform = function (args, parts) {
                var cmd_str = '{"command":"' + args[0] + '","args":[' + Array.prototype.map.call(parts, function (arg) {
                        return '{"part":"' + arg + '"}';
                    }).join(",") + ']}';
                window.external.notify(cmd_str);
            }
        }
        else {
            sendToPlatform = function (args, parts) {
                var url = "native://" + args[0] + "/?" + encodeURIComponent(JSON.stringify(parts));
                if (iframe === null) {
                    iframe = document.createElement("IFRAME");
                    iframe.style.position = "absolute";
                    iframe.style.visibility = "hidden";
                    iframe.style.top = "0";
                    iframe.style.left = "0";
                    document.documentElement.appendChild(iframe);
                }
                iframe.src = url;
            }
        }

        var addCallback = function (func, recurring) {
            if (recurring === undefined) {
                recurring = false;
            }
            callbacks[lastCallBackId] = {func: func, recurring: recurring};
            lastCallBackId++;
            return lastCallBackId - 1;
        };

        var run_command = function () {
            var command = queue.commands[0];
            if (command === undefined) {
                return;
            }

            var args = command.arguments;

            var parts = Array.prototype.slice.call(args, 1);
            var callback = -1;
            if (typeof(parts[parts.length - 1]) === "function") {
                callback = addCallback(parts[parts.length - 1], command.recurring);
                parts[parts.length - 1] = callback;
            } else {
                parts.push(callback);
            }

            sendToPlatform(args, parts);
        };

        var exec = function () {
            var start;
            start = queue.commands.length === 0;
            queue.commands.push({arguments: arguments, recurring: false});
            if (start === true) {
                run_command();
            }
        };

        var execWithRecurringCallback = function () {
            var start;
            start = queue.commands.length === 0;
            queue.commands.push({arguments: arguments, recurring: true});
            if (start === true) {
                run_command();
            }
        };

        exports.runNextCommand = function () {
            queue.commands.shift();
            run_command();
        };

        exports.addEventCallback = function (objectId, eventName, callback) {
            execWithRecurringCallback(objectId + ".addCallbackForEvent_", eventName, callback);
        };

        exports.removeAllCallBacksForEvent = function (objectId, eventName) {
            execWithRecurringCallback(objectId + ".removeAllCallBacksForEvent_", eventName);
        };

        exports.executeCallback = function (callbackId, res) {
            if (callbacks[callbackId] === undefined) {
                console.log("undefined callback");
                return
            }
            callbacks[callbackId].func(res);
            if (!callbacks[callbackId].recurring) {
                delete callbacks[callbackId];
            }
        };

        exports.exec = exec;
        exports.execWithRecurringCallback = execWithRecurringCallback;

        /*
         var log = console.log;
         // assume the presence of console and document.
         console.log = function (message) {
         exec("DebugConsole.log_atLevel_", message, "INFO");
         };
         console.debug = function (message) {
         exec("DebugConsole.log_atLevel_", message, "DEBUG");
         };
         console.warn = function (message) {
         exec("DebugConsole.log_atLevel_", message, "WARN");
         };
         console.error = function (message) {
         exec("DebugConsole.log_atLevel_", message, "ERROR");
         };
         */
    });
});