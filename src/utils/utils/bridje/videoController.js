define(["./modules"], function () {
    defineModule("VideoController", function (require, exports) {
        /*
         MCTransitionNone            = 0 << 20,
         MCTransitionFlipFromLeft    = 1 << 20,
         MCTransitionFlipFromRight   = 2 << 20,
         MCTransitionCurlUp          = 3 << 20,
         MCTransitionCurlDown        = 4 << 20,
         MCTransitionCrossDissolve   = 5 << 20,
         MCTransitionFlipFromTop     = 6 << 20,
         MCTransitionFlipFromBottom  = 7 << 20,

         // Custom transitions.
         MCTransitionSlideToTop      = 1 << 28,
         MCTransitionSlideToRight    = 2 << 28,
         MCTransitionSlideToBottom   = 3 << 28,
         MCTransitionSlideToLeft     = 4 << 28
         */
        var Native = $require("Native");

        var startNewScene = function (src) {
            Native.exec("VideoController.startNewScene_", src);
        };

        var setTransitionTypeAndDuration = function (num, dur) {
            if (num === undefined) {
                num = 0;
            }
            if (dur === undefined) {
                dur = 800;
            }
            Native.exec("VideoController.setTransitionType_andDuration_", num, 0.001 * dur);
        };

        var addPause = function (num) {
            Native.exec("VideoController.addPause_", num);
        };

        var addPauses = function (arr) {
            Native.exec("VideoController.addPauses_", arr);
        };

        var addTiming = function (num) {
            Native.exec("VideoController.addTiming_", num);
        };

        var addTimings = function (arr) {
            Native.exec("VideoController.addTimings_", arr);
        };

        var addLoop = function (begin, end) {
            Native.exec("VideoController.addLoop__", begin, end);
        };

        var addLoops = function (arr) {
            Native.exec("VideoController.addLoops_", arr);
        };

        var finalizeScene = function () {
            Native.exec("VideoController.finalizeScene");
        };

        var finalizeSceneDatas = function () {
            Native.exec("VideoController.allScenesSent");
        };

        var play = function () {
            Native.exec("VideoController.play");
        };

        var playForward = function () {
            Native.exec("VideoController.playForward");
        };

        var pause = function () {
            Native.exec("VideoController.pause");
        };

        var resume = function () {
            Native.exec("VideoController.resume");
        };

        var loadFirstScene = function () {
            Native.exec("VideoController.loadFirstScene");
        };

        var setNextScene = function (index) {
            if (index === undefined) {
                return;
            }
            Native.exec("VideoController.setNextScene_", index);
        };

        var jumpToSceneWithIndex = function (index) {
            Native.exec("VideoController.jumpToSceneWithIndex_", index);
        };

        var transitionToSceneWithIndexAndTypeAndDuration = function (index, type, duration) {
            if (index === undefined) {
                return;
            }
            if (((type === undefined) && (duration !== undefined)) || ((type !== undefined) && (duration === undefined))) {
                return;
            }
            Native.exec("VideoController.transitionToSceneWithIndex_andType_andDuration_", index, type, 0.001 * duration);
        };

        var transitionToNextSceneWithTypeAndDuration = function (type, duration) {
            if (((type === undefined) && (duration !== undefined)) || ((type !== undefined) && (duration === undefined))) {
                return;
            }
            Native.exec("VideoController.transitionToNextSceneWithType_andDuration_", type, 0.001 * duration);
        };

        var transitionToSceneWithoutVideoWithTypeAndDuration = function (type, duration) {
            Native.exec("VideoController.transitionToEmptySceneWithType_andDuration_", type, 0.001 * duration);
        };

        var transitionToNextSceneFromSceneWithoutVideoWithTypeAndDuration = function (type, duration) {
            transitionToNextSceneWithTypeAndDuration(type, duration);
        };

        var transitionFromSceneWithoutVideoWithTypeAndDuration = function (index, type, duration) {
            transitionToSceneWithIndexAndTypeAndDuration(index, type, duration);
        };

        var goToTime = function (time, play) {
            Native.exec("VideoController.goToTime_andPlay_", time, play);
        };

        var rewind = function () {
            Native.exec("VideoController.rewind");
        };

        var onSceneIsReady = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "sceneIsReady");
            Native.addEventCallback("VideoController", "sceneIsReady", f);
        };

        var onScenePaused = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "scenePaused");
            Native.addEventCallback("VideoController", "scenePaused", f);
        };

        var onSceneLoopPrepared = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "sceneLoopPrepared");
            Native.addEventCallback("VideoController", "sceneLoopPrepared", f);
        };

        var onSceneLooped = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "sceneLooped");
            Native.addEventCallback("VideoController", "sceneLooped", f);
        };

        var onSceneTiming = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "sceneTiming");
            Native.addEventCallback("VideoController", "sceneTiming", f);
        };

        var onSceneStartPlaying = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "sceneStartPlaying");
            Native.addEventCallback("VideoController", "sceneStartPlaying", f);
        };

        var onPlayForward = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "scenePlayForward");
            Native.addEventCallback("VideoController", "scenePlayForward", f);
        };

        var onSceneEndPlaying = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "sceneEndPlaying");
            Native.addEventCallback("VideoController", "sceneEndPlaying", f);
        };

        var onSceneEndPlayingWithPause = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "sceneEndPlayingWithPause");
            Native.addEventCallback("VideoController", "sceneEndPlayingWithPause", f);
        };

        var onTransitionEnded = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "transitionEnded");
            Native.addEventCallback("VideoController", "transitionEnded", f);
        };

        var onPlayerPause = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "playerPause");
            Native.addEventCallback("VideoController", "playerPause", f);
        };

        var onPlayerResume = function (f) {
            Native.removeAllCallBacksForEvent("VideoController", "playerResume");
            Native.addEventCallback("VideoController", "playerResume", f);
        };

        exports.startNewScene = startNewScene;
        exports.setTransitionType = setTransitionTypeAndDuration;
        exports.setTransitionTypeAndDuration = setTransitionTypeAndDuration;
        exports.addPause = addPause;
        exports.addPauses = addPauses;
        exports.addTiming = addTiming;
        exports.addTimings = addTimings;
        exports.addLoop = addLoop;
        exports.addLoops = addLoops;
        exports.finalizeScene = finalizeScene;
        exports.finalizeSceneDatas = finalizeSceneDatas;
        exports.play = play;
        exports.playForward = playForward;
        exports.pause = pause;
        exports.resume = resume;
        exports.loadFirstScene = loadFirstScene;
        exports.setNextScene = setNextScene;
        exports.jumpToSceneWithIndex = jumpToSceneWithIndex;
        exports.transitionToSceneWithIndexAndTypeAndDuration = transitionToSceneWithIndexAndTypeAndDuration;
        exports.transitionToNextSceneWithTypeAndDuration = transitionToNextSceneWithTypeAndDuration;
        exports.transitionToSceneWithoutVideoWithTypeAndDuration = transitionToSceneWithoutVideoWithTypeAndDuration;
        exports.transitionToNextSceneFromSceneWithoutVideoWithTypeAndDuration = transitionToNextSceneFromSceneWithoutVideoWithTypeAndDuration;
        exports.transitionFromSceneWithoutVideoWithTypeAndDuration = transitionFromSceneWithoutVideoWithTypeAndDuration;
        exports.goToTime = goToTime;
        exports.rewind = rewind;
        exports.onSceneIsReady = onSceneIsReady;
        exports.onScenePaused = onScenePaused;
        exports.onSceneLoopPrepared = onSceneLoopPrepared;
        exports.onSceneLooped = onSceneLooped;
        exports.onSceneTiming = onSceneTiming;
        exports.onSceneStartPlaying = onSceneStartPlaying;
        exports.onPlayForward = onPlayForward;
        exports.onSceneEndPlaying = onSceneEndPlaying;
        exports.onSceneEndPlayingWithPause = onSceneEndPlayingWithPause;
        exports.onTransitionEnded = onTransitionEnded;
        exports.onPlayerPause = onPlayerPause;
        exports.onPlayerResume = onPlayerResume;
    });
});