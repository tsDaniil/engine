define(["./modules"], function () {
    defineModule("AudioController", function (require, exports) {
        var Native = $require("Native");
        var addSoundToList = function (key, fileName, playerType) {
            if (playerType === undefined) playerType = 0;
            Native.exec("AudioController.addSoundToList_forKey_ofType_", fileName, key, playerType);
        };
        var removeSoundFromList = function (key) {
            Native.exec("AudioController.removeSoundFromList_", key);
        };
        var playSound = function (key, times) {
            if (times === undefined) times = 1;
            Native.exec("AudioController.playSound_times_", key, times);
        };
        var playSoundAsync = function (key) {
            Native.exec("AudioController.playSoundAsync_", key);
        };
        var playSoundTo = function (key, to) {
            if (key === undefined) return;
            if (to === undefined) to = 0;
            Native.exec("AudioController.playSound_to_", key, 0.001 * to);
        };
        var playSoundFromTo = function (key, from, to) {
            if (from === undefined) from = 0;
            if (to === undefined) to = 0;
            Native.exec("AudioController.playSound_from_to_", key, 0.001 * from, 0.001 * to);
        };
        var setVolumeForSoundTo = function (key, vol) {
            if (vol === undefined) vol = 1;
            Native.exec("AudioController.setVolumeForSound_to_", key, vol);
        };
        var playSoundFirstMs = function (key, dur) {
            if (dur === undefined) dur = 0;
            Native.exec("AudioController.playSound_from_to_", key, 0, 0.001 * dur);
        };
        var fadeInAndPlay = function (key, dur) {
            if (dur == undefined) dur = 500;
            Native.exec("AudioController.fadeInAndPlay_dur_", key, 0.001 * dur);
        };
        var pauseSound = function (key) {
            Native.exec("AudioController.pauseSound_", key);
        };
        var stopSound = function (key) {
            Native.exec("AudioController.stopSound_", key);
        };
        var fadeOutAndAct = function (key, action, duration) {
            if (duration === undefined) {
                duration = 500;
            }
            Native.exec("AudioController.fadeOutAndAct_action_dur_", key, action, duration * 0.001);
        };
        var fadeVolumeTo = function (key, vol, dur) {
            if (key === undefined) return;
            if (dur === undefined) dur = 500;
            if (vol === undefined) return;
            Native.exec("AudioController.fadeVolume_dur_to_", key, 0.001 * dur, vol);
        };
        var setNumberOfLoopsForSound = function (key, number) {
            if (key === undefined) {
                return;
            }
            if (number === undefined) {
                return;
            }
            Native.exec("AudioController.setNumberOfLoopsForSound_times_", key, number);
        };
        var stopAllSounds = function () {
            Native.exec("AudioController.stopAllSounds");
        };

        var addBg = function (key, fileName) {
            addSoundToList(key, fileName, 2);
        };
        var playBg = function (key, dur) {
            fadeInAndPlay(key, dur);
        };
        var pauseBg = function (key, dur) {
            fadeOutAndAct(key, 1, dur);
        };
        var stopBg = function (key, dur) {
            fadeOutAndAct(key, 2, dur);
        };
        var addSfx = function (key, fileName) {
            addSoundToList(key, fileName, 1);
        };
        var playSfx = function (key) {
            playSoundAsync(key);
        };
        var unloadAllSfx = function () {
            Native.exec("AudioController.unloadAllSfx");
        };
        var pauseAllSounds = function () {
            Native.exec("AudioController.pauseAllSounds");
        };
        var resumeAllSounds = function () {
            Native.exec("AudioController.resumeAllSounds");
        };

        exports.addSoundToList = addSoundToList;
        exports.removeSoundFromList = removeSoundFromList;
        exports.playSound = playSound;
        exports.playSoundAsync = playSoundAsync;
        exports.playSoundFromTo = playSoundFromTo;
        exports.playSoundTo = playSoundTo;
        exports.setVolumeForSoundTo = setVolumeForSoundTo;
        exports.playSoundFirstMs = playSoundFirstMs;
        exports.fadeInAndPlay = fadeInAndPlay;
        exports.pauseSound = pauseSound;
        exports.stopSound = stopSound;
        exports.fadeOutAndAct = fadeOutAndAct;
        exports.fadeVolumeTo = fadeVolumeTo;
        exports.setNumberOfLoopsForSound = setNumberOfLoopsForSound;
        exports.setTimes = setNumberOfLoopsForSound;
        exports.stopAllSounds = stopAllSounds;
        exports.addBg = addBg;
        exports.playBg = playBg;
        exports.pauseBg = pauseBg;
        exports.stopBg = stopBg;
        exports.addSfx = addSfx;
        exports.playSfx = playSfx;
        exports.unloadAllSfx = unloadAllSfx;
        exports.pauseAllSounds = pauseAllSounds;
        exports.resumeAllSounds = resumeAllSounds;
    });
});