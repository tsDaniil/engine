define(["./modules"], function () {
    defineModule("Navigation", function (require, exports) {
        var Native = $require("Native");

//  exports.on = function (event, f) { // подписываемся на событие, имя события передается параметром
//    Native.addEventCallback("Navigation", event, f);
//  };

        exports.onPageBecameKey = function (f) {
            Native.addEventCallback("Navigation", "pageBecameKey", f);
        };

        exports.onPageNoLongerKey = function (f) {
            Native.addEventCallback("Navigation", "pageNoLongerKey", f);
        };

        exports.onPageHidden = function (f) {
            Native.addEventCallback("Navigation", "pageHidden", f);
        };

        exports.onGlobalAlert = function (f) {
            Native.addEventCallback("Navigation", "globalAlert", f);
        };

        exports.onBookLoadedWithSection = function (f) {
            /* подписываемся на событие загрузки книги, вызывается когда приходит engineIsReady в видеоконтроллер, в хэндлер передается объект следующего вида:
             {
             @"sectionNumber":@(_sectionNumberBeforeLanguageSwitching),
             @"issueName":_issueData.issueName,
             @"issueNumber":[NSString stringWithFormat:@"%02d", _issueData.issueNumber.intValue],
             @"seasonNumber":[NSString stringWithFormat:@"%02d", _issueData.seasonNumber.intValue],
             @"bookName":_issueData.bookName,
             @"iPad1":@(IPAD_VERSION_IS_1)
             }
             */
            Native.removeAllCallBacksForEvent("Navigation", "bookLoadedWithSection");
            Native.addEventCallback("Navigation", "bookLoadedWithSection", f);
        };

        exports.onStoredValue = function (f) {
            Native.removeAllCallBacksForEvent("Navigation", "storedValue");
            Native.addEventCallback("Navigation", "storedValue", f);
        };

        exports.onSectionChanged = function (f) { // подписываемся на событие смены сцены, в хэндлер передается номер сцены, вызывается при переходах по оглавлению
            Native.removeAllCallBacksForEvent("Navigation", "sectionChanged");
            Native.addEventCallback("Navigation", "sectionChanged", f);
        };

        exports.globalAlert = function (data) {
            Native.exec("Navigation.globalAlert_", data);
        };

        exports.goToNextPage = function () { // переход на следующую страницу, для HTML-книжек
            Native.exec("Navigation.goToNextPage");
        };

        exports.goToPrevPage = function () { // переход на предыдущую страницу, для HTML-книжек
            Native.exec("Navigation.goToPrevPage");
        };

        exports.showNavigation = function () { // показать оглавление
            Native.exec("Navigation.showNavigation");
        };

        exports.exit = function () {
            Native.exec("Navigation.exit");
        };

        exports.setSettings = function (settings) {
            /*if (window.engineAPI.setSettingsHandler) {
             window.engineAPI.setSettingsHandler.call(window, settings);
             }*/
        };

        exports.showSettings = function () { // показать меню настроек
            Native.exec("Navigation.showSettings");
        };

        exports.getPageNumber = function (f) { // возвращает номер страницы в переданную функцию, для HTML-книжек
            Native.exec("Navigation.getPageNumber", f);
        };

        exports.enableNativeNavigationButtons = function () { // ничего не делает
            Native.exec("Navigation.enableNativeNavigationButtons");
        };

        exports.disableNativeNavigationButtons = function () { // ничего не делает
            Native.exec("Navigation.disableNativeNavigationButtons");
        };

        exports.scrollingEnabled = function (enabled) { // отключает скроллинг контейнера, для HTML-книжек
            Native.exec("Navigation.scrollingEnabled_", enabled);
        };

        exports.setSectionNumber = function (number, sectionsCount) { // сообщает контроллеру, что загружена сцена с заданным номером и общее количество сцен (нужно для виралки и правильного отображения текущей сцены в оглавлении)
            if (number === undefined) {
                return;
            }
            if (sectionsCount === undefined) {
                Native.exec("Navigation.setSectionNumber_", number);
            }
            else {
                Native.exec("Navigation.setSectionNumber_sectionsCount_", number, sectionsCount);
            }
        };

        exports.sectionFinished = function (number) { // сообщает контроллеру, что отыгралась сцена с заданным номером
            if (number === undefined) {
                return;
            }
            Native.exec("Navigation.sectionFinished_", number);
        };

        exports.engineIsReady = function () { // сообщает контроллеру о готовности к проигрыванию
            Native.exec("Navigation.engineIsReady");
        };

        exports.setStoredValueForKey = function (value, key) {
            if ((typeof key) != "string") return;
            Native.exec("Navigation.setStoredValue_forKey_", value, key);
        };

        exports.getStoredValueForKey = function (key) {
            if ((typeof key) != "string") return;
            Native.exec("Navigation.getStoredValueForKey_", key);
        };

        exports.anyAction = function (action, additionalParameters) {
            Native.exec("Navigation.anyAction_additionalParameters_", action, additionalParameters);
        };

        exports.log = function (msg) { // выводит сообщение в консоль дебаггера оси-хоста
            Native.exec("Navigation.log_", msg);
        };
    });
});