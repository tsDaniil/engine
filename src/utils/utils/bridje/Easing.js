define(["./modules"], function () {
    defineModule("Easing", function (require, exports) {
        exports.easeInQuad = function (x, t, b, c, d) {
            return c * (t /= d) * t + b;
        };

        exports.easeOutQuad = function (x, t, b, c, d) {
            return -c * (t /= d) * (t - 2) + b;
        };

        exports.easeInOutQuad = function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) {
                return c / 2 * t * t + b;
            }
            return -c / 2 * ((--t) * (t - 2) - 1) + b;
        };

        exports.easeInCubic = function (x, t, b, c, d) {
            return c * (t /= d) * t * t + b;
        };

        exports.easeOutCubic = function (x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t + 1) + b;
        };

        exports.easeInOutCubic = function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) {
                return c / 2 * t * t * t + b;
            }
            return c / 2 * ((t -= 2) * t * t + 2) + b;
        };

        exports.easeInQuart = function (x, t, b, c, d) {
            return c * (t /= d) * t * t * t + b;
        };

        exports.easeOutQuart = function (x, t, b, c, d) {
            return -c * ((t = t / d - 1) * t * t * t - 1) + b;
        };

        exports.easeInOutQuart = function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) {
                return c / 2 * t * t * t * t + b;
            }
            return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
        };

        exports.easeInQuint = function (x, t, b, c, d) {
            return c * (t /= d) * t * t * t * t + b;
        };
        exports.easeOutQuint = function (x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
        };

        exports.easeInOutQuint = function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) {
                return c / 2 * t * t * t * t * t + b;
            }
            return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
        };

        exports.easeInSine = function (x, t, b, c, d) {
            return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
        };

        exports.easeOutSine = function (x, t, b, c, d) {
            return c * Math.sin(t / d * (Math.PI / 2)) + b;
        };

        exports.easeInOutSine = function (x, t, b, c, d) {
            return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
        };
        exports.easeInExpo = function (x, t, b, c, d) {
            return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
        };
        exports.easeOutExpo = function (x, t, b, c, d) {
            return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
        };

        exports.easeInOutExpo = function (x, t, b, c, d) {
            if (t == 0) {
                return b;
            }
            if (t == d) {
                return b + c;
            }
            if ((t /= d / 2) < 1) {
                return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
            }
            return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
        };

        exports.easeInCirc = function (x, t, b, c, d) {
            return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
        };

        exports.easeOutCirc = function (x, t, b, c, d) {
            return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
        };

        exports.easeInOutCirc = function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) {
                return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
            }
            return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
        };

        exports.easeInElastic = function (x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) {
                return b;
            }
            if ((t /= d) == 1) {
                return b + c;
            }
            if (!p) {
                p = d * .3;
            }
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            }
            else {
                var s = p / (2 * Math.PI) * Math.asin(c / a);
            }
            return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        };

        exports.easeOutElastic = function (x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) {
                return b;
            }
            if ((t /= d) == 1) {
                return b + c;
            }
            if (!p) {
                p = d * .3;
            }
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            }
            else {
                var s = p / (2 * Math.PI) * Math.asin(c / a);
            }
            return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
        };

        exports.easeInOutElastic = function (x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) {
                return b;
            }
            if ((t /= d / 2) == 2) {
                return b + c;
            }
            if (!p) {
                p = d * (.3 * 1.5);
            }
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            }
            else {
                var s = p / (2 * Math.PI) * Math.asin(c / a);
            }
            if (t < 1) {
                return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
            }
            return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
        };

        exports.easeInBack = function (x, t, b, c, d, s) {
            if (s == undefined) {
                s = 1.70158;
            }
            return c * (t /= d) * t * ((s + 1) * t - s) + b;
        };

        exports.easeOutBack = function (x, t, b, c, d, s) {
            if (s == undefined) {
                s = 1.70158;
            }
            return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
        };
        exports.easeInOutBack = function (x, t, b, c, d, s) {
            if (s == undefined) {
                s = 1.70158;
            }
            if ((t /= d / 2) < 1) {
                return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
            }
            return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
        };

        exports.easeInBounce = function (x, t, b, c, d) {
            return c - this.easeOutBounce(x, d - t, 0, c, d) + b;
        };

        exports.easeOutBounce = function (x, t, b, c, d) {
            if ((t /= d) < (1 / 2.75)) {
                return c * (7.5625 * t * t) + b;
            } else if (t < (2 / 2.75)) {
                return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
            } else if (t < (2.5 / 2.75)) {
                return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
            } else {
                return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
            }
        };

        exports.easeInOutBounce = function (x, t, b, c, d) {
            if (t < d / 2) {
                return this.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
            }
            return this.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
        };

        exports.easeJam110Forward = function (x, t, b, c, d) {
            t /= d;
            if (t < 0.5) {
                return (c * (-17.6 * t * t * t + 13.2 * t * t) + b);
            } else {
                return (c * (1.6 * t * t * t - 3.6 * t * t + 2.4 * t + 0.6) + b);
            }
        };

        exports.easeJam110Backward = function (x, t, b, c, d) {
            t /= d;
            if (t < 0.5) {
                return (c * (1.6 * t * t * t - 1.2 * t * t) + b);
            } else {
                return (c * (-17.6 * t * t * t + 39.6 * t * t - 26.4 * t + 5.4) + b);
            }
        };

        exports.easeJam120Forward = function (x, t, b, c, d) {
            t /= d;
            if (t < 0.5) {
                return (c * (-19.2 * t * t * t + 14.4 * t * t) + b);
            } else {
                return (c * (3.2 * t * t * t - 7.2 * t * t + 4.8 * t + 0.2) + b);
            }
        };

        exports.easeJam120Backward = function (x, t, b, c, d) {
            t /= d;
            if (t < 0.5) {
                return (c * (3.2 * t * t * t - 2.4 * t * t) + b);
            } else {
                return (c * (-19.2 * t * t * t + 43.2 * t * t - 28.8 * t + 5.8) + b);
            }
        };

        exports.easeSin = function (x, t, b, c, d) {
            return c * Math.sin(t /= d) + b;
        };

        exports.easeCos = function (x, t, b, c, d) {
            return c * Math.cos(t /= d) + b;
        };

        exports.easeCosSin = function (x, t, b, c, d) {
            return c * Math.cos(t /= d) * Math.sin(t) + b;
        };

        exports.easeSinSin15 = function (x, t, b, c, d) {
            return c * Math.sin(t /= d) * Math.sin(t * 1.5) + b;
        };

        exports.easeSinTanCos12 = function (x, t, b, c, d) {
            return c * Math.sin(Math.tan(Math.cos(t /= d) * 1.2)) + b;
        };

        exports.easeSinTan005 = function (x, t, b, c, d) {
            return c * Math.sin(Math.tan(t /= d) * 0.05) + b;
        };

        exports.easeCosSin3Sin02 = function (x, t, b, c, d) {
            return c * Math.cos(Math.sin((t /= d) * 3) * Math.sin(t * 0.2)) + b;
        };

        exports.easeSinPow8Sin = function (x, t, b, c, d) {
            return c * Math.sin(Math.pow(8, Math.sin(t /= d))) + b;
        };

        exports.easeSinExpCos082 = function (x, t, b, c, d) {
            return c * Math.sin(Math.exp(Math.cos((t /= d) * 0.8)) * 2) + b;
        };

        exports.easeSinPItan001 = function (x, t, b, c, d) {
            return c * Math.sin((t /= d) - Math.PI * Math.tan(t) * 0.01) + b;
        };

        exports.easePowSinPI12 = function (x, t, b, c, d) {
            return c * Math.pow(Math.sin((t /= d) * Math.PI), 12) + b;
        };

        exports.easeCosSinTanPIPI8 = function (x, t, b, c, d) {
            return c * Math.cos(Math.sin(t /= d) * Math.tan(t * Math.PI) * Math.PI / 8) + b;
        };

        exports.easeSinTanPowSin10 = function (x, t, b, c, d) {
            return c * Math.sin(Math.tan(t /= d) * Math.pow(Math.sin(t), 10)) + b;
        };

        exports.easeCosSin33 = function (x, t, b, c, d) {
            return c * Math.cos(Math.sin((t /= d) * 3) + t * 3) + b;
        };

        exports.easePowAbsSin206sin206 = function (x, t, b, c, d) {
            return c * Math.pow(Math.abs(Math.sin((t /= d) * 2)) * 0.6, Math.sin(t * 2)) * 0.6 + b;
        };
    });
});