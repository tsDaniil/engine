define([
    "utils/utils/vendor/userControl/events/Touch",
    "utils/utils/vendor/userControl/events/Tap",
    "utils/utils/vendor/userControl/events/Swipe",
    "utils/utils/vendor/userControl/events/Move"
], function (Touch, Tap, Swipe, Move) {

    if (!window.requestAnimationFrame) {

        window.requestAnimationFrame = (function () {

            return window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function (/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {

                    window.setTimeout(callback, 1000 / 60);

                };

        })();

    }

    var tapTime = 200;
    var swipeTime = 200;

    /**
     * @type Lite
     */
    var $ = (function () {

        /**
         * @class Collection
         * @extends Array
         * @param {Array} arr
         * @constructor
         */
        var Collection = function (arr) {
            arr.forEach(function (elem) {
                this.push(elem);
            }, this);
        };
        (function (Collection, extend, methods) {
            Collection.prototype = extend;
            Collection.prototype.constructor = Collection;
            var coleectionActionsList = ["remove", "tap", "change", "hover", "on", "addClass", "removeClass", "toggleClass", "show", "hide", "displayToggle", "css"];
            for (var methodName in methods) {
                if (methods.hasOwnProperty(methodName)) {
                    Collection.prototype[methodName] = methods[methodName];
                }
            }
            coleectionActionsList.forEach(function (CollectionMethodName) {
                this.prototype[CollectionMethodName] = function () {
                    return this._do(CollectionMethodName, arguments);
                }
            }, Collection);
        })(Collection, [], {
            /**
             * @return {Lite}
             */
            last: function () {
                return this[this.length - 1];
            },
            /**
             * @return {Lite}
             */
            first: function () {
                return this[0];
            },
            /**
             * @param {String} method
             * @param {Array} [args]
             * @return {Collection}
             * @private
             */
            _do: function (method, args) {
                this.forEach(function ($elem) {
                    $elem[method].apply($elem, args);
                });
                return this;
            },
            /**
             * @param mapHandler
             * @return {Collection}
             */
            map: function (mapHandler) {
                return new Collection(Array.prototype.map.call(this, mapHandler));
            },
            /**
             * @param filterHandler
             * @return {Collection}
             */
            filter: function (filterHandler) {
                return new Collection(Array.prototype.filter.call(this, filterHandler));
            },
            eq: function (index) {
                return this[index];
            }
        });


        /**
         * @class Lite
         * @param {Node|String} some
         * @return {$}
         * @constructor
         */
        var Lite = function (some) {

            if (!(this instanceof Lite)) {
                return new Lite(some);
            }

            /**
             * Хранит события подписанные пользователем
             * @type {{}}
             * @private
             */
            this._events = {};
            /**
             * Хранит внутренние события
             * @type {{}}
             * @private
             */
            this._privateEvents = {};

            /**
             * @type {DomEventManagerEvents}
             * @private
             */
            this._bindedEvents = {};

            this._delegationMode = false;

            /**
             * Флаг наличия кроссплатформенных обработчиков
             * @type {boolean}
             */
            this._hasEvents = false;

            if (typeof some == "string") {
                /**
                 * @type {HTMLElement}
                 */
                this.node = document.querySelector(some);
            } else {

                if (Lite.isElement(some) || Lite.isDocument(some)) {
                    /**
                     * @type {HTMLElement}
                     */
                    this.node = some;
                } else {
                    alert("WTF its not Element!");
                }
            }

        };

        Lite.prototype = {

            /**
             * Запускаем событие
             * @param {string} eventName
             * @param {Array} args
             */
            trigger: function (eventName, args) {

                if (!eventName) {
                    console.error("wrong param!");
                    return this;
                }

                if (!args) {
                    args = [];
                }

                if (!this._events) {
                    return this;
                }

                this._splitEvent(eventName).forEach(function (event, index) {

                    var localArgs = this._currentArgs(eventName, index, args.slice());

                    if (this._events.hasOwnProperty(event)) {
                        this._startUserEvent(event, localArgs);
                    }

                }, this);
            },

            _splitEvent: function (eventName) {
                var events = eventName.split(":");

                var getPrev = function (index) {
                    if (index >= 1) {
                        return getPrev(index - 1) + events[index] + ":";
                    }
                    return events[index] + ":";
                };

                return events.map(function (event, index) {
                    return getPrev(index).slice(0, -1);
                });
            },

            _currentArgs: function (eventName, index, args) {
                var arr = eventName.split(":");
                arr.splice(0, index + 1);
                return arr.concat(args);
            },

            _startUserEvent: function (event, localArgs) {
                this._events[event].slice().forEach(function (eventObject) {
                    eventObject.handler.apply(eventObject.context, localArgs);
                });
            },

            on: function (eventName, handler, context) {

                if (!this._hasEvents) {
                    this._setPrivateHandlers();
                }

                if (!(eventName in this._events)) {
                    this._events[eventName] = [];
                }

                this._events[eventName].push({
                    handler: handler, context: context || this
                });
                return this;
            },

            off: function (eventName, handler) {

                if (!eventName) {
                    this._events = {};
                    return this;
                }

                if (!handler) {
                    delete this._events[eventName];
                    return this;
                }

                if (this._events[eventName]) {
                    this._events[eventName] = this._events[eventName].filter(function (event) {
                        return event.handler != handler;
                    });
                }

                return this;
            },

            /**
             * Назначаем внутренние обработчики
             * @private
             */
            _setPrivateHandlers: function () {

                var hasHandlers = false;

                this.addEventListener(Lite.events.start, function (event) {

                    if (hasHandlers) {
                        this.removeEventListener(Lite.events.move);
                        this.removeEventListener(Lite.events.end);
                        this._dropEvent();
                    }

                    if (this._delegationMode) {
                        event.stopPropagation();
                    }
                    event.preventDefault();

                    var reformatedEvent = this._reformatEvent(event);
                    if (reformatedEvent) {
                        this._createTouchEvent(reformatedEvent, "start");
                    }


                    this.addEventListener(Lite.events.move, function (event) {

                        if (this._delegationMode) {
                            event.stopPropagation();
                        }
                        event.preventDefault();

                        var reformatedEvent = this._reformatEvent(event);
                        if (reformatedEvent) {
                            this._createMoveEvent(reformatedEvent);
                        }

                        return false;

                    }.bind(this));

                    this.addEventListener(Lite.events.end, function (event) {

                        if (this._delegationMode) {
                            event.stopPropagation();
                        }
                        event.preventDefault();

                        var reformatedEvent = this._reformatEvent(event);

                        if (reformatedEvent) {
                            this._createTouchEvent(reformatedEvent, "end");
                        }


                        this.removeEventListener(Lite.events.move);
                        this.removeEventListener(Lite.events.end);

                        hasHandlers = false;

                        return false;

                    }.bind(this));

                    hasHandlers = true;
                    return false;

                }.bind(this));

                this._hasEvents = true;

            },

            setDelegationMode: function (mode) {
                this._delegationMode = mode;
                return this;
            },

            /**
             *
             * @param {userEvent} event
             * @param {string} status
             * @private
             */
            _createTouchEvent: function (event, status) {
                if (status == "start") {
                    this._startEvent(new Touch(event, status));
                } else {
                    /**
                     * Если мы отпускаем палец и палец не двигался, и между старт и энд меньше "tapTime" мс - созжаем тап
                     */
                    if (((this._bindedEvents.touch.startTime - event.time) < tapTime) && (!this._bindedEvents.move || this._bindedEvents.move.distance.general < 25)) {
                        this._createTapEvent(event);
                    }
                    if (this._bindedEvents.move && this._bindedEvents.move.hasSwipeSpeed && ((event.time - this._bindedEvents.touch.startTime) < swipeTime)) {
                        this._createSwipeEvent(event);
                    }
                    /**
                     * Если мы отпускаем палец и было движение и была набрана скорость свайпа и время касания меньше "swipeTime" мс - создаем свайп
                     */
                    this._startEvent(new Touch(event, status));
                    this._dropEvent();
                }
            },

            /**
             *
             * @param {userEvent} event
             * @private
             */
            _createMoveEvent: function (event) {
                this._startEvent(new Move(this._bindedEvents, event));
            },

            /**
             * @param event
             * @private
             */
            _createTapEvent: function (event) {
                this._startEvent(new Tap(this._bindedEvents, event));
            },

            /**
             * @param event
             * @private
             */
            _createSwipeEvent: function (event) {
                this._startEvent(new Swipe(this._bindedEvents, event))
            },

            _dropEvent: function () {
                this._bindedEvents = {};
                this.identifier = null;
            },

            /**
             *
             * @param {UserEventI|Touch|Move|Tap|Swipe} event
             * @private
             */
            _startEvent: function (event) {
                this._bindedEvents[event.type] = event;
                this.trigger(event.getEventString(), [event.getClone()]);
            },

            /**
             * Преобразуем событие
             * @param event
             * @return {userEvent}
             * @private
             */
            _reformatEvent: function (event) {
                var touch;
                if (event.touches) {
                    var touches = event.touches.length ? event.touches : event.changedTouches;
                    if (this.identifier == null) {
                        this.identifier = touches[0].identifier;
                        touch = touches[0];
                    } else {
                        for (var i = 0; i < touches.length; i++) {
                            if (this.identifier == touches[i].identifier) {
                                touch = touches[i];
                            }
                        }
                    }
                } else {
                    touch = event;
                }
                if (!touch) {
                    return undefined;
                } else {
                    return {
                        coords: {
                            x: touch.pageX,
                            y: touch.pageY
                        },
                        time: event.timeStamp,
                        originEvent: event
                    }
                }

            },

            /**
             * @param {String} html
             * @return {String|$}
             */
            html: function (html) {
                if (arguments.length) {
                    this.node.innerHTML = html;
                } else {
                    return this.node.innerHTML;
                }
                return this;
            },

            /**
             * @param {String} className
             * @private
             */
            _addClass: function (className) {
                var classList = this.node.className.split(" ");
                classList.push(className);
                this.node.className = classList.join(" ");
            },

            /**
             * @param {String} className
             * @private
             */
            _removeClass: function (className) {
                var classList = this.node.className.split(" ");
                this.node.className = classList.filter(function (name) {
                    return name != className;
                }).join(" ");
            },

            /**
             * @param {String} className
             * @return {$}
             */
            addClass: function (className) {
                if (!this.hasClass(className)) {
                    this._addClass(className);
                }
                return this;
            },

            /**
             * @param {String} className
             * @return {boolean}
             */
            hasClass: function (className) {
                var classList = this.node.className.split(" ");
                return classList.some(function (name) {
                    return name === className;
                });
            },

            /**
             * @param {String} className
             * @return {$}
             */
            removeClass: function (className) {
                if (arguments.length) {
                    if (this.hasClass(className)) {
                        this._removeClass(className);
                    }
                } else {
                    this.node.className = "";
                }
                return this;
            },

            /**
             * @param {String} className
             * @return {$}
             */
            toggleClass: function (className) {
                return this.hasClass(className) ? this._removeClass(className) : this._addClass(className);
            },

            /**
             * @param {String|Node|$} elem
             * @return $
             */
            append: function (elem) {

                if (typeof elem === "string") {
                    elem = Lite.parseHTML(elem);
                    Lite.each(Lite.toArray(elem.childNodes), function (node) {
                        this.append(node);
                    }.bind(this));
                    return this;
                }

                if (elem instanceof Lite) {
                    elem = elem.node;
                }

                if (elem instanceof Collection) {
                    elem.forEach(function (elem) {
                        this.append(elem);
                    }, this);
                    return this;
                }

                this.node.appendChild(elem);
                return this;
            },

            /**
             *
             * @param value
             * @return {*}
             */
            val: function (value) {
                if (arguments.length) {
                    this.node.value = value;
                } else {
                    return this.node.value;
                }
                return this;
            },

            /**
             *
             * @param elem
             * @return {$}
             */
            prepend: function (elem) {

                if (typeof elem === "string") {
                    elem = Lite.parseHTML(elem);
                    Lite.each(Lite.toArray(elem.childNodes), function (node) {
                        this.prepend(node);
                    }.bind(this));
                    return this;
                }

                if (elem instanceof Lite) {
                    elem = elem.node;
                }

                if (elem instanceof Collection) {
                    elem.forEach(function (elem) {
                        this.prepend(elem);
                    }, this);
                    return this;
                }

                var children = this.children();
                if (children.length) {
                    this.node.insertBefore(elem, children.first().node);
                } else {
                    this.append(elem);
                }
                return this;
            },

            /**
             * @param {String} eventName
             * @param {function} handler
             * @return {$}
             */
            addEventListener: function (eventName, handler) {

                if (!this._privateEvents[eventName]) {
                    this._privateEvents[eventName] = [];
                }

                this._privateEvents[eventName].push(handler);

                if ("addEventListener" in this.node) {
                    this.node.addEventListener(eventName, handler, false);
                } else {
                    this.node.attachEvent(eventName, handler);
                }

                return this;
            },

            /**
             * @param {String} [eventName]
             * @param {function} [handler]
             * @return {$}
             */
            removeEventListener: function (eventName, handler) {

                if (!eventName) {
                    for (var name in this._privateEvents) {
                        if (this._privateEvents.hasOwnProperty(name)) {
                            this._privateEvents[name].forEach(function (handler) {
                                this.node.removeEventListener(name, handler);
                            }, this);
                        } else {
                            break;
                        }
                    }
                    this._privateEvents = {};
                    return this;
                }

                if (!handler) {
                    if (eventName in this._privateEvents) {
                        this._privateEvents[eventName].forEach(function (handler) {
                            this.node.removeEventListener(eventName, handler);
                        }, this);
                        delete  this._privateEvents[eventName];
                    }
                    return this;
                }

                if (eventName in this._privateEvents) {
                    this._privateEvents[eventName].filter(function (binding) {
                        if (binding === handler) {
                            this.node.removeEventListener(eventName, binding);
                            return false;
                        } else {
                            return true;
                        }
                    }, this);
                }

                return this;
            },

            clone: function () {
                return new $(this.node.cloneNode(true));
            },

            /**
             * @param {function} handler
             * @return {$}
             */
            tap: function (handler) {

                if (!handler) {
                    var rect = this.getRect();
                    this.trigger("User:tap", [new Tap({
                            touch: {startTime: new Date().getTime()}
                        },
                        {
                            coords: {
                                x: rect.coords.x + rect.size.width / 2,
                                y: rect.coords.y + rect.size.height / 2
                            },
                            timeStamp: new Date().getTime()
                        })]);
                    return this;
                }

                return this.on("User:tap", handler);
            },

            /**
             * @param {function} handlerEnter
             * @param {function} handlerOut
             * @return {$}
             */
            hover: function (handlerEnter, handlerOut) {
                this.addEventListener("mouseenter", handlerEnter);
                this.addEventListener("mouseout", handlerOut);
                return this;
            },

            /**
             * @param {function} handler
             * @return {$}
             */
            change: function (handler) {
                return this.addEventListener("change", handler);
            },

            /**
             * @param {number} x
             * @param {number} y
             * @return {$}
             */
            move: function (x, y) {
                return this.css({
                    "left": (parseInt(this.css("left")) + x) + "px",
                    "top": (parseInt(this.css("top")) + y) + "px"
                });
            },

            /**
             * @param {Object|String} name
             * @param {String} [value]
             * @return {String|$}
             */
            css: function (name, value) {

                var prefixStyles = {
                    "transform": true,
                    "transition": true,
                    "transform-origin": true,
                    "box-shadow": true,
                    "animation": true
                };

                var prefixes = [
                    "-moz-",
                    "-ms-",
                    "-webkit-",
                    "-o-"
                ];

                if (Lite.isObject(name)) {
                    Lite.each(name, function (value, name) {
                        this.css(name, value);
                    }.bind(this));
                    return this;
                }

                if (value === undefined) {
                    if (this.node.style[name]) {
                        return this.node.style[name];
                    } else {
                        return getComputedStyle(this.node)[name];
                    }
                }

                if (name in prefixStyles) {

                    prefixes.forEach(function (prefix) {
                        this.css(prefix + name, value);
                    }.bind(this));
                }

                function camelCase(input) {
                    return input.toLowerCase().replace(/-(.)/g, function (match, group1) {
                        return group1.toUpperCase();
                    });
                }

                if (/-(.)/g.test(name)) {
                    this.node.style[camelCase(name)] = value;
                }
                this.node.style[name] = value;

                return this;
            },

            displayToggle: function () {
                if (this.css("display") == "none") {
                    this.css("display", "block");
                } else {
                    this.css("display", "none");
                }
                return this;
            },

            /**
             * @param {String} name
             * @param {String} [value]
             * @return {String|$}
             */
            attr: function (name, value) {
                if (arguments.length != 2) {
                    return this.node.getAttribute(name);
                } else {
                    this.node.setAttribute(name, value);
                }
                return this;
            },

            /**
             * @param {String} selector
             * @return {Collection}
             */
            find: function (selector) {
                return Lite.toCollection(this.node.querySelectorAll(selector)).map(function (element) {
                    return new Lite(element);
                });
            },

            findElement: function (selector) {
                return new Lite(this.node.querySelector(selector));
            },

            /**
             * @return {Collection}
             */
            children: function () {
                return Lite.toCollection(this.node.childNodes).filter(function (node) {
                    return Lite.isElement(node);
                }).map(function (element) {
                    return new Lite(element);
                });
            },

            /**
             * @return {$}
             */
            hide: function () {
                return this.css("display", "none");
            },

            /**
             * @return {$}
             */
            show: function () {
                return this.css("display", "block");
            },

            /**
             * @return {$}
             */
            remove: function () {
                if (this.node.parentNode)
                    this.node.parentNode.removeChild(this.node);
                this.removeEventListener();
                return this;
            },

            /**
             * @param {Boolean} [recursiv]
             * @return {{left: number, top: number}}
             */
            offset: function (recursiv) {
                var offset = {
                    left: this.node.offsetLeft,
                    top: this.node.offsetTop
                };
                if (recursiv && this.node.getAttribute("id") != "bigrect") {
                    var getOffset = function (element, offset) {
                        if (element.getAttribute("id") != "bigrect") {
                            offset.left += element.offsetLeft;
                            offset.top += element.offsetTop;
                            return getOffset(element.parentNode, offset);
                        } else {
                            return offset;
                        }
                    };
                    return getOffset(this.node.parentNode, offset);
                } else {
                    return offset;
                }
            },

            /**
             * Получаем область тача кнопки
             * @return {{x: number, y: number, width: number, height: number}}
             */
            getRect: function (touchAreaX, touchAreaY) {
                var offset = this.offset(true);
                return {
                    coords: {
                        x: offset.left - (touchAreaX || 0),
                        y: offset.top - (touchAreaY || touchAreaX || 0)
                    }, size: {
                        width: this.width() + (touchAreaX || 0) * 2,
                        height: this.height() + (touchAreaY || touchAreaX || 0) * 2
                    }
                }
            },

            /**
             * @return {$}
             */
            parent: function () {
                return new $(this.node.parentNode);
            },

            /**
             * @param {Number} [width]
             * @return {$|Number}
             */
            width: function (width) {
                if (arguments.length) {
                    this.css("width", width + "px");
                } else {
                    return this.node.clientWidth;
                }
                return this;
            },

            /**
             * @param {Number} [height]
             * @return {$|Number}
             */
            height: function (height) {
                if (arguments.length) {
                    this.css("height", height + "px");
                } else {
                    return this.node.clientHeight;
                }
                return this;
            },

            /**
             * @param {String} [text]
             * @return {String|$}
             */
            text: function (text) {
                if (!arguments.length) {
                    return this.html(text);
                } else {
                    var texts = [];

                    function getTexts(nodeList) {

                        Lite.toArray(nodeList).forEach(function (node) {

                            if (Lite.isElement(node)) {
                                getTexts(node.childNodes);
                            } else {
                                texts.push(node.textContent);
                            }

                        });
                    }

                    getTexts(this.node.querySelectorAll("*"));

                    return texts.filter(function (text) {
                        return /\S/.test(text);
                    }).map(function (text) {
                        return text.replace(/\s{2,}/g, "");
                    }).join(" ");
                }
            },

            /**
             * @return {boolean}
             */
            hasText: function () {
                function getTexts(nodeList) {

                    for (var i = 0; i < nodeList.length; i++) {
                        if (Lite.isElement(nodeList[i])) {
                            if (getTexts(nodeList[i].childNodes)) {
                                return true;
                            }
                        } else {
                            if (!!nodeList[i].textContent) {
                                return true;
                            }
                        }

                    }
                    return false;
                }

                return getTexts(this.node.childNodes);
            },

            /**
             * @return {$}
             */
            empty: function () {
                return this.html("");
            }
        };

        /**
         * @param {Object} opts
         * @param {Number} opts.duration
         * @param {function(progress)} opts.step
         * @param {function} [opts.callback]
         */
        Lite.animate = function (opts) {

            var Animate = {
                callback: opts.callback,
                start: new Date().getTime(),
                frame: function () {
                    this.progress = (new Date().getTime() - this.start) / opts.duration;
                    if (this.progress > 1) this.progress = 1;

                    // отрисовать анимацию
                    opts.step(this.progress);

                    if (this.progress == 1) {   // конец :)}
                        if (this.callback) {
                            this.callback();
                        }
                    } else {
                        requestAnimationFrame(this.frame.bind(this));
                    }
                },
                end: function () {
                    this.progress = 1;
                }
            };

            Animate.frame();

            return Animate;
        };

        /**
         * @param {string} html
         * @param [noNative]
         * @return {$}
         * @static
         */
        Lite.parseHTML = function (html, noNative) {
            var result = document.createElement("div");
            result.innerHTML = html;
            return noNative ? new $(result) : result;
        };

        /**
         * @param elem
         * @param type
         * @return {boolean}
         * @static
         */
        Lite.isType = function (elem, type) {
            return elem instanceof type;
        };

        /**
         * @param some
         * @return {boolean}
         * @static
         */
        Lite.isArray = function (some) {
            return Lite.isType(some, Array);
        };

        /**
         * @param some
         * @return {boolean}
         * @static
         */
        Lite.isString = function (some) {
            return Lite.isType(some, String);
        };

        /**
         * @param some
         * @return {boolean}
         * @static
         */
        Lite.isObject = function (some) {
            return Lite.isType(some, Object);
        };

        /**
         * @param some
         * @return {boolean}
         * @static
         */
        Lite.isFunc = function (some) {
            return Lite.isType(some, Function);
        };

        /**
         * @param some
         * @return {boolean}
         * @static
         */
        Lite.isNumber = function (some) {
            return Lite.isType(some, Number);
        };

        /**
         * @param node
         * @return {boolean}
         * @static
         */
        Lite.isElement = function (node) {
            return node.nodeType === 1;
        };

        /**
         * @param node
         * @return {boolean}
         * @static
         */
        Lite.isDocument = function (node) {
            return node.nodeType === 9;
        };

        /**
         * @param some
         * @param callback
         * @static
         */
        Lite.each = function (some, callback) {
            for (var key in some) {
                if (some.hasOwnProperty(key)) {
                    callback(some[key], key);
                }
            }
        };

        /**
         * @param {Array} elements
         * @return {Collection}
         * @static
         */
        Lite.createCollection = function (elements) {
            return Lite.toCollection(elements);
        };

        /**
         * @param collection
         * @return {Array}
         * @static
         */
        Lite.toArray = function (collection) {
            var result = [];
            if (collection instanceof NodeList || collection instanceof arguments.constructor) {
                for (var i = 0; i < collection.length; i++) {
                    result.push(collection[i]);
                }
            } else {
                alert("OMG, WTF!!");
            }
            return result;
        };

        /**
         * @param collection
         * @return {Collection}
         * @static
         */
        Lite.toCollection = function (collection) {
            var result = [];
            if (collection instanceof NodeList || collection instanceof arguments.constructor) {
                for (var i = 0; i < collection.length; i++) {
                    result.push(collection[i]);
                }
            } else {
                alert("OMG, WTF!!");
            }
            return new Collection(result);
        };

        /**
         * @param tagName
         * @param classList
         * @param isDome
         * @return {$}
         * @static
         */
        Lite.createElement = function (tagName, classList, isDome) {
            var element = document.createElement(tagName);
            element.className = classList;
            return isDome ? new $(element) : element;
        };

        /**
         * Клонирование простых объектов
         * @param {Object|Array} param
         */
        Lite.cloneLite = function (param) {

            if (Lite.isArray(param)) {
                return param.slice();
            }
            var result = {};

            var clone = function (old, new$) {

                Lite.each(old, function (value, key) {

                    if (Lite.isObject(value)) {
                        if (Lite.isArray(value)) {
                            new$[key] = value.slice();
                        } else {
                            new$[key] = {};
                            clone(value, new$[key]);
                        }
                    } else {
                        new$[key] = value;
                    }
                });

            };

            clone(param, result);
            return result;
        };

        Lite.merge = function ($default, data) {



        };

        Lite.events = {
            start: "mousedown",
            move: "mousemove",
            end: "mouseup"
        };

        Lite.setEvents = function (events) {
            Lite.events = events;
        };

        Lite.hasInArray = function (arr, some) {
            return arr.some(function (elem) {
                return elem === some
            });
        };

        Lite.extend = function (Object) {
            var f = Object.constructor || function () {
                };
            f.prototype = new Lite(document.createElement("DIV"));
            f.prototype.constructor = Object.constructor || function () {
            };
            for (var method in Object) {
                if (Object.hasOwnProperty(method)) {
                    f.prototype[method] = Object[method];
                }
            }
            return f;
        };

        var events = {
            mouse: {
                start: "mousedown",
                move: "mousemove",
                end: "mouseup"
            },
            mobile: {
                start: "touchstart",
                move: "touchmove",
                end: "touchend"
            },
            winMobile: {
                start: "MSPointerDown",
                move: "MSPointerMove",
                end: "MSPointerUp"
            }
        };

        var EventDetector = {
            run: function () {
                if (window.navigator.msPointerEnabled) {
                    Lite.setEvents(events.winMobile);
                } else if (this.getEventSupport("touchmove")) {
                    Lite.setEvents(events.mobile);
                } else {
                    Lite.setEvents(events.mouse);
                }
            },
            getEventSupport: function (event) {
                event = "on" + event;
                var target = document.createElement("div");
                target.setAttribute(event, "");
                var isSupported = typeof target[event] === "function";
                if (typeof target[event] !== "undefined") target[event] = null;
                target.removeAttribute(event);
                return isSupported;
            }
        };

        EventDetector.run();

        window.addEventListener(Lite.events.start, function (event) {
            event.preventDefault();
        }, false);

        return Lite;

    })();

    return $;
});