/// <reference path="../../../../../interface/Events.d.ts" />

import UserEvent = require('./UserEvent');

var isHorizontalCoff = 2 / 3;

class Swipe extends UserEvent implements SwipeI {

    /**
     * Координаты
     */
    public coords:CoordsI;
    /**
     * Координаты прикосновения
     */
    public startCoords:CoordsI;
    /**
     * Направление ("left"|"right"|"up"|"down")
     */
    public direction:string;
    /**
     * горизонтальный ли свайп
     */
    public isHorizontal:boolean = false;
    /**
     * Направление свайпа
     */
    public vector:CoordsI;

    /**
     * Тип
     * @type {String}
     */
    public type:string = "swipe";

    constructor(parentEvents:DomEventManagerEvents, eventData:userEvent) {

        super(eventData);

        this.coords = eventData.coords;
        this.startCoords = parentEvents.touch.coords;

        this.vector = {
            x: this.coords.x - this.startCoords.x,
            y: this.coords.y - this.startCoords.y
        };

        if (Math.abs(this.vector.x) / Math.abs(this.vector.y || 0.00001) > isHorizontalCoff) {
            this.isHorizontal = true;
        }

        if (this.isHorizontal) {
            this.direction = Swipe.getDirection(this.vector.x, "left", "right");
        } else {
            this.direction = Swipe.getDirection(this.vector.y, "up", "down");
        }
    }

    public getEventString():string {
        return "User:swipe:" + (this.isHorizontal ? "horizontal:" : "vertical:") + this.direction;
    }

    private static getDirection(vector:number, dorection1:string, direction2:string):string {
        if (vector < 0) {
            return dorection1;
        } else {
            return direction2;
        }
    }

}
export = Swipe;