/// <reference path="../../../../../interface/Events.d.ts" />

class UserEvent implements UserEventI {

    public type:string;
    public coords:CoordsI;
    private originEvent:MouseEvent;

    constructor(event:userEvent) {
        this.originEvent = event.originEvent;
    }

    public hitTestByRect(rect:RectI):boolean {
        return ((this.coords.x >= rect.coords.x) && (this.coords.y >= rect.coords.y)
            && (this.coords.x <= rect.coords.x + rect.size.width) && (this.coords.y < rect.coords.y + rect.size.height));
    }

    public getEventString():string {
        return "User:" + this.type;
    }

    public getClone():any {
        return Object.create(this);
    }

    public stopPropagation():void {
        this.originEvent.stopPropagation();
    }
}

export = UserEvent;