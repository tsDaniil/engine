/// <reference path="../../../../../interface/Events.d.ts" />

import UserEvent = require('./UserEvent');

class Tap extends UserEvent implements TapI {

    /**
     * Координаты
     */
    public coords:CoordsI;
    /**
     * тип
     * @type {string}
     */
    public type:string = "tap";
    /**
     * время прикосновения
     */
    public startTime:number;
    /**
     * время
     */
    public timeStamp:number;

    constructor(parentEvents:DomEventManagerEvents, eventData:userEvent) {

        super(eventData);

        this.coords = eventData.coords;
        this.startTime = parentEvents.touch.startTime;
        this.timeStamp = eventData.time;

    }

}
export = Tap;