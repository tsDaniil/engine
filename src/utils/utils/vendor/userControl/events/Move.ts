/// <reference path="../../../../../interface/Events.d.ts" />

import UserEvent = require('./UserEvent');

var swipeSpeed = 0.5;

class Move extends UserEvent implements MoveI {

    /**
     * Координаты тача
     */
    public coords: CoordsI;
    /**
     * координаты старта тача
     */
    public startCoords: CoordsI;
    /**
     * координаты тача в прошлый мув
     */
    public lastCoords: CoordsI;
    /**
     * дистанция пройденная за весь мув
     */
    public distance: DistanceI;
    /**
     * дистанция пройденная с последнего мува
     */
    public lastDistance: DistanceI;
    /**
     * скорость общая
     */
    public speed: SpeedI;
    /**
     * скорость с последнего мува
     */
    public lastSpeed: SpeedI;
    /**
     * Тип события
     */
    public type: string = "move";
    /**
     * Время прикосновения
     */
    public startTime: number;
    /**
     * набрал ли скорость необходимуб для свайпа
     */
    public hasSwipeSpeed: boolean;
    /**
     * текущее время
     */
    public timeStamp:number;

    constructor(parentEvents:DomEventManagerEvents, eventData:userEvent) {

        super(eventData);

        var lastCoords = parentEvents.move ? parentEvents.move.coords : parentEvents.touch.coords;
        var timeStamp = parentEvents.move ? parentEvents.move.timeStamp : parentEvents.touch.startTime;

        this.coords = eventData.coords;
        this.startCoords = parentEvents.touch.coords;
        this.lastCoords = lastCoords;

        this.distance = Move.currentDistance(parentEvents.touch.coords, eventData.coords);
        this.lastDistance = Move.currentDistance(lastCoords, eventData.coords);

        this.timeStamp = eventData.time;
        this.startTime = parentEvents.touch.startTime;

        this.speed = Move.currentSpeed(this.distance, this.timeStamp - this.startTime);
        this.lastSpeed = Move.currentSpeed(this.lastDistance, this.timeStamp - timeStamp);

        if (parentEvents.move && parentEvents.move.hasSwipeSpeed) {
            this.hasSwipeSpeed = true;
        } else {
            this.hasSwipeSpeed = this.lastSpeed.general >= swipeSpeed;
        }
    }

    private static currentDistance(startCoords:CoordsI, endCoords:CoordsI):DistanceI {
        var x = endCoords.x - startCoords.x, y = endCoords.y - startCoords.y;
        return {
            x: x,
            y: y,
            general: Math.sqrt(Math.pow(x,2) + Math.pow(y,2))
        }
    }

    private static currentSpeed(distance:DistanceI, time:number):SpeedI {
        return {
            x: distance.x / time,
            y: distance.y / time,
            general: distance.general / time
        }
    }
}
export = Move;