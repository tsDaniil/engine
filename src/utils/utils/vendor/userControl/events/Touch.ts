/// <reference path="../../../../../interface/Events.d.ts" />

import UserEvent = require('./UserEvent');

class Touch extends UserEvent implements TouchI {

    /**
     * координаты
     */
    public coords:CoordsI;
    /**
     * тип события
     * @type {string}
     */
    public type:string = "touch";
    /**
     * статус ("start"|"end")
     */
    public status:string;
    /**
     * время события
     */
    public startTime:number;

    constructor(eventData:userEvent, status:string) {

        super(eventData);

        this.coords = eventData.coords;
        this.status = status;
        this.startTime = eventData.time;
    }

    /**
     * Полчаем строку которая будет создавать событие
     * @return {string}
     */
    public getEventString():string {
        return "User:" + this.type + ":" + this.status;
    }

}
export = Touch;