/// <reference path="../../../../interface/Events.d.ts" />
/// <reference path="../../../../interface/EventManagerI.d.ts" />
/// <reference path="../../../../interface/DomLite.d.ts" />

interface InteractiveObjI {
    handler:(a?:any, b?:any, c?:any, d?:any)=>any;
    context:any;
    rect:InteractiveRectI;
}

import $ = require("$");

var manager:EventManager;
class EventManager implements EventManagerI {

    private _events:Object;
    private node:$i;

    constructor() {
        this.node = $("#bigrect");
        this.init();
    }

    private init():void {
        this.node.on("User", function () {
            var event = arguments[arguments.length - 1];
            this.startEvent(event);
        }, this);
    }

    /**
     * Запускаем событие
     * @param event
     */
    public startEvent(event:UserEventI):void {
        this.trigger(event.getEventString(), [event]);
    }

    /**
     * Запускает события (делит по именам и передает дальше)
     * @param eventName
     * @param args
     * @return {EventManager}
     */
    public trigger(eventName:string, args?:Array<any>):EventManagerI {

        var userEventManager = this;

        if (!eventName) {
            console.error("wrong param!");
            return this;
        }

        if (!args) {
            args = [];
        }

        if (!this._events) {
            return this;
        }

        this.splitEvent(eventName).forEach(function (event:string, index:number) {

            var localArgs = EventManager.currentArgs(eventName, index, args.slice());

            if (userEventManager._events.hasOwnProperty(event)) {
                userEventManager.startUserEvent(event, localArgs);
            }

        }, this);

        return this;
    }

    private static currentArgs(eventName:string, index:number, args:Array<any>):Array<any> {
        var arr = eventName.split(":");
        arr.splice(0, index + 1);
        return arr.concat(args);
    }

    private startUserEvent(event:string, localArgs:Array<any>):void {

        var UserEvent:UserEventI = localArgs[localArgs.length - 1];

        this._events[event].slice().some(function (eventObject:InteractiveObjI) {
            if (eventObject.rect.hitTest(UserEvent)) {
                eventObject.handler.apply(eventObject.context, localArgs);
                if (eventObject.rect.isStop()) {
                    return true;
                }
            }
        });
    }

    private splitEvent(eventName:string):Array<string> {
        var events = eventName.split(":");

        var getPrev = function (index) {
            if (index >= 1) {
                return getPrev(index - 1) + events[index] + ":";
            }
            return events[index] + ":";
        };

        return events.map(function (event, index) {
            return getPrev(index).slice(0, -1);
        });
    }

    public on(eventName:string, handler:(a?, b?, c?, d?) => any, rect:InteractiveRectI, context?:any):EventManagerI {

        if (!eventName || !handler) {
            console.error("Неверные параметры!");
            return this
        }

        if (!this._events) {
            this._events = {};
        }

        if (!this._events[eventName]) {
            this._events[eventName] = [];
        }

        this._events[eventName].push({handler: handler, context: context || this, rect: rect});

        if (this._events[eventName].length > 1) {
            this._events[eventName].sort(function (a, b) {
                return a.rect.getLevel() - b.rect.getLevel();
            });
        }

        return this;
    }

    public off(eventName?:string, handler?:(a?, b?, c?, d?) => any):EventManagerI {

        if (!eventName) {
            this._events = {};
            return this;
        }

        if (!handler) {
            if (this._events[eventName]) {
                delete this._events[eventName];
            }
            return this;
        }

        if (this._events[eventName]) {

            for (var i = 0; i < this._events[eventName].length; i++) {
                if (handler === this._events[eventName][i].handler) {
                    this._events[eventName].splice(i, 1);
                    break;
                }
            }

        }

        return this;
    }

}
manager = new EventManager();
export = manager;