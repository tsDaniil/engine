/// <reference path="../../../../interface/DomLite.d.ts" />
/// <reference path="../../../../interface/require.d.ts" />

interface eventNamesI {
    start:string;
    move:string;
    end:string;
}

import Touch = require("./events/Touch");
import Tap = require("./events/Tap");
import Swipe = require("./events/Swipe");
import Move = require("./events/Move");
import EventManager = require("./EventManager");
import $ = require("$");

var tapTime = 200;
var swipeTime = 200;

var events = {
    mouse: {
        start: "mousedown",
        move: "mousemove",
        end: "mouseup"
    },
    mobile: {
        start: "touchstart",
        move: "touchmove",
        end: "touchend"
    },
    winMobile: {
        start: "MSPointerDown",
        move: "MSPointerMove",
        end: "MSPointerUp"
    }
};

var domEventManager;
class DomEventManager {

    /**
     * Элемент на котором слушаем события
     */
    private node:Element;
    /**
     * Объект с событиями для подписки (винда / тач / мышь)
     */
    private eventTypes:eventNamesI;
    /**
     * Объект произошедших событий с момента тач старт до тач энд
     */
    private events:DomEventManagerEvents;
    /**
     * Идентификатор пальца (или 0)
     */
    private identifier:number;

    constructor() {
        this.node = document.querySelector("#bigrect");
        this.init();
    }

    /**
     * Инициализация
     */
    private init():void {

        this.detectEvents();
        this.dropEvent();
        this.setHandlers();

    }

    /**
     * Сбрасываем объект событий
     */
    private dropEvent():void {
        this.events = {};
    }

    /**
     * Определяем строки для подписок на события
     */
    private detectEvents():void {

        if (window.navigator.msPointerEnabled) {
            this.eventTypes = events.winMobile;
        } else if (DomEventManager.getEventSupport("touchstart")) {
            this.eventTypes = events.mobile;
        } else {
            this.eventTypes = events.mouse;
        }

        var that = this;
        require(["$"], function ($:$Static) {
            $.setEvents(that.eventTypes);
        });

    }

    /**
     * Проверяем наличие событий
     * @param type
     * @return {boolean}
     */
    private static getEventSupport(type:string):boolean {
        type = "on" + type;
        var target = document.createElement("div");
        target.setAttribute(type, "");
        var isSupported = typeof target[type] === "function";
        if (typeof target[type] !== "undefined") target[type] = null;
        target.removeAttribute(type);
        return isSupported;
    }

    /**
     * Переформатируем стандартное событие в кроссплатформенное событие
     * @param event
     * @return {{coords: {x: number, y: number}, time: number, originEvent: any}}
     */
    private reformatEvent(event) {
        var touch;
        if (event.touches) {
            var touches = event.touches.length ? event.touches : event.changedTouches;
            if (this.identifier == null) {
                this.identifier = touches[0].identifier;
                touch = touches[0];
            } else {
                for (var i = 0; i < touches.length; i++) {
                    if (this.identifier == touches[i].identifier) {
                        touch = touches[i];
                    }
                }
            }
        } else {
            touch = event;
        }
        return {
            coords: {
                x: touch.pageX,
                y: touch.pageY
            },
            time: event.timeStamp,
            originEvent: event
        }
    }

    /**
     * Назначаем обработчики
     */
    private setHandlers():void {

        this.setStartHandlers();
        this.setMoveHandlers();
        this.setEndHandlers();

    }

    /**
     * Назначаем обработчики начал (тач старт)
     */
    private setStartHandlers():void {

        var that = this;
        this.node.addEventListener(this.eventTypes.start, function (event) {

            event.stopPropagation();
            event.preventDefault();

            that.createTouchEvent(that.reformatEvent(event), "start");

        }, false);

    }

    /**
     * Назначаем обработчики движения (тач мув)
     */
    private setMoveHandlers():void {

        var that = this;

        this.node.addEventListener(this.eventTypes.move, function (event) {

            if (that.events.touch) {

                event.stopPropagation();
                event.preventDefault();

                that.createMoveEvent(that.reformatEvent(event));
            }
        }, false);

    }

    /**
     * Назначаем обработчик отпускания пальца (тач енд)
     */
    private setEndHandlers():void {

        var that = this;
        this.node.addEventListener(this.eventTypes.end, function (event) {

            if (that.events.touch) {

                event.stopPropagation();
                event.preventDefault();

                that.createTouchEvent(that.reformatEvent(event), "end");
            }

        }, false);

    }

    /**
     * Создаем тач событие
     * @param  event
     * @param status
     */
    private createTouchEvent(event:userEvent, status:string):void {

        if (status == "start") {
            this.startEvent(new Touch(event, status));
        } else {
            /**
             * Если мы отпускаем палец и палец не двигался, и между старт и энд меньше "tapTime" мс - созжаем тап
             */
            if (this.events.touch.startTime - event.time < tapTime && (!this.events.move || this.events.move.distance.general <= 20)) {
                this.createTapEvent(event);
            }
            if (this.events.move && this.events.move.hasSwipeSpeed && ((event.time - this.events.touch.startTime) < swipeTime) && this.events.move.distance.general >= 20) {
                this.createSwipeEvent(event);
            }
            /**
             * Если мы отпускаем палец и было движение и была набрана скорость свайпа и время касания меньше "swipeTime" мс - создаем свайп
             */
            this.startEvent(new Touch(event, status));
            this.dropEvent();
        }

    }

    /**
     * Создаем тап
     * @param event
     */
    private createTapEvent(event:userEvent):void {
        this.startEvent(new Tap(this.events, event));
    }

    /**
     * Создаем свайп
     * @param event
     */
    private createSwipeEvent(event:userEvent):void {
        this.startEvent(new Swipe(this.events, event));
    }

    /**
     * Создаем мув
     * @param event
     */
    private createMoveEvent(event:userEvent):void {
        this.startEvent(new Move(this.events, event));
    }

    /**
     * Запускаем событие
     * @param event
     */
    private startEvent(event:UserEventI) {
        this.events[event.type] = event;
        EventManager.startEvent(event);
    }
}
domEventManager = new DomEventManager();
export = domEventManager;