/// <reference path="../../../../interface/InteractiveRectI.d.ts" />
/// <reference path="../../../../interface/DomLite.d.ts" />
/// <reference path="../../../../interface/Utils.d.ts" />
/// <reference path="../../../../interface/Events.d.ts" />

import EventManager = require("./EventManager");

class InteractiveRect implements InteractiveRectI {

    private _events:Object;
    private rect:RectI;
    private level:number;
    private stop:boolean;

    constructor(rect:RectI, level?:number, stop?:boolean) {

        this.rect = rect;
        this.level = level || 5;
        this.stop = stop || false;

    }

    public on(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any)=>any, context?:any):InteractiveRectI {

        if (!this._events) {
            this._events = {};
        }

        if (!this._events[eventName]) {
            this._events[eventName] = [];
        }

        this._events[eventName].push({handler: handler, context: context || window});

        EventManager.on(eventName, handler, this, context);

        return this;
    }

    public once(eventName:string, handler:(a?:any, b?:any, c?:any, d?:any)=>any, context?:any):InteractiveRectI {

        var unbinder = function () {
            this.off(eventName, handler);
            this.off(eventName, unbinder);
        }.bind(this);

        this.on(eventName, handler, context);
        this.on(eventName, unbinder, this);

        return this;
    }

    public off(eventName?:string, handler?:(a?:any, b?:any, c?:any, d?:any)=>any):InteractiveRectI {

        if (!eventName) {

            this.disable();
            this._events = {};

        }

        if (!handler) {

            if (eventName in this._events) {
                this._events[eventName].forEach(function (eventObject) {
                    EventManager.off(eventName, eventObject.handler);
                });
                delete this._events[eventName];
            }

        }

        if (eventName && handler) {

            if (eventName in this._events) {

                for (var i = 0; i < this._events[eventName].length; i++) {
                    if (this._events[eventName][i].handler === handler) {
                        EventManager.off(eventName, handler);
                        this._events[eventName].splice(i,1);
                        break;
                    }
                }

            }

        }

        return this;
    }

    public enable():InteractiveRectI {

        for (var eventName in this._events) {
            if (this._events.hasOwnProperty(eventName)) {
                this._events[eventName].forEach(function (eventObject) {
                    EventManager.on(eventName, eventObject.handler, this, eventObject.context);
                }, this);
            }
        }

        return this;
    }

    public disable():InteractiveRectI {

        for (var eventName in this._events) {
            if (this._events.hasOwnProperty(eventName)) {
                this._events[eventName].forEach(function (eventObject) {
                    EventManager.off(eventName, eventObject.handler);
                });
            }
        }

        return this;
    }

    public isStop():boolean {
        return this.stop;
    }

    public getLevel():number {
        return this.level;
    }

    public hitTest(event:UserEventI):boolean {
        return event.hitTestByRect(this.rect);
    }

    public move(deltaX:number, deltaY:number):InteractiveRectI {
        this.rect.coords.x += deltaX;
        this.rect.coords.y += deltaY;
        return this;
    }

    public setPosition(x:number, y:number):InteractiveRectI {
        this.rect.coords.x = x;
        this.rect.coords.y = y;
        return this;
    }

    public getRect():RectI {
        return {
            size: {
                width: this.rect.size.width,
                height: this.rect.size.height
            },
            coords: {
                x: this.rect.coords.x,
                y: this.rect.coords.y
            }
        };
    }

    public setRect(rect:RectI):InteractiveRectI {
        this.rect = rect;
        return this;
    }

}
export = InteractiveRect;