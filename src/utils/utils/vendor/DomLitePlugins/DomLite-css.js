define(["$"], function ($) {


    (function () {

        var CSSMatrix=function(){var a=[].slice.call(arguments);if(a.length)for(var b=a.length;b--;)Math.abs(a[b])<CSSMatrix.SMALL_NUMBER&&(a[b]=0);this.setIdentity();if(16==a.length)this.m11=this.a=a[0],this.m12=this.b=a[1],this.m13=a[2],this.m14=a[3],this.m21=this.c=a[4],this.m22=this.d=a[5],this.m23=a[6],this.m24=a[7],this.m31=a[8],this.m32=a[9],this.m33=a[10],this.m34=a[11],this.m41=this.e=a[12],this.m42=this.f=a[13],this.m43=a[14],this.m44=a[15];else if(6==a.length)this.affine=!0,this.m11=this.a=a[0],
            this.m12=this.b=a[1],this.m14=this.e=a[4],this.m21=this.c=a[2],this.m22=this.d=a[3],this.m24=this.f=a[5];else if(1===a.length&&"string"==typeof a[0])this.setMatrixValue(a[0]);else if(0<a.length)throw new TypeError("Invalid Matrix Value");};CSSMatrix.SMALL_NUMBER=1E-6;
        CSSMatrix.Rotate=function(a,b,c){a*=Math.PI/180;b*=Math.PI/180;c*=Math.PI/180;var d=Math.cos(a);a=-Math.sin(a);var h=Math.cos(b);b=-Math.sin(b);var g=Math.cos(c);c=-Math.sin(c);var f=new CSSMatrix;f.m11=f.a=h*g;f.m12=f.b=-h*c;f.m13=b;f.m21=f.c=a*b*g+d*c;f.m22=f.d=d*g-a*b*c;f.m23=-a*h;f.m31=a*c-d*b*g;f.m32=a*g+d*b*c;f.m33=d*h;return f};
        CSSMatrix.RotateAxisAngle=function(a,b,c,d){d*=Math.PI/360;var h=Math.sin(d);d=Math.cos(d);var g=h*h,f=Math.sqrt(a*a+b*b+c*c);0===f?(b=a=0,c=1):(a/=f,b/=f,c/=f);var f=a*a,k=b*b,l=c*c,e=new CSSMatrix;e.m11=e.a=1-2*(k+l)*g;e.m12=e.b=2*(a*b*g+c*h*d);e.m13=2*(a*c*g-b*h*d);e.m21=e.c=2*(b*a*g-c*h*d);e.m22=e.d=1-2*(l+f)*g;e.m23=2*(b*c*g+a*h*d);e.m31=2*(c*a*g+b*h*d);e.m32=2*(c*b*g-a*h*d);e.m33=1-2*(f+k)*g;e.m14=e.m24=e.m34=0;e.m41=e.e=e.m42=e.f=e.m43=0;e.m44=1;return e};
        CSSMatrix.ScaleX=function(a){var b=new CSSMatrix;b.m11=b.a=a;return b};CSSMatrix.ScaleY=function(a){var b=new CSSMatrix;b.m22=b.d=a;return b};CSSMatrix.ScaleZ=function(a){var b=new CSSMatrix;b.m33=a;return b};CSSMatrix.Scale=function(a,b,c){var d=new CSSMatrix;d.m11=d.a=a;d.m22=d.d=b;d.m33=c;return d};CSSMatrix.SkewX=function(a){a*=Math.PI/180;var b=new CSSMatrix;b.m21=b.c=Math.tan(a);return b};CSSMatrix.SkewY=function(a){a*=Math.PI/180;var b=new CSSMatrix;b.m12=b.b=Math.tan(a);return b};
        CSSMatrix.Translate=function(a,b,c){var d=new CSSMatrix;d.m41=d.e=a;d.m42=d.f=b;d.m43=c;return d};
        CSSMatrix.multiply=function(a,b){return new CSSMatrix(b.m11*a.m11+b.m12*a.m21+b.m13*a.m31+b.m14*a.m41,b.m11*a.m12+b.m12*a.m22+b.m13*a.m32+b.m14*a.m42,b.m11*a.m13+b.m12*a.m23+b.m13*a.m33+b.m14*a.m43,b.m11*a.m14+b.m12*a.m24+b.m13*a.m34+b.m14*a.m44,b.m21*a.m11+b.m22*a.m21+b.m23*a.m31+b.m24*a.m41,b.m21*a.m12+b.m22*a.m22+b.m23*a.m32+b.m24*a.m42,b.m21*a.m13+b.m22*a.m23+b.m23*a.m33+b.m24*a.m43,b.m21*a.m14+b.m22*a.m24+b.m23*a.m34+b.m24*a.m44,b.m31*a.m11+b.m32*a.m21+b.m33*a.m31+b.m34*a.m41,b.m31*a.m12+b.m32*
            a.m22+b.m33*a.m32+b.m34*a.m42,b.m31*a.m13+b.m32*a.m23+b.m33*a.m33+b.m34*a.m43,b.m31*a.m14+b.m32*a.m24+b.m33*a.m34+b.m34*a.m44,b.m41*a.m11+b.m42*a.m21+b.m43*a.m31+b.m44*a.m41,b.m41*a.m12+b.m42*a.m22+b.m43*a.m32+b.m44*a.m42,b.m41*a.m13+b.m42*a.m23+b.m43*a.m33+b.m44*a.m43,b.m41*a.m14+b.m42*a.m24+b.m43*a.m34+b.m44*a.m44)};
        CSSMatrix.prototype.setMatrixValue=function(a){a=String(a).trim();this.setIdentity();if("none"==a)return this;var b=a.slice(0,a.indexOf("("));if("matrix3d"==b){a=a.slice(9,-1).split(",");for(b=a.length;b--;)a[b]=parseFloat(a[b]);this.m11=this.a=a[0];this.m12=this.b=a[1];this.m13=a[2];this.m14=a[3];this.m21=this.c=a[4];this.m22=this.d=a[5];this.m23=a[6];this.m24=a[7];this.m31=a[8];this.m32=a[9];this.m33=a[10];this.m34=a[11];this.m41=this.e=a[12];this.m42=this.f=a[13];this.m43=a[14];this.m44=a[15]}else if("matrix"==
            b){this.affine=!0;a=a.slice(7,-1).split(",");for(b=a.length;b--;)a[b]=parseFloat(a[b]);this.m11=this.a=a[0];this.m12=this.b=a[2];this.m41=this.e=a[4];this.m21=this.c=a[1];this.m22=this.d=a[3];this.m42=this.f=a[5]}else throw new TypeError("Invalid Matrix Value");return this};CSSMatrix.prototype.multiply=function(a){return CSSMatrix.multiply(this,a)};CSSMatrix.prototype.inverse=function(){throw Error("the inverse() method is not implemented (yet).");};
        CSSMatrix.prototype.translate=function(a,b,c){null==c&&(c=0);return CSSMatrix.multiply(this,CSSMatrix.Translate(a,b,c))};CSSMatrix.prototype.scale=function(a,b,c){null==b&&(b=a);null==c&&(c=1);return CSSMatrix.multiply(this,CSSMatrix.Scale(a,b,c))};CSSMatrix.prototype.rotate=function(a,b,c){null==b&&(b=a);null==c&&(c=a);return CSSMatrix.multiply(this,CSSMatrix.Rotate(a,b,c))};
        CSSMatrix.prototype.rotateAxisAngle=function(a,b,c,d){null==b&&(b=a);null==c&&(c=a);return CSSMatrix.multiply(this,CSSMatrix.RotateAxisAngle(a,b,c,d))};CSSMatrix.prototype.skewX=function(a){return CSSMatrix.multiply(this,CSSMatrix.SkewX(a))};CSSMatrix.prototype.skewY=function(a){return CSSMatrix.multiply(this,CSSMatrix.SkewY(a))};
        CSSMatrix.prototype.toString=function(){return this.affine?"matrix("+[this.a,this.b,this.c,this.d,this.e,this.f].join(", ")+")":"matrix3d("+[this.m11,this.m12,this.m13,this.m14,this.m21,this.m22,this.m23,this.m24,this.m31,this.m32,this.m33,this.m34,this.m41,this.m42,this.m43,this.m44].join(", ")+")"};
        CSSMatrix.prototype.setIdentity=function(){this.m11=this.a=1;this.m21=this.c=this.m14=this.m13=this.m12=this.b=0;this.m22=this.d=1;this.m32=this.m31=this.m24=this.m23=0;this.m33=1;this.m43=this.m42=this.f=this.m41=this.e=this.m34=0;this.m44=1;return this};
        CSSMatrix.prototype.transform=function(a){var b=this.m21*a.x+this.m22*a.y+this.m23*a.z+this.m24*a.w,c=this.m31*a.x+this.m32*a.y+this.m33*a.z+this.m34*a.w,d=this.m41*a.x+this.m42*a.y+this.m43*a.z+this.m44*a.w;a.x=(this.m11*a.x+this.m12*a.y+this.m13*a.z+this.m14*a.w)/d;a.y=b/d;a.z=c/d;return a};
        CSSMatrix.prototype.toFullString=function(){return[[this.m11,this.m12,this.m13,this.m14].join(", "),[this.m21,this.m22,this.m23,this.m24].join(", "),[this.m31,this.m32,this.m33,this.m34].join(", "),[this.m41,this.m42,this.m43,this.m44].join(", ")].join("\n")};


        /**
         * @extends Lite
         */
        $ = $.extend({

            constructor: $,

            _getMantrix: function (method, args) {
                if (!this._matrix) {
                    this._matrix = new CSSMatrix();
                }
                this._matrix = this._matrix[method].apply(this._matrix, args);
                return this._matrix.toString();
            },

            _setTime: function() {
                this._cssTimer = setTimeout(function () {
                    this.apply();
                }.bind(this), 0);
            },

            apply: function () {
                clearTimeout(this._cssTimer);
                return this.css("transform", this._matrix.toString());
            },

            /**
             * @param {Number} x
             * @param {Number} [y]
             * @return Lite
             */
            scale: function (x, y) {
                this._getMantrix("scale", [x, y || x]);
                this._setTime();
                return this;
            },

            /**
             * @param {String} x
             * @param {String} y
             * @return Lite
             */
            translate: function (x, y) {
                this._getMantrix("translate", [x, y]);
                this._setTime();
                return this;
            },

            rotate: function (rx,ry,rz) {
                if (arguments.length == 1) {
                    rz = rx;
                    rx = 0;
                }
                this._getMantrix("rotate", [rx||0, ry||0, rz||0]);
                this._setTime();
                return this;
            },

            /**
             * @param {Number} time
             * @param {String} [styleName]
             * @return Lite
             */
            transition: function (time, styleName) {
                return this.css("transition", "transition" + (styleName || "all") +  time + "s");
            },

            dropMatrix: function () {
                this._matrix = new CSSMatrix();
                return this;
            }

        });

    })();
});