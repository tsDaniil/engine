(function () {

    var TextNodeWrapper = function (textNode) {
        this.node = textNode;
    };

    TextNodeWrapper.prototype = {
        text: function (text) {
            if (text != undefined) {
                this.node.textContent = text;
            } else {
                return this.node.textContent;
            }
        },
        isElement: function () {
            return false;
        }
    };

    /**
     * @extends Lite
     */
    $ = $.extend({

        constructor: $,

        fullChildren: function () {

            return $.toArray(this.node.childNodes).map(function (node) {
                return $.isElement(node) ? new $(node) : new TextNodeWrapper(node);
            }, this);

        },

        isElement: function () {
            return true;
        }

    });

})();