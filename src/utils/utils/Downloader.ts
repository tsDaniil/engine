/// <reference path="../../interface/DownloaderI.d.ts" />
/// <reference path="../../interface/require.d.ts" />
/// <reference path="../init.ts" />
/// <reference path="../../interface/Promise.d.ts" />

import Module = require("../base/Module");
import $ = require("$");
import Promise = require("promise");

class ImagesData implements ImageDataI {

    public each(callback:(value:ImageStateI, imageSrc?:string) => void):void {
        $.each(this, callback);
    }

    public getNames():Array<string> {
        var paths = Object.getOwnPropertyNames(this);
        return paths.map(function (path) {
            var pathArr = path.split("/");
            if (path.length) {
                return path[path.length - 1];
            } else {
                return path;
            }
        });
    }
}

var downloader:DownloaderI;
class Downloader extends Module implements DownloaderI {

    private loadedStyles:Array<string> = [];
    private loadedImages:Array<string> = [];
    private waitTimeStyle:number = 2000;
    private waitTimeImage:number = 5000;

    public hasStyle(path:string):boolean {
        return this.loadedStyles.some(function (loadedPath:string) {
            return loadedPath == path;
        });
    }

    public loadStyle(path:string, callback?:(message:string)=>void, time?:number):void {
        var hasTimeout = true;
        if (this.hasStyle(path)) {
            console.log("Стиль уже загружен! " + path);
            if (callback) {
                callback("ok!");
            }
        } else {
            if (callback) {
                var timer = setTimeout(function () {
                    console.warn("Не дождались загрузки стиля!", path);
                    callback("Не дождались загрузки стиля!");
                    hasTimeout = false;
                }, time || this.waitTimeStyle);
            }
            var link = document.createElement("link");
            link.rel = "stylesheet";
            link.onerror = function () {
                if (window["engineAdditionalURL"] && path.indexOf(window["engineAdditionalURL"]) == -1) {
                    path = window["engineAdditionalURL"] + path;
                    downloader.loadStyle(path, hasTimeout ? callback : undefined, hasTimeout ? time : undefined);
                    if (hasTimeout) {
                        clearTimeout(timer);
                    }
                } else {
                    console.error("Не удалось скачать стиль! " + path)
                }
            };
            if (callback) {
                link.onload = function () {
                    clearTimeout(timer);
                    hasTimeout = false;
                    callback("ok");
                };
            }
            link.href = path;
            this.loadedStyles.push(path);
            document.head.appendChild(link);
        }
    }

    public loadStyles(paths:Array<string>, callback?:(message:string)=>void, time?:number):void {
        if (!callback) {
            paths.forEach(function (path) {
                this.loadStyle(path);
            }, this);
        } else {
            if (callback) {
                var timer = setTimeout(function () {
                    console.warn("Не дождались загрузки стиля!", paths);
                    callback("Не дождались загрузки стиля!");
                }, time || this.waitTimeStyle * paths.length);
            }
            var count = paths.length;
            paths.forEach(function (path) {
                this.loadStyle(path, function () {
                    count--;
                    if (count == 0) {
                        clearTimeout(timer);
                        callback("ok!");
                    }
                });
            }, this);
        }
    }

    public loadEngineStyle(path:string, callback?:(message:string)=>void, time?:number):void {
        this.loadStyle(paths.enginePath + path, callback, time);
    }

    public loadEngineStyles(pathsToLoad:Array<string>, callback?:(message:string)=>void, time?:number):void {
        this.loadStyles(pathsToLoad.map(function (path) {return paths.enginePath + path}), callback, time);
    }

    public loadImage(path:string, callback?:(message:boolean, image:HTMLImageElement, event?:any)=>void, time?:number, progressCallback?:(loaded, total)=>void):void {
        var image = new Image();
	    time = time || 1000;
        if (callback) {

            var timer = setTimeout(function () {
                console.warn("Не успел скачать картинку!", path);
                callback(false, image);
            }, time || this.waitTimeImage);

	        image.onload = function (event) {
                clearTimeout(timer);
                callback(true, this, event);
            };

            image.onerror = function (event) {
                clearTimeout(timer);
	            this.errorLoad = true;
                callback(false, this, event);
            };

			if(progressCallback) {
				image.onprogress = function (event) {
					progressCallback(event.loaded, event.total);
				};
			}
        }

        image.src = path;
        this.loadedImages.push(path);
    }

    public loadImages(paths:Array<string>, callback?:(message:ImageDataI)=>void, time?:number, progressCallback?:(loadSize, totalSize)=>void):void {
        if (!paths.length) {
            callback(new ImagesData());
        } else {
            if (!callback) {
                paths.forEach(function (path) {
                    this.loadImage(path);
                }, this);
            } else {
                var result = new ImagesData();
                var count = paths.length;
	            var imgsCount = {};

                paths.forEach(function (path) {

                    this.loadImage(path, function (state, image, event) {
                        result[image.src] = {
                            state: state,
                            image: image,
	                        event: event
                        };
                        count--;
                        if (count == 0) {
                            callback(result);
                        }

                    }, time, function(loadSize, totalSize){
	                    imgsCount[this] = {loadSize: loadSize, totalSize:totalSize};
	                    var size = 0;
	                    var load = 0;
						for(var i in imgsCount){
							size += totalSize;
							load += loadSize;
						}
	                    progressCallback(load, size );
                    });

                }, this);
            }
        }
    }

    public addLitePlugin(pluginName:string, callback?:()=>void):void {
        var that = this;
        require(["utils/utils/vendor/DomLitePlugins/" + pluginName], function () {
            that.fireEvent(pluginName + ":loaded");
            if (callback) {
                callback();
            }
        });

    }

}
downloader = new Downloader();
export = downloader;