/// <reference path="../../interface/ModalI.d.ts" />
/// <reference path="../../interface/DomLite.d.ts" />

import Module = require("../base/Module");
import Templater = require("../app/Templater");
import Downloader = require("../utils/Downloader");
import $ = require("$");

var templatePath:string = "utils/utils/template";
var templateName:string = "modal.tpl.html";

class Modal extends Module implements ModalI {

    private node:$i;
    private content:string;

    constructor() {
        super();
        var that = this;
        Templater.loadTemplate(templatePath, function () {
            that.loaded();
        });
        Downloader.loadEngineStyles([
            "utils/utils/vendor/bootstrap/bootstrap.min.css",
            "utils/utils/vendor/bootstrap/bootstrap-theme.min.css"]);
    }

    public setContent(content:string, title:string, size?:string):void {
        this.content = content;
        var that:Modal = this;
        this.onLoad(function () {
            that.node = $.parseHTML(Templater.getTemplate(templateName, {
                size: size || "modal-sm",
                title: title,
                content: that.content
            }), true).children().first().css("opacity", "1");
            $(document.body).append(that.node);
        });
    }

    public show():void {
        if (this.content) {
            var that:Modal = this;
            this.onLoad(function () {
                that.node.show();
            });
        } else {
            console.error("Вы не задали контент для модального окна!");
        }
    }

    public hide():void {
        this.node.hide();
    }

    public getNode():$i {
        return this.node;
    }

}
export = Modal;