/// <reference path="../../interface/AjaxI.d.ts" />

class Ajax implements AjaxI {

    private request(method:string, url:string, data:Object, success:(status:number, response?:any) => void, error?:(error:any, request:any) => void):void {

        var request = new XMLHttpRequest();
        request.open(method.toUpperCase(), url + Ajax.parseParams(data), true);
        request.onreadystatechange = function () {

            if (this.readyState == 4) {

                if (this.status != 200) {
                    if (error) {
                        error(this, this);
                    }
                } else {
                    if (success) {
                        success(this.status, this.responseText);
                    }
                }
            }

        };

        var onerrorHander = function (e) {
            if (error) {
                error(e, request);
            }
        };

        request.onabort = onerrorHander;
        request.onerror = onerrorHander;
        request.ontimeout = onerrorHander;

        request.send("");
    }

    public get(options:RequestOptionsI):void {
        this.request("get", options.url, options.data || {}, options.success, options.error);
    }

    public post(options:RequestOptionsI):void {
        this.request("post", options.url, options.data || {}, options.success, options.error);
    }

    private static parseParams(params:Object):string {

        var names = Object.getOwnPropertyNames(params);
        var result = "";

        if (names.length) {

            result += "?";

            names.forEach(function (name, index) {

                result += name + "=" + params[name];

                if (index < (names.length - 1)) {
                    result += "&"
                }

            });

        }

        return result;
    }

}
var ajax:AjaxI = new Ajax();
export = ajax;